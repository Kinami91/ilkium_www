<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['uri_segment'] = 3;
$config['num_links'] = 2;
$config['page_query_string'] = FALSE;
$config['use_page_numbers'] = TRUE;
$config['query_string_segment'] = "page";

$config['full_tag_open'] = "<ul class=\"pagination\">";
$config['full_tag_close'] = "</ul>";

$config['first_link'] = "1";
$config['first_tag_open'] = "<li>";
$config['first_tag_close'] = "</li>";

$config['last_link'] = "Last";
$config['last_tag_open'] = "<li><a>";
$config['last_tag_close'] = "</li>";

$config['next_link'] = "»";
$config['next_tag_open'] = "<li>";
$config['next_tag_close'] = "</li>";

$config['prev_link'] = "«";
$config['prev_tag_open'] = "<li>";
$config['prev_tag_close'] = "</li>";

$config['cur_tag_open'] = "<li class='active'><a>";
$config['cur_tag_close'] = "</a></li>";

$config['num_tag_open'] = "<li>";
$config['num_tag_close'] = "</li>";
