<?php

function printr($object, $name = '') {

    if ($name)
        print ( '\'' . $name . '\' : ');

    if (is_array($object)) {
        print ( '<pre>');
        print_r($object);
        print ( '</pre>');
    } else {
        var_dump($object);
    }
}

function hc($str, $n = 500, $end_char = ' ...') {
    $CI = & get_instance();
    $charset = $CI->config->item('charset');

    if (mb_strlen($str, $charset) < $n) {
        return $str;
    }

    $str = preg_replace("/\s+/iu", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

    if (mb_strlen($str, $charset) <= $n) {
        return $str;
    }
    return mb_substr(trim($str), 0, $n, $charset) . $end_char;
}

function goLogin($url) {
    echo "<script>location.replace('/login?url=".urlencode($url)."');</script>";
}

function alert($msg='', $url='') {
 $CI =& get_instance();

 if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';

 echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".$CI->config->item('charset')."\">";
 echo "<script type='text/javascript'>alert('".$msg."');";
    if ($url)
        echo "location.replace('".$url."');";
 else
  echo "history.go(-1);";
 echo "</script>";
 exit;
}