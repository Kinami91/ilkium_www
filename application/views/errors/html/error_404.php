

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="SemiColonWeb" />
        <meta http-equiv="x-ua-compatible" content="IE=edge" >

        <!-- Stylesheets
        ============================================= -->
        <!--<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />-->
        <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
        <!--<link rel="stylesheet" href="/css/bootstrap-combined.css" type="text/css" />-->
        <link rel="stylesheet" href="/css/style.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/dark.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/font-icons.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/animate.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/magnific-popup.min.css" type="text/css" />

        <link rel="stylesheet" href="/css/responsive.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/colors.min.css" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lt IE 8]>
                <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
                <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

        <!-- External JavaScripts
        ============================================= -->
        <script type="text/javascript" src="/js/jquery.js"></script>
        <script type="text/javascript" src="/js/plugins.js"></script>

        <!-- Document Title
        ============================================= -->
        <title>이르키움</title>

    </head>

    <body class="stretched">

        <!-- Document Wrapper
        ============================================= -->
        <div id="wrapper" class="clearfix">

            <!-- Header
            ============================================= -->
            <header id="header">

                <div id="header-wrap">

                    <div class="container clearfix">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                                                <!-- Logo
                        ============================================= -->
                        <div id="logo">
                            <a href="/" class="standard-logo" data-dark-logo="/images/logo-dark.png"><img src="/images/main/index_logo.png" alt="Canvas Logo"></a>
                            <a href="/" class="retina-logo" data-dark-logo="/images/logo-dark@2x.png"><img src="/images/main/index_logo_s.png" alt="Canvas Logo"></a>
                        </div><!-- #logo end -->
                                                <!-- Primary Navigation
                        ============================================= -->
                        <nav id="primary-menu" class="sub-title">
                            <div id="top-search" class="text-success"><i class="icon-calendar"></i>
                                수능 D<span>-326 </span>일</div>

                            <ul>
                                <li class="sub-menu"><a href="/intro"><div>이르키움</div><span>Intro</span></a>
                                    <ul>
                                        <li><a href="/intro"><div>이르키움 소개</div></a></li>
                                        <li><a href="/intro/manage"><div>관리시스템</div></a></li>
                                        <li><a href="/intro/teacher"><div>강사소개</div></a></li>
                                        <li><a href="/intro/why"><div>Why 이르키움?</div></a></li>

                                    </ul>
                                </li>

                                <li class="sub-menu"><a href="/admission"><div>입학안내</div><span>Admission</span></a>

                                </li>
                                <li class="sub-menu"><a href="/notice/noticeList"><div>이르키움 소식</div><span>Latest News</span></a>
                                    <ul>
                                        <li><a href="/notice/noticeList"><div>공지사항</div></a></li>
                                        <li><a href="/notice/faqs"><div>FAQ</div></a></li>

                                    </ul>
                                </li>
                                <li><a href="/branch"><div>지점안내</div><span>Branch</span></a>

                                </li>
                            </ul>


                            <!-- Top Search
                            ============================================= -->

                        </nav><!-- #primary-menu end -->

                    </div>

                </div>

            </header><!-- #header end --><section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>Page Not Found</h1>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">Error</li>
        </ol>
    </div>

</section>
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_half nobottommargin">
                <div class="error404 center">404</div>
            </div>

            <div class="col_half nobottommargin col_last">

                <div class="heading-block nobottomborder">
                    <h4>요청하신 페이지를 찾을 수 없습니다.</h4>
                    <span>정확한 주소를 확인 한 후 다시 시도해주시기 바랍니다.</span>
                </div>


            </div>

        </div>

    </div>

</section><!-- #content end --><!-- Footer
============================================= -->
<footer id="footer" class="dark">

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half">
                Copyrights &copy; 2015 All Rights Reserved by 이르키움.<br>
                <!--<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>-->
            </div>

                <div class="clear"></div>
                                이르키움(주) <span class="middot">&middot;</span> 대표이사 이해붕
                <span class="middot">&middot;</span><i class="icon-phone2"></i>&nbsp; TEL : 031.902.2675
                <br>
                <i class="icon-briefcase"></i>&nbsp;사업자등록번호 : 128-87-01744 <span class="middot">&middot;</span>

                경기도 고양시 일산동구 마두동 753-1(일산로 249) 주영빌딩 2층
                
        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/js/functions.min.js?ver=1"></script>
<script>
    var main_controller = "api";
    $("#primary-menu > ul > li").removeClass("current");
    switch(main_controller) {
        case "intro":
            $("#primary-menu > ul > li:first").addClass("current");
        break;
        case "admission":
            $("#primary-menu > ul > li:eq(1)").addClass("current");
        break;
    case "notice":
            $("#primary-menu > ul > li:eq(2)").addClass("current");
        break;
    case "branch":
            $("#primary-menu > ul > li:eq(3)").addClass("current");
        break;
    }
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71590744-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>