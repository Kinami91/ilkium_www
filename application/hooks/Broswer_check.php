<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Broswer_check {
    private $CI;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->library('user_agent');
    }

    public function broswer_check() {
                
        if ($this->CI->agent->is_mobile()) {
            define('BROWSER_TYPE', 'M');
        } else {
            define('BROWSER_TYPE', 'W');
        }
    }

}

/* End of file Broswer_check.php */
/* Location: ./application/controllers/Broswer_check.php */