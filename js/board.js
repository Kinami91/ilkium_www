$(function () {
    
//    $("#btnWrite").on("click", function (e) {
//        e.preventDefault();
//        location.href = "/board/write";
//    });
//    
//    $("#btnList").on("click", function (e) {
//        e.preventDefault();
//        location.href = "/board/boardList";
//    });    
    
    var norobot_val = null;
    
    $("#imgCaptcha").on("click", function () {
        $.getJSON("/api/kcaptcha/image", function (r) {
            $("#imgCaptcha").attr("src", "/img/captcha/" + r.filename);
            norobot_val = r.word;
        });
    });
    $("#imgCaptcha").trigger('click');
    
   
  
    $("#btnCommentSubmit").on("click", function (e) {
        e.preventDefault();
       
        Util.api("addBoardComment", {
            board_id: board_id,
            writer: $("#comment_writer").val(),
            comment_pwd: $("#comment_pwd").val(),
            comment: $("#comment_body").val()
        }, function (r) {
            console.log(r);
            insertCommentTemplate(r);
        });
    });
    
    var insertCommentTemplate = function (data) {
        var $str = $([
        '<div class="item">',
        '    <div class="item-head">',
        '        <div class="item-details">',
        '            <span  class="item-name primary-link">' + data.writer + '</span>',
        '            <span class="item-label">' + data.ins_date + '</span>',
        '        </div>',
        '        <div class="item-status"><i class="fa fa-remove"></i></div>',
        '    </div>',
        '    <div class="item-body">',
        data.comment,
        '    </div>',
        '</div>'
        ].join("\n"));
        
        $str.prependTo($("#commentList"));
        $("#imgCaptcha").trigger("click");
        $("#comment_body, #secret_letter").val("");
        
        
    };
});

