var Util = {};

var api_host = function () {
    if (window.location.host == "dev.admin.ilkium.co.kr") {
        return "dev.www.ilkium.co.kr";
    } else if (window.location.host == "lgtv.see2.co.kr") {
        return "dev.www.ilkium.co.kr";
    } else {
        return "dev.www.ilkium.co.kr";
    }
}();

Util.ajax = function (url, data, callback) {
    return $.ajax({
        url: url,
        dataType: "json",
        type: "post",
        //		jsonp : "callback",
        data: data,
        //		crossDomain : true,
        success: callback,
        error: function () {
            
//            Util.toast("ruby", "서비스가 원활하지 않습니다. 관리자에게 문의하세요");
//            UI.popup({
//                content: "서비스가 원활하지 않습니다.",
//                items: ['확인'],
//                callback: function(i) {
//                    location.href("/");
//                },
//                postback: function () {
//                    location.href("/");
//                }
//            });
            console.log('Fail : ' + url + '    ********************************************************');
            console.log(data);
            callback;
            console.log('*************************************************************');
        }
    });
};

Util.api = function (arg1, arg2, arg3) {
//    App.startPageLoading({animate: true});
    var send_args = {
//        method : 'smart_tv'
    }
    , send_callback_func;

    if (typeof arg1 !== "object") {
        send_args.method = arg1;

        if (typeof arg2 === "object" && typeof arg3 === "function") {
            $.extend(send_args, arg2);
            send_callback_func = arg3;
        } else if (typeof arg2 === "function") {
            send_callback_func = arg2;
        } else {
            console.log("invalid argments !!");
            return;
        }
    } else {
        console.log("invalid argments !!");
        return;
    }

//    $('#loading').sfLoading('show');
    Util.ajax("http://" + window.location.host + "/api/" + send_args.method, send_args, function (d) {
//        App.stopPageLoading();
        console.log(send_args);
//        console.log("http://" + window.location.host + "/api/"+send_args.method);
        send_callback_func(d[send_args.method]);
    });

};

Util.toast = function (theme, msg) {
    var settings = {
        theme: theme,
        sticky: false,
        horizontalEdge: "top",
        verticalEdge: "right",
        life: 1500
    },
    $button = $(this);

    settings.heading = "알림";

    $.notific8('zindex', 11500);
    $.notific8(msg, settings);

    $button.attr('disabled', 'disabled');

    setTimeout(function () {
        $button.removeAttr('disabled');
    }, 1000);
};
