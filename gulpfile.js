var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssMin = require('gulp-css');

gulp.task('css', function () {
    
    gulp.src([
        './css/bootstrap.min.css',
        './css/style.css',
        './css/dark.min.css',
        './css/font-icons.min.css',
        './css/animate.min.css',
        './css/magnific-popup.min.css'
    ])
    .pipe(concat('default.css'))
    .pipe(cssMin())
    .pipe(gulp.dest('./css'));
    
});

gulp.task('script', function () {
    gulp.src([
      './js/jquery.js',
      './js/plugins.js'
    ])
    .pipe(concat('lib.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js'));
    
    gulp.src([
      './js/functions.min.js',
      './js/bootstrap.min.js'
    ])
    .pipe(concat('lib2.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js'));    
});

gulp.task('default', ['script', 'css']);
//gulp.task('default', function(callback) {
//  return gulp.src([
//      './js/jquery.js',
//      './js/plugins.js'
//    ])
//    .pipe(sourcemaps.init())
//    // getBundleName creates a cache busting name
//    .pipe(concat(getBundleName('vendor')))
//    .pipe(uglify())
//    .pipe(sourcemaps.write('./'))
//    .pipe(gulp.dest('./js')) fdsfdsafdsafd
//});