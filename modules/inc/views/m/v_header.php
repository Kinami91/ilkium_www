<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <title>이르키움 - 독학재수관리학원</title>

        <link rel="stylesheet" type="text/css" href="/assets/styles/style.css?<?= filemtime(FCPATH . "/assets/styles/style.css") ?>">
        <link rel="stylesheet" type="text/css" href="/assets/styles/skin.css?<?= filemtime(FCPATH . "/assets/styles/skin.css") ?>">
        <link rel="stylesheet" type="text/css" href="/assets/styles/framework.css?<?= filemtime(FCPATH . "/assets/styles/framework.css") ?>">
        <link rel="stylesheet" type="text/css" href="/assets/styles/ionicons.min.css?<?= filemtime(FCPATH . "/assets/styles/ionicons.min.css") ?>">
        <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet"/>-->

        <script type="text/javascript" src="/assets/scripts/jquery.js"></script>
        <script type="text/javascript" src="/assets/scripts/plugins.js"></script>
        <script type="text/javascript" src="/assets/scripts/custom.js"></script>
        <script type="text/javascript" src="/assets/scripts/app.js"></script>
    </head>

    <body>
        <div id="page-transitions">
            <div class="sidebars sidebars-light">
                <div class="sidebar sidebar-left">
                    <div class="sidebar-header sidebar-header-image bg-3">
                        <div class="overlay dark-overlay"></div>
                        <a href="/" class="sidebar-logo">
                            &nbsp;
                        </a>
                    </div>
                    <div class="menu-options icon-background no-submenu-numbers sidebar-menu">
                        <!--<a class="active-item" href="/"><i class="icon-bg bg-night-dark ion-ios-search"></i><span>가까운 학원찾기</span></a>-->
                        <em class="menu-divider">About</em>
                        <a href="/intro/why"><i class="icon-bg bg-red-dark ion-help"></i><span>왜 이르키움인가?</span></a>
                        <a href="/intro"><i class="icon-bg bg-green-dark ion-speakerphone"></i><span>이르키움 소개</span></a>
                        <a href="/intro/manage"><i class="icon-bg bg-orange-dark ion-ios-color-filter"></i><span>관리시스템</span></a>
                        <a href="/success/story"><i class="icon-bg bg-teal-dark ion-android-clipboard"></i><span>성공사례</span></a>
                        <a href="/intro/teacher"><i class="icon-bg bg-night-dark ion-android-people"></i><span>강사소개</span></a>
                        <a href="/admission"><i class="icon-bg bg-blue-dark ion-ios-pricetag"></i><span>입학안내</span></a>
                        <a href="/news"><i class="icon-bg bg-magenta-dark ion-ios-bookmarks"></i><span>대학입시정보</span></a>
                        <em class="menu-divider">Communication</em>
                        <a href="/notice"><i class="icon-bg bg-green-light ion-mic-a"></i><span>공지사항</span></a>
                        <a href="/notice/faqs"><i class="icon-bg bg-night-light ion-chatbox-working"></i><span>FAQ</span></a>
                        <a href="/board"><i class="icon-bg bg-blue-dark ion-android-chat"></i><span>Q&A</span></a>
                        <a href="#" class="close-sidebar"><i class="icon-bg bg-red-light ion-android-close"></i><span>Close</span><i class="ion-record"></i></a>
                        <em class="menu-divider">Copyright <u class="copyright-year"></u>. All rights reserved</em>
                    </div>
                </div> 
                <div class="sidebar sidebar-right">
                    <div class="sidebar-header sidebar-header-classic">
                        <!--<h3><span class="ilkium-text-black"></span> 지점목록</h3>-->
                        
                        <a href="#" class="sidebar-logo">
                            <strong>학원찾기</strong>
                        </a>
                    </div>

                    <div class="menu-options sidebar-menu">
                        <em class="menu-divider">서울지역</em>
                        <a class="default-link" href="#">
                            <span>강남 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('knam')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('knam')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>강동 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('kdong')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('kdong')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>강서 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('kseo')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('kseo')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>강북 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('kbuk')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('kbuk')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>관악 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('gnk')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('gnk')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>노원 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('nowon')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('nowon')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>목동 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('mdong')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('mdong')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>은평 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('epyeong')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('epyeong')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>잠실 &nbsp;<b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('jsil')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('jsil')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <em class="menu-divider">경기지역</em>
                        <a class="default-link" href="#">
                            <span>구리 <b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('guri')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('guri')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>분당 <b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('bdang')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('bdang')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>수원 <b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('swj')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('swj')"></i>
                            <i class="ion-record"></i>
                        </a>
                        <a class="default-link" href="#">
                            <span>일산 <b>이르키움</b></span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk" onclick="Util.goBranch('isan')"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk" onclick="Util.goBranch('isan')"></i>
                            <i class="ion-record"></i>
                        </a>
<!--                        <a class="default-link" href="#">
                            <span><b>화정</b> 이르키움</span>
                            <i class="icon icon-round icon-phone-bg icon-phone-color ion-ios-telephone icon-xs-ilk"></i>
                            <i class="icon icon-round icon-location-bg icon-location-color ion-location icon-xs-ilk"></i>
                            <i class="ion-record"></i>
                        </a>-->
                        
                    </div>
                </div>
            </div>

            <div class="header header-logo-center header-light">
                <a href="#" class="header-icon header-icon-1 open-sidebar-left"></a>
                <a href="/" class="header-logo"></a>
                <a href="#" class="header-icon header-icon-4 open-sidebar-right"></a>    
            </div>
            <div id="page-content" class="page-content-scroll">	    