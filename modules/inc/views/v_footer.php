<!-- Footer
============================================= -->
<footer id="footer" class="dark">

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half">
                Copyrights &copy; 2015 All Rights Reserved by 이르키움.<br>
                <!--<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>-->
            </div>

            <div class="clear"></div>
            <?php
//                print_r($info);
            if (isset($info) && $info['branch_name']) {
                ?>
                <?= $info['branch_name'] ?> <span class="middot">&middot;</span> <?= $info['branch_id'] == "suwon" ? "원장" : "대표" ?>: <?= $info['ceo_name'] ?>
                <span class="middot">&middot;</span><i class="icon-phone2"></i>&nbsp; TEL : <?= $info['phone'] ?>
                <?php if ($info['fax']) { ?><span class="middot">&middot;</span><i class="icon-print"></i>&nbsp;FAX : <?= $info['fax'] ?><?php } ?><br>
                <?php if ($info['busi_num']) { ?><i class="icon-briefcase"></i>&nbsp;사업자등록번호 : <?= $info['busi_num'] ?><span class="middot">&middot;</span><?php } ?>

                <?= $info['address'] ?>
                <?php
            } else {
                ?>
                이르키움(주) <span class="middot">&middot;</span> 대표이사 이해붕
                <span class="middot">&middot;</span><i class="icon-phone2"></i>&nbsp; TEL : 031.902.2675
                <?php if ($info['fax']) { ?><span class="middot">&middot;</span><i class="icon-print"></i>&nbsp;FAX : <?= $info['fax'] ?><?php } ?><br>
                <i class="icon-briefcase"></i>&nbsp;사업자등록번호 : 128-87-01744 <span class="middot">&middot;</span>

                경기도 고양시 일산동구 마두동 753-1(일산로 249) 주영빌딩 2층
                <?php
            }
            ?>

        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/js/functions.js?ver=3"></script>
<!--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
<script>
    var main_controller = "<?= $this->uri->segment(1) ?>";
    $("#primary-menu > ul > li").removeClass("current");
    switch (main_controller) {
        case "intro":
            $("#primary-menu > ul > li:first").addClass("current");
            break;
        case "success":
            $("#primary-menu > ul > li:eq(1)").addClass("current");
            break;
        case "admission":
            $("#primary-menu > ul > li:eq(2)").addClass("current");
            break;
        case "board":
        case "notice":
            $("#primary-menu > ul > li:eq(3)").addClass("current");
            break;
        case "branch":
            $("#primary-menu > ul > li:eq(4)").addClass("current");
            break;
    }

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71590744-1', 'auto');
  ga('send', 'pageview');

</script>
<?php
if($_SERVER['HTTP_HOST']=="pchon.ilkium.co.kr") {
?>
<!-- 공통 적용 스크립트 , 모든 페이지에 노출되도록 설치. 단 전환페이지 설정값보다 항상 하단에 위치해야함 --> 
<script type="text/javascript" src="//wcs.naver.net/wcslog.js"> </script> 
<script type="text/javascript"> 
if (!wcs_add) var wcs_add={};
wcs_add["wa"] = "s_1252af33174b";
if (!_nasa) var _nasa={};
wcs.inflow();
wcs_do(_nasa);
</script>
<?php    
}
?>

</body>
</html>