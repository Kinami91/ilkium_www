<?php 

//        $this->load->library("user_agent");
//        $agent_ver = $this->agent->version();
//        if($this->agent->browser() == "Internet Explorer" && ($agent_ver == "7.0" || $agent_ver == "8.0" || $agent_ver == "6.0")) {
//            $this->load->view("branch/v_banIE");
//            
//            exit;
//        }
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="SemiColonWeb" />
        <meta http-equiv="x-ua-compatible" content="IE=edge" >

        <!-- Stylesheets
        ============================================= -->
        <!--<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />-->
        <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />
        <!--<link rel="stylesheet" href="/css/bootstrap-combined.css" type="text/css" />-->
        <link rel="stylesheet" href="/css/style.css?<?=filemtime(FCPATH ."/css/style.css")?>" type="text/css" />
        <link rel="stylesheet" href="/css/dark.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/font-icons.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/animate.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/magnific-popup.min.css" type="text/css" />

        <link rel="stylesheet" href="/css/responsive.css?<?=filemtime(FCPATH ."/css/responsive.css")?>" type="text/css" />
        <link rel="stylesheet" href="/css/colors.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/colors.min.css" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" href="/favicon.ico">

        <meta name="naver-site-verification" content="dd3f103b6c309d790839d5889e3181feb9958fc5"/>
        <?php
            $url = $_SERVER['HTTP_HOST'];
            if(strstr($url, "pchon")) {
        ?>
        <meta name="description" content="평촌독학재수학원 이르키움입니다. 재수 및 N수를 고민하는 모든 학생들의 성공을 기원합니다."/>        
        
        <?php
            }
        
        
        ?>
        <!--[if lt IE 8]>
                <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
                <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

        <!-- External JavaScripts
        ============================================= -->
        <script type="text/javascript" src="/js/jquery.js"></script>
        <script type="text/javascript" src="/js/plugins.js"></script>
        <script type="text/javascript" src="/js/functions.js?<?=filemtime(FCPATH ."/js/functions.js")?>"></script>

        <!-- Document Title
        ============================================= -->
        <title>독학재수학원 이르키움입니다.</title>

    </head>

    <body class="stretched">

        <!-- Document Wrapper
        ============================================= -->
        <div id="wrapper" class="clearfix">

            <!-- Header
            ============================================= -->
            <header id="header">

                <div id="header-wrap">

                    <div class="container clearfix">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                        <?php
                        if (strstr($url, "www")) {
                            $link = "/";
                        } else {
                            $link = "http://www.ilkium.co.kr";
                        }
                        ?>
                        <!-- Logo
                        ============================================= -->
                        <div id="logo">
                            <a href="<?=$link?>" class="standard-logo" data-dark-logo="/images/logo-dark.png"><img src="/images/main/index_logo.png" alt="Canvas Logo"></a>
                            <a href="<?=$link?>" class="retina-logo" data-dark-logo="/images/logo-dark@2x.png"><img src="/images/main/index_logo_s.png" alt="Canvas Logo"></a>
                        </div><!-- #logo end -->
                        <?php
                        $datetime1 = date_create('2018-11-15');
                        $datetime2 = date_create(date("Y-m-d"));
//$datetime2 = date_create('2016-11-11');
                        $interval = date_diff($datetime1, $datetime2);
                        $d_day = $interval->format('%R%a ');
                        ?>
                        <!-- Primary Navigation
                        ============================================= -->
                        <nav id="primary-menu" class="sub-title">
                            <div id="top-search" class="text-success"><i class="icon-calendar"></i>
                                수능 D<span><?= $d_day ?></span>일</div>

                            <ul>
                                <li class="sub-menu"><a href="#"><div>이르키움</div><span>Intro</span></a>
                                    <ul>
                                        <li><a href="/intro"><div>이르키움 소개</div></a></li>
                                        <li><a href="/intro/manage"><div>관리시스템</div></a></li>
                                        <li><a href="/intro/teacher"><div>강사소개</div></a></li>
                                        <li><a href="/intro/why"><div>Why 이르키움?</div></a></li>

                                    </ul>
                                </li>

                                <li class="sub-menu"><a href="#"><div>합격자명단</div><span>Success</span></a>
                                    <ul>
                                        <li><a href="/success">합격자 명단</a></li>
                                        <li><a href="/success/story">합격자 후기</a></li>
                                    </ul>
                                </li>
                                <li class="sub-menu"><a href="#"><div>입학안내</div><span>Admission</span></a>
                                    <ul>
                                        <li><a href="/admission">입학절차</a></li>
                                        <li><a href="/admission/main">재수관리반</a></li>
                                        <li><a href="#">윈터스쿨반</a></li>
                                        <li><a href="#">썸머스쿨반</a></li>
                                        <li><a href="#">반수생반</a></li>
                                    </ul>
                                </li>
                                <li class="sub-menu"><a href="#"><div>이르키움 소식</div><span>Latest News</span></a>
                                    <ul>
                                        <li><a href="/notice/noticeList"><div>공지사항</div></a></li>
                                        <li><a href="/board/boardList"><div>묻고 답하기</div></a></li>
                                        <li><a href="/notice/faqs"><div>FAQ</div></a></li>

                                    </ul>
                                </li>
                                <li class="sub-menu"><a href="#"><div>지점안내</div><span>Branches</span></a>
                                        <ul>
                                            <li><a href="/branch"><div><i class="icon-map-marker2"></i>지도보기</div></a></li>
                                            <li class="sub-title clearfix"><div>&nbsp; <i class="icon-folder-close"></i> 서울 지역</div></li>
                                            <li><a href="http://kdong.ilkium.co.kr/"><div>강동이르키움</div></a></li>
                                            <li><a href="http://kseo.ilkium.co.kr/"><div>강서이르키움</div></a></li>
                                            <li><a href="http://kbuk.ilkium.co.kr/"><div>강북이르키움</div></a></li>
                                            <li><a href="http://knam.ilkium.co.kr/"><div>대치이르키움</div></a></li>
                                            <li><a href="http://gnk.ilkium.co.kr/"><div>관악이르키움</div></a></li>
                                            <li><a href="http://nowon.ilkium.co.kr/"><div>노원이르키움</div></a></li>
                                            <li><a href="http://mdong.ilkium.co.kr/"><div>목동이르키움</div></a></li>
                                            <li><a href="http://epyeong.ilkium.co.kr/"><div>은평이르키움</div></a></li>
                                            <li><a href="http://jsil.ilkium.co.kr/"><div>잠실이르키움</div></a></li>
                                            <li class="sub-title"><div>&nbsp; <i class="icon-folder-close"></i>경기 지역</div></li>
                                            <li><a href="http://guri.ilkium.co.kr/"><div>구리이르키움</div></a></li>
                                            <li><a href="http://bdang.ilkium.co.kr/"><div>분당이르키움</div></a></li>
                                            <li><a href="http://swj.ilkium.co.kr/"><div>수원이르키움</div></a></li>
                                            <li><a href="http://isan.ilkium.co.kr/"><div>일산이르키움</div></a></li>
                                            <!--<li><a href="http://pchon.ilkium.co.kr/"><div>평촌이르키움</div></a></li>-->
                                            <!--<li><a href="http://hjung.ilkium.co.kr/"><div>화정이르키움</div></a></li>-->
                                        </ul>
                                </li>
                            </ul>


                            <!-- Top Search
                            ============================================= -->

                        </nav><!-- #primary-menu end -->

                    </div>

                </div>

            </header><!-- #header end -->