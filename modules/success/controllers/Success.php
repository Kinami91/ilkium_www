<?php 

if (!defined('BASEPATH')) 
	exit('No direct script access allowed');

class Success extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
            $data['rows'] = $this->db->select("*")
                    ->from("i_common.univ_applicant")
                    ->where("enter_year", "2018")
                    ->order_by("univ_grade ASC, university ASC")
                    ->get()->result_array();
            
            $data['rows'] = null;
            
            $this->load->view("inc/v_header");
            $this->load->view("v_winList", $data);
            $this->load->view("inc/v_footer");
		
	}
        
        public function story() {
            if(BROWSER_TYPE == "W") {
                $this->load->view("inc/v_header");
                $this->load->view("v_winStory");
                $this->load->view("inc/v_footer");
            } else {
                $this->load->view("inc/m/v_header");
                $this->load->view("m/v_winStory");
                $this->load->view("inc/m/v_footer");
            }
            
        }

}

/* End of file Success.php */
/* Location: ./application/controllers/Success.php */