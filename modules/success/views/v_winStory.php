<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">
    <div class="container clearfix">
        <h1>합격자 후기</h1>
        <span>Successful Story</span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <!--<li><a href="#">이르키움</a></li>-->
            <li class="active">합격자 후기</li>
        </ol>
    </div>
</section><!-- #page-title end -->


<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <!--        <div class="section parallax bottommargin-sm" style="padding: 0; background-image: url(&quot;http://canvashtml-cdn.semicolonweb.com/images/parallax/3.jpg&quot;); background-position: 50% 72px;" data-stellar-background-ratio="0.3">
                    <div class="heading-block center nobottomborder nobottommargin">
                        <h2>이르키움 2016학년도 합격자명단</h2>
                        <p class="nobottommargin">합격을 축하합니다. 그간 정말 고생많았습니다.</p>
                    </div>
                </div>        -->
        <div class="container clearfix">
            <div class="postcontent col_last bottommargin clearfix">
                <div id="posts">
<!--                    <div class="fancy-title title-bottom-border clearfix">
                        <h5>대성 고등학교  <span>김태윤</span></h5>
                    </div>-->
                    <blockquote class="quote">
                        <p style="font-size:14px;">제가 이르키움을 다니기 시작한 것은 2월달 친구의 추천을 받아 다니게 되었습니다. <br/>
                            재수를 하기로 결심하고 어디서 어떻게 해야 할지 정말 고민을 많이 했습니다. <br/>고3때처럼 독서실을 갈까, 재수종합반 그리고 독학재수학원
                            이 세가지 선택중에 독서실은 1년간 꾸준히 처음같은 마음가짐으로 할 자신이 없었고,
                            재수종합반은 금전적 부담이 커서 독학재수학원중 이르키움을 선택하게 되었습니다.<br/>
                            혹시 독학재수학원이라하면 선생님들도 없이 그냥 관리만 해주는게 아닌가 하는 제 걱정과는 달리
                            국,영,수 각 영역에서의 수준 높으신 선생님들과 멘토분들이 각 과목에서의 질문과 공부방향 설정에 정말 많은 도움을 주셨습니다.<br/>
                            제가 학원에서 공부하면서 성적뿐아니라 슬럼프로 힘들때마다 꼼꼼히 관리해주는 방식이 제가 지금 11월까지
                            참고 버티는데 가장 큰 힘이 되었던 것 같습니다.<br/> 재수 1년간 가족들보다 같이 있는 시간이 많았던 원장님과
                            실장님의 진심어린 노고가 이르키움의 큰 장점이라고 생각이 듭니다.<br/>
                            아마 다시 수능을 준비하시는 분이라면 1년간 정말 힘들도 지칠테지만 이르키움 학원과 같이 희망하는 대학에 꼭 입학하셨으면 좋겠습니다.<br/>
                            화이팅하세요!!</p>
                        <footer>대성 고등학교  <span>김태윤</span>(은평이르키움학원)</footer>
                    </blockquote>
                    <div class="divider"><i class="icon-circle"></i></div>
<!--                    <div class="fancy-title title-bottom-border clearfix">
                        <h5>분당고등학교  <span>홍석호</span>(서강대 경제학과)</h5>
                    </div>-->
                    <blockquote class="quote">
                        <p style="font-size:14px;">처음에 내가 독학재수를 하겠다고 주변사람들에게 말했을 때 모두다 반대했다. <br />
                            하지만 나는 종합학원에서 세운 규칙대로 공부하는 것보다 내가 부족한 부분을 스스로 보완하는 식으로 혼자 공부하는 것이 더 효율적이라고 생각했다. <br/>
                        여러 독학재수학원을 상담하던 중 수학선생님이면서 재수학원에서 25년간 가르치고 기숙학원까지 운영하신 원장님과 상담하면서 
                        신뢰가 가는 분당이르키움에 다니기로 결정하였다. <br />
                        재수를 시작할 때 가장 먼저 해야 할 것은 자기가 왜 실패했는지 분석하는 것이다. 만일 현역때와 똑같은 생각을 가지고 똑같이 공부하면
                        결과는 항상 같을 것이다.
                        사람이 발전을 하려면 적절한 피드백은 필수라고 생각한다. <br />
                        수능 때 부진했던 과목, 즉 자신을 재수, N수하게 만든 과목을 파악하고
                        그에 따른 올바른 계획을 세우는 것이 필요하다.<br />
                        재수를 성공적으로 끝마치면서 하고 싶은 말은, 재수(N수)를 한다고 해서 
                        절대로 실패자가 아니라는 것이다. 간혹 친구들 중에 재수에 대해서 너무 부정적으로 생각하는 경우가 있는데
                        그렇지 않다. 목표를 위해 도전하고 노력하는 것만큼 행복한 일은 없다고 생각한다.<br />
                        이왕 한번 하기로 결심한거 후회하지 않도록 열심히 살았으면 좋겠다.<br />
                        여러분들도 분당이르키움학원에서 재도전에 성공하시길 빕니다.
                        </p>
                        <footer>분당고등학교  <span>홍석호</span>(서강대 경제학과-분당이르키움학원)</footer>
                    </blockquote>
                    <div class="divider"><i class="icon-circle"></i></div>                    
<!--                    <div class="fancy-title title-bottom-border clearfix">
                        <h5>진관고등학교  <span>류혜림</span></h5>
                    </div>-->
                    <blockquote class="quote">
                        <p style="font-size:14px;">
                            재수를 시작해 두 달 정도 집에서 독재를 했었습니다. 하지만 5월이 되면서 점점 풀어지고 의지도 잃어 공부에 소홀하게 되더라구요.<br />
                            재수종합반을 가야하나 고민하던 찰나에, 집 근처 이르키움독학재수학원이 있다는 것을 알고 바로 학원에 등록했습니다.<br />
                            결론적으로, 이르키움에 다니길 정말 잘한 것임을 깨달았습니다.<br />
                            실제로 학원은 내가 원하는 공부를 할 수 있게 해주되 많은 것은 간섭하지 않았고, 내 공부량이 나에게 적절한지, 
                            나에게 맞는 더 좋은 인강 및 교재가 어느 것인지 전체적인 관리를 해주었습니다.<br />
                            또한 공부를 나의 컨디션에 맞게 조절하면서 할 수 있어서 좋았고 강압적이면서도 강압적이지 않은 분위기라 편안함을 
                            가질 수 있었습니다.<br />
                            또한 다른 학생들이 열심히 하는 모습을 보면서 공부하다보니 긴장감을 놓치지 않은채 꾸준히 공부를 할 수 있었습니다.<br />
                            더불어 부족 과목에 대한 무료수업을 통해 높은 성적의 등급을 받았습니다.
                            가장 취약했던 영어 수업을 듣게 되었는데 저에겐 정말 최상이었습니다.<br />
                            매주 한 시간 반 정도였지만 개개인별 꼼꼼한 방향제시 및 문제점 해결을 통해 푸는 방법등에서 많은 개선이 있었고
                            곧 성적상승이라는 성취감을 맛볼 수 있었습니다. 또한 학원에서 소수정예 논술 수업을 개강해
                            멀리 이동하지 않고도 양질의 수업을 들을 수 있어 매우 알차게 학원생활을 한 것 같습니다.<br /><br />
                            입시에 대해서도 입시전담원장님과 수시정밀상담도 받았고 많은 정보에 대해서도 도움을 받을 수 있었습니다.
                            꾸준하게 일정한 공부시간을 달성할 수 있었고, 혼자만 하지 않다보니 우울한 감정이나 스트레스도 덜 받을 수 있었습니다.<br />
                            이르키움학원을 통해 일년이라는 긴 시간동안 잘 버티면서 재수생활을 할 수 있었던 것 같습니다.<br />
                            이르키움 너무도 큰 힘이 되어준 것 같습니다. <br />
                        </p>
                        <footer>진관고등학교  <span>류혜림</span>(은평이르키움학원)</footer>
                    </blockquote>
                    
                </div>
            </div>
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">
                        <ul>
                            <li><a href="/success">합격자 명단</a></li>
                            <li><a href="/success/story">합격자 후기</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>  

    </div>
</section>