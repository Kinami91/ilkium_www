<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">
    <div class="container clearfix">
        <h1>합격자 명단</h1>
        <span>Successful Applicant</span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <!--<li><a href="#">이르키움</a></li>-->
            <li class="active">합격자 명단</li>
        </ol>
    </div>
</section><!-- #page-title end -->


<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">
        <div class="section parallax dark header-stick" style="padding: 70px 0px; background-image: url('/images/main/slide/slide_00.jpg'); background-position:  50% 50%;">
            <div class="container clearfix">
                <div class="row">

                    <div class="col-md-9">
                        <div class="heading-block bottommargin-sm">
                            <h2>이르키움 2017학년도 대입 합격자명단</h2>
                        </div>

                        <h3 class='nobottommargin'>합격을 축하합니다. 그동안 정말 고생많았습니다.</h3>
                    </div>
                </div>
            </div>
        </div>
        <!--        <div class="section parallax bottommargin-sm" style="padding: 0; background-image: url(&quot;http://canvashtml-cdn.semicolonweb.com/images/parallax/3.jpg&quot;); background-position: 50% 72px;" data-stellar-background-ratio="0.3">
                    <div class="heading-block center nobottomborder nobottommargin">
                        <h2>이르키움 2016학년도 합격자명단</h2>
                        <p class="nobottommargin">합격을 축하합니다. 그간 정말 고생많았습니다.</p>
                    </div>
                </div>        -->
        <div class="container clearfix">
            <div class="postcontent col_last nobottommargin clearfix">
                <div id="posts" class="post clearfix table-responsive">

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>이름</th>
                                <th>학교</th>
                                <th>학과</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < count($rows); $i++) {
                                $row = $rows[$i];

                                $row['stud_name'] = preg_replace('/.(?=.$)/u', '○', $row['stud_name']);
                                ?>    
                                <tr>
                                    <td align="center"><?= $i + 1 ?></td>
                                    <td align="center"><?= $row['stud_name'] ?></td>
                                    <td align="center"><?= $row['university'] ?></td>
                                    <td align="center"><?= $row['management'] ?></td>
                                </tr>

                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">
                        <ul>
                            <li><a href="/success">합격자 명단</a></li>
                            <li><a href="/success/story">합격자 후기</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>  

    </div>
</section>