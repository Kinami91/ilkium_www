<?php 

if (!defined('BASEPATH')) 
	exit('No direct script access allowed');

class Error extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// load error config
		$this->load->config('error');
                // load error library    
                $this->load->library('error');		
                
	}
	
	public function index()
	{
		
	}

}

/* End of file error.php */
/* Location: ./application/controllers/error.php */