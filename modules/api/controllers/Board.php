<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Board extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("board/m_board");
        $this->load->model("notice/m_notice");
    }

    public function _remap($method) {
        $p = $this->input->post_get(NULL, true);
//        $p['time'] = date("Y-m-d H:i:s");
//        $val = print_r($p, true);
//        $fp = fopen("/tmp/post_test.txt", "a+");
//        fwrite($fp, $val);
//        fclose($fp);
        
        $result = method_exists($this, $method) ? $this->$method($p) : array('error' => "can't call '$method' method");
        echo json_encode(array($method => $result));
    }
    
    public function boardList($p) {
        if($p['branch_id']) {
            $this->m_board->setBranchId($p['branch_id']);
        }
//        $ret['query'] = $this->db->last_query();
        return $this->m_board->getBoardList($p['field'], $p['keyword'], $p['offset'], $p['limit']);
    }
    
    public function noticeList($p) {
        if($p['branch_id']) {
            $this->m_notice->setBranchId($p['branch_id']);
        }
//        return $p;
//        $ret['query'] = $this->db->last_query();
        return $this->m_notice->noticeList($p['branch_id'], 1, $p['limit']);
        
    }
    
    public function test($p) {
        $ar = $this->input->post_get(NULL, false);
        $ar1 = array(
            "teswt" => 1,
            "test2" => 2
        );
//        return $ar['limit'];
        return $ar;
    }

}

/* End of file Board.php */
/* Location: ./application/controllers/Board.php */