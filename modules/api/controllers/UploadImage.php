<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uploadimage extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'form', 'string'));
    }

    // 파일업르드
    public function index() {
        $uploadDir = "/home/kinami/images/uploads/" . date("Ym") . "/";

        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777, true);
            chmod($uploadDir, 0707);
        }

//        $file = pathinfo($_FILES['userfile']['name']);

        $sFileInfo = '';
        $headers = array();

        foreach ($_SERVER as $k => $v) {
            if (substr($k, 0, 9) == "HTTP_FILE") {
                $k = substr(strtolower($k), 5);
                $headers[$k] = $v;
            }
        }

        $file = new stdClass;
        $file->name = str_replace("\0", "", rawurldecode($headers['file_name']));
        $filename_ext = strtolower(array_pop(explode('.', $file->name)));
        $newName = uniqid().random_string('alnum', 8) . "." . $filename_ext;
        $file->size = $headers['file_size'];
        $file->content = file_get_contents("php://input");

        $allow_file = array("jpg", "png", "bmp", "gif");

        if (!in_array($filename_ext, $allow_file)) {
            echo "NOTALLOW_" . $file->name;
        } else {
            $newPath = $uploadDir . $newName;

            if (file_put_contents($newPath, $file->content)) {
                $imgInfo = getimagesize("http://image.ilkium.co.kr/uploads/" . date("Ym") . "/".  $newName);
                
                $sFileInfo .= "&bNewLine=true";
                $sFileInfo .= "&sFileName=" . $file->name;
                $sFileInfo .= "&sFileURL=http://image.ilkium.co.kr/uploads/" . date("Ym") . "/".  $newName;
                $sFileInfo .= "&nWidth=" . $imgInfo[0];
                $sFileInfo .= "&nHeight=" . $imgInfo[1];
            }

            echo $sFileInfo;
        }


//        $config = array(
//            'upload_path' => $image_upload_folder,
//            'allowed_types' => 'png|jpg|jpeg|bmp|tiff',
//            'max_size' => 8096,
//            'remove_space' => TRUE,
//            'encrypt_name' => FALSE,
//            'max_filename' => 10,
//            'file_name'=> uniqid().random_string('alnum', 8).".".$file['extension']
//        );
////        printr($file);
//        $this->load->library("upload", $config);
//
//        if (!$this->upload->do_upload()) {
//            $upload_error = $this->upload->display_errors();
//            return json_encode($upload_error);
//        } else {
//            $file_info = $this->upload->data();
//            $this->setImagePath($movie_id, $image_id, $file_info['full_path']);
//            return json_encode($file_info);
//        }
    }
    
    /*
     * 프로필 이미지 업로드
     */
    public function boardImageUpload() {
        $this->load->helper(array('url', 'form', 'string'));
        $file = pathinfo($_FILES['upload']['name']);
        $config = array(
            'upload_path' => "/home/kinami/images/uploads/" .date("Ym"),
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => 2048,
            'remove_space' => TRUE,
            'encrypt_name' => FALSE,
//            'max_filename' => 10,
            'file_name'=> uniqid().random_string('alnum', 8).".".$file['extension']
        );
//        printr($file);
        $this->load->library("upload", $config);
        
        $this->upload->initialize($config);

        if (!$this->upload->do_upload("upload")) {
            $upload_error = $this->upload->display_errors('', '');
            echo "<script>alert('업로드 실패 ". $upload_error ."'</script>";
        } else {
            $CKEditorFuncNum = $this->input->get('CKEditorFuncNum');
            $file_info = $this->upload->data();
            $url = "http://image.ilkium.co.kr/uploads/" . date("Ym") . "/" . $file_info['file_name'];
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction('".$CKEditorFuncNum."', '".$url."', '전송에 성공 했습니다')</script>"; 
        }        
    }    


}

/* End of file UploadImage.php */
/* Location: ./application/controllers/UploadImage.php */