<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();


        ini_set('display_errors', 0);
        if (version_compare(PHP_VERSION, '5.3', '>=')) {
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        } else {
            error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
        }
    }

    public function _remap($method) {
        $p = $this->input->post(NULL, true);
        $result = method_exists($this, $method) ? $this->$method($p) : array('error' => "can't call '$method' method");
//        $result['menu'] = $this->getMenu();
//	$this->setLog($method, $p);
        echo json_encode(array($method => $result));
    }

    // 게시판 댓글 추가
    public function addBoardComment($p) {
//        if (!$_SESSION['mem_id'] || !$_SESSION['mem_grade']) {
//            return array("error" => "Not Login");
//        }

        $this->load->model("board/m_board");
        return $this->m_board->addBoardComment($p);
    }

    public function deleteBoardComment($p) {
        $this->load->model("board/m_board");
        
        return $this->m_board->deleteBoardComment($p);
    }
    
    // 공지사항 댓글 추가
    public function addNoticeComment($p) {
//        if (!$_SESSION['mem_id'] || !$_SESSION['mem_grade']) {
//            return array("error" => "Not Login");
//        }

        $this->load->model("notice/m_notice");
        return $this->m_notice->addNoticeComment($p);        
    }
    
    public function delNoticeComment($p) {
        $this->load->model("notice/m_notice");
        
        return $this->m_notice->deleteNoticeComment($p);
    }
    
    public function test() {
        $ar = array(
            "test1"=> "test1 value",
            "test2" => "test2 value"
        );
        
        return $ar;
    }

}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */