<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kcaptcha extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('captcha');
        $this->load->helper('string');
    }

    function session() {
        require(APPPATH . 'config/kcaptcha' . EXT);

        while (TRUE) {
            $keystring = '';
            for ($i = 0; $i < $length; $i++) {
                $keystring .= $allowed_symbols{mt_rand(0, strlen($allowed_symbols) - 1)};
            }
            if (!preg_match('/cp|cb|ck|c6|c9|rn|rm|mm|co|do|cl|db|qp|qb|dp|ww/', $keystring))
                break;
        }
        
        $this->session->set_userdata("captcha_keystring", $keystring);
        $this->kcaptcha->setKeyString($this->session->userdata("captcha_keystring"));
        echo md5($this->kcaptcha->getKeyString());
    }

    public function image() {
//        $this->output->enable_profiler(TRUE);
        $vals = array(
            'word'          => strtoupper(random_string('numeric', 6)),
            'img_path'      => '/home/kinami/www/img/captcha/',
            'img_url'       => "http://".$_SERVER['SERVER_NAME'] . "/img/captcha/",
            "font_path"     => '/home/kinami/www/css/fonts/NanumBarunGothic-Bold.ttf',
            'img_width'     => 150,
            'img_height'    => 50,
            'expiration'    => 3600,
            'word_length'   => 6,
            'font_size'     => 18,
            'img_id'        => 'ImageId',
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(130, 100, 100),
                'grid' => array(240, 40, 40)
            )
        );
        
        $cap = create_captcha($vals);
        
//        printr($cap['image']);
        
        $this->session->set_userdata("captcha_key", $cap['word']);
        $ret = array(
            "filename" => $cap['filename'],
            "word" => md5($this->session->userdata("captcha_key"))
        );
        
        echo json_encode($ret);
        exit;
//        printr($cap);
    }

}

/* End of file Kcaptcha.php */
/* Location: ./application/controllers/Kcaptcha.php */