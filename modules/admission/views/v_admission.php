<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>입학절차</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">입학절차</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container nobottommargin clearfix">
            <div class="fancy-title title-bottom-border">                
                <h3><span>이르키움</span> 입학안내</h3>
            </div>        
            <p>이르키움 독학재수학원은 코칭 경험이 풍부한 강사진이 직접 자신에게 맞는 인터넷 강의, 
                과외식 개인지도까지 맟춤형으로 제시 받을 수 있는 곳입니다. <br />
                한발 앞선 준비가 놀라운 결과를 가져옵니다. 이르키움은 여러분의 성공 수능을 응원합니다.</p>

            <div class="col_one_third">
                <div class="feature-box fbox-center fbox-bg fbox-light fbox-effect">
                    <div class="fbox-icon">
                        <a href="#"><i class="i-alt"> 1</i></a>
                    </div>
                    <h3>나의 약점을 강점으로<span class="subtitle">이르키움만의 수학프로그램으로 대부분의 학생이 어려워하는 수학을 
                            자신의 것으로 만들어 냅니다.</span></h3>
                </div>
            </div>            

            <div class="col_one_third">
                <div class="feature-box fbox-center fbox-bg fbox-border fbox-effect">
                    <div class="fbox-icon">
                        <a href="#"><i>2</i></a>
                    </div>
                    <h3>학습컨설팅을 통한 치밀한 수험준비 
                        <span class="subtitle">이르키움만의 1:1 맞춤식 학습계획 및 전략수립을 통해 성적향상의 지름길이 되어드립니다.</span></h3>
                </div>
            </div>

            <div class="col_one_third col_last">
                <div class="feature-box fbox-center fbox-bg fbox-outline fbox-dark fbox-effect">
                    <div class="fbox-icon">
                        <a href="#"><i class="i-alt">3</i></a>
                    </div>
                    <h3>성적향상의 첨병인 질의응답 시스템
                        <span class="subtitle">이르키움에서는 재수종합반 강의 10년 이상의 경험을 가진 강사들이
                            수능 성공 비법을 전수합니다.</span></h3>
                </div>
            </div>
            <div class="clear"></div>
            <div class="fancy-title title-dotted-border">
                <h3>Process (최초등록시)</h3>
            </div>

            <div class="text-center">
                <img src="/images/main/admission-01.png" alt="" />
            </div>

            <div class="fancy-title title-dotted-border">
                <h3>Process (등록후)</h3>
            </div>

            <div class="text-center">
                <img src="/images/main/admission-02.png" alt="" />
            </div>            
            <!--<img src="/images/main/admission.jpg" alt="" />-->
        </div>
        <div class="container clearfix nobottommargin">
            <div class="promo promo-dark promo-flat bottommargin">
                <h3>재수,N수를 준비하는 학생이라면 이르키움과 함께 해보세요!</h3>
                <span>오로지 독학재수관리만 해온 이르키움만의 노하우로 수험생들의 성공을 위해 끝까지 함께하겠습니다.</span>
                <a href="/branch" class="button button-dark button-xlarge button-rounded">지점 방문</a>
            </div>               
        </div>
    </div>
</section>