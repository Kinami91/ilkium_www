<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>재수관리반</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">입학안내</a></li>
            <li class="active">재수관리반</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section id="slider" class="boxed-slider" style="margin-bottom: 0px; padding-top: 0px;">

    <div class="container clear-fix">
        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <!--#01-->
                <div class="swiper-slide" style="background-image: url('/images/main/slide/slide_0001.jpg');">
                    <div class="container  clearfix ">
                        <div class="slider-caption slider-caption-center dark">
                            <h1 data-caption-animate="fadeInUp" data-caption-delay="300" style="font-size: 2.5em; font-weight: 800;">재수정규관리반</h1>
                            <p data-caption-animate="fadeInDown" data-caption-delay="200" style="font-size: 1.4em; font-weight: 500;">너의 현재가 너의 미래다! <br>“<span style="color:#ffff66;">더 나은 나</span>”로 바꾸기 위한 개인밀착 수능 프로젝트!!</p>
                            <p data-caption-animate="fadeInDown" data-caption-delay="200"><br /><br />결국은 내 자신이 모든 문제에 대한 해답을 가지고 있다.</p>
                            <p data-caption-animate="fadeInUp" data-caption-delay="300" style="color:#66cc66;">성공적인 재수의 시작은 내 자신에 대한 문제점을 찾는 것부터....</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>
<section id="content" class="container">
    <div class="clear"></div>
    <div class="tabs tabs-bb clearfix">
        <ul class="tab-nav clearfix">
            <li><a href="#winter1">모집요강</a></li>
            <li><a href="#winter2">학습관리시스템</a></li>
            <li><a href="#winter3">시간표</a></li>
        </ul>
        <div class="tab-container clearfix">
            <div class="tab-content container clearfix" id="winter1">
                <table class="table table-bordered table-hovered">
                    <colgroup>
                        <col width="25%"/>
                        <col />
                    </colgroup>
                    <tr>
                        <th>개강일자</th>
                        <td>2018년 1월 8일(월)</td>
                    </tr>
                    <tr>
                        <th>모집기간</th>
                        <td>2018년 1월 8일(월)부터 상시모집</td>
                    </tr>
                    <tr>
                        <th>모집대상</th>
                        <td>2019학년도 대입지원 자격을 갖춘 수험생</td>
                    </tr>
                    <tr>
                        <th>필요서류</th>
                        <td>2018학년도 수능 성적표 또는 9월 평가원 모의고사 성적표</td>
                    </tr>
                    <tr>
                        <th>입학원서접수</th>
                        <td>방문접수, 전화예약접수</td>
                    </tr>
                </table>
                <div class="col_half">
                    <h5>필수준비물</h5>
                    필기도구 및 각종 학습도구, 학습교재, 이어폰 or 헤드폰, 세면도구, 물컵
                </div>
                <div class="col_half column">
                    <h5>선택준비물</h5>
                    PMP, TABLET, 개인노트북, 전자사전, 책꽂이, 방석, 실내화, 무릎담요 등
                </div>
            </div>
            <div class="tab-content container clearfix" id="winter2">
                <div class="col_half">
                    <div class="col_full">
                        <h4>나의 위치파악</h4>
                        <p>
                            입시의 모든 시작은 나의 위치 파악이다. 나의 위치를 모른다면 가장 기본적인 위치를 만드는 것이 우선되어야 한다.
                        </p>
                    </div>   
                    <div class="col_full">
                        <h4>나만의 약점 공략</h4>
                        <p>
                            해야할 것보다 더 중요한 것은 하지 않았던 것들을 내 것으로 만드는 것이 입시의 시작이다. 이르키움 재수관리반의 핵심은 지난 시간 동안 놓쳤던 것들을 바로 잡는 것으로 시작
                        </p>
                    </div>     
                    <div class="col_full">
                        <h4>STUDY POSITIONING SYSTEM(S.P.S.)</h4>
                        <p>
                            이르키움의 2018년 재수관리반 프로젝트 명 : STUDY POSITIONING SYSTEM(S.P.S.)    
                        </p>
                    </div>                     

                </div>
                <div class="col_half col_last">
                    <img src="/images/main/SPS_system.png" alt="" class="responsive-image"/>
                </div>


            </div>
            <div class="tab-content container clearfix" id="winter3">
                <table class="table table-bordered">
                    <tr class="text-center bg-primary">
                        <th class="text-center">교시</th>
                        <th class="text-center">시간</th>
                        <th class="text-center">월</th>
                        <th class="text-center">화</th>
                        <th class="text-center">수</th>
                        <th class="text-center">목</th>
                        <th class="text-center">금</th>
                        <th class="text-center">토</th>
                    </tr>
                    <tr>
                        <td class="text-center">0</td>
                        <td class="text-center">08:00 ~ 08:30</td>
                        <td colspan="6">등원체크/어휘테스트/일간계획표 작성/개인정비</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">1</td>
                        <td class="text-center">08:40 ~ 10:00</td>
                        <td colspan="6">국어자기주도학습 및 개인별 선택인강(현강포함) / 과목별 질의응답</td>
                    </tr>
                    <tr>
                        <td class="text-center">2</td>
                        <td class="text-center">10:20 ~ 12:00</td>
                        <td colspan="6">수학자기주도학습 및 개인별 선택인강(현강포함) / 과목별 질의응답</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">점심</td>
                        <td class="text-center">12:00 ~ 13:00</td>
                        <td colspan="6">점심식사 / 어휘 재시험 실시(미통과자)</td>
                    </tr>
                    <tr>
                        <td class="text-center">3</td>
                        <td class="text-center">13:10 ~ 14:20</td>
                        <td colspan="6">수능영어듣기(EBS교재중심)/영어자기주도학습/개별인강</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">4</td>
                        <td class="text-center">14:40 ~ 16:10</td>
                        <td colspan="6">질의응답 / 개별인강 / 자기주도학습 / 현강</td>
                    </tr>
                    <tr>
                        <td class="text-center">5</td>
                        <td class="text-center">16:30 ~ 18:00</td>
                        <td colspan="6">학과상담 / 질의응답 / 특강 / 개인별 단과</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">저녁</td>
                        <td class="text-center">18:00 ~ 19:00</td>
                        <td colspan="5">저녁식사 및 휴식</td>
                        <td rowspan="3">일과종료</td>
                    </tr>
                    <tr>
                        <td class="text-center">6</td>
                        <td class="text-center">19:00 ~ 20:20</td>
                        <td colspan="5">과목별 학과상담 / 성적상담 / 자기주도학습 / 특강</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">7</td>
                        <td class="text-center">20:40 ~ 22:00</td>
                        <td colspan="5">자기주도학습 / 상담 / 개인별 선택인강 / 개인별 일일평가</td>
                    </tr>
                </table>
                <h4>* 국어 1회 / 수학 3회 / 영어 2회 주간 테스트 실시</h4>
            </div>
        </div>
    </div>    
</section>

