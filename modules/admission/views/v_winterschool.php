<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>2018 윈터스쿨</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">2018 윈터스쿨</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap header-stick">

        <div class="container nobottommargin clearfix">
            <div class="entry-image nobottommargin">
                <img src="/images/main/2018-winterschool.png" alt="" />
            </div>
            <div class="clear"></div>
            <div class="tabs tab-nav tabs-alt tabs-justify clearfix">
                <ul class="tab-nav tab-nav2 clearfix">
                    <li><a href="#winter1">윈터스쿨 소개</a></li>
                    <li><a href="#winter2">관리시스템</a></li>
                    <li><a href="#winter3">모집요강</a></li>
                    <li><a href="#winter4">윈터스쿨 시간표</a></li>
                </ul>
                <div class="tab-container clearfix">
                    <div class="tab-content container clearfix" id="winter1">
                        <h3>2018 이르키움 윈터스쿨</h3>
                        <blockquote class="quote">
                            <p>결국은 내 자신이 모든 문제에 대한 답을 가지고 있다.</p>
                        </blockquote>

                        <div class="fancy-title title-dotted-border title-center">
                            <h3><span>Who</span> am I? <span>Where</span> am I?</h3>
                        </div>
                        <p>
                            <span class="dropcap">대</span>입 성공의 시작은 학습량과 무의미한 강압적 관리가 아닙니다.<br/>
                            원하는 대학 합격의 중심은 나의 출발점이 어디인 지를 제대로 알고 있어야만 합니다.<br/>
                            지난 학년의 부족한 부분을 찾아 메우지 않고 진도 완성에 초점을 맞춘 무리한 학습량의 증대는 
                            출발점을 잃게 하고 성적 상승이 일어나지 않아 실패에 직면합니다. <br/><br/>
                            이르키움의 윈터스쿨은 <span class="text-danger" style="font-weight: bold">본인의 위치를 찾아주고 만들어 주는 것을 최우선</span>으로 합니다.<br/>
                            해야할 것보다 더 중요한 것은 <span class="text-danger" style="font-weight: bold">하지 않았던 것들을 내 것으로 만드는 것이 성공 입시의 시작</span>>입니다.<br/>
                            이것이 이르키움의 수년간의 경험으로 만들어 낸 <span class="highlight">S.P.S(Study Positioning System)</span> 입니다.<br/>

                        </p>
                    </div>
                    <div class="tab-content container clearfix" id="winter2">
                        <h3>SPS 프로그램을 통한 밀착관리</h3>
                        <div class="entry-image">
                            <img src="/images/main/2018-winterschool-1.png" alt="" />
                        </div>
                    </div>
                    <div class="tab-content container clearfix" id="winter3">
                        <h3>2018 윈터스쿨 모집요강</h3>
                        <table class="table table-hover table-bordered">
                            <tr>
                                <th class="bg-info">개강일자</th>
                                <td>2017년 12월 26일(화) 1차 개강<br>
                                    2018년 1월 2일(화) 2차 개강</td>
                            </tr>
                            <tr>
                                <th class="bg-info">학습기간</th>
                                <td>
                                    2017년 12월 26일 ~ 2018년 2월 28일(학원별로 상이)
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-info">모집대상</th>
                                <td>
                                    예비 고2(현 고1), 고3(현 고2)
                                </td>
                            </tr>
                            <tr>
                                <th class="bg-info">필요서류</th>
                                <td>
                                    2017년 11월 전국학력평가 모의고사 성적표 및 내신 평균 등급
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-content container clearfix" id="winter4">
                        <h3>2018 윈터스쿨 시간표</h3>
                        <table class="table table-bordered">
                            <tr class="text-center bg-primary">
                                <th class="text-center">교시</th>
                                <th class="text-center">시간</th>
                                <th class="text-center">월</th>
                                <th class="text-center">화</th>
                                <th class="text-center">수</th>
                                <th class="text-center">목</th>
                                <th class="text-center">금</th>
                                <th class="text-center">토</th>
                            </tr>
                            <tr>
                                <td class="text-center">0</td>
                                <td class="text-center">08:00 ~ 08:30</td>
                                <td colspan="6">등원체크/어휘테스트/일간계획표 작성/개인정비</td>
                            </tr>
                            <tr class="active">
                                <td class="text-center">1</td>
                                <td class="text-center">08:40 ~ 10:00</td>
                                <td colspan="6">국어자기주도학습 및 개인별 선택인강(현강포함) / 과목별 질의응답</td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td class="text-center">10:20 ~ 12:00</td>
                                <td colspan="6">수학자기주도학습 및 개인별 선탠ㄱ인강(현강포함) / 과목별 질의응답</td>
                            </tr>
                            <tr class="active">
                                <td class="text-center">점심</td>
                                <td class="text-center">12:00 ~ 13:00</td>
                                <td colspan="6">점심식사 / 어휘 재시험 실시(미통과자)</td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td class="text-center">13:10 ~ 14:20</td>
                                <td colspan="6">수능영어듣기(EBS교재중심)/영어자기주도학습/개별인강</td>
                            </tr>
                            <tr class="active">
                                <td class="text-center">4</td>
                                <td class="text-center">14:40 ~ 16:10</td>
                                <td colspan="6">질의응답 / 개별인강 / 자기주도학습 / 현강</td>
                            </tr>
                            <tr>
                                <td class="text-center">5</td>
                                <td class="text-center">16:30 ~ 18:00</td>
                                <td colspan="6">학과상담 / 질의응답 / 특강 / 개인별 단과</td>
                            </tr>
                            <tr class="active">
                                <td class="text-center">저녁</td>
                                <td class="text-center">18:00 ~ 19:00</td>
                                <td colspan="5">저녁식사 및 휴식</td>
                                <td rowspan="3">일과종료</td>
                            </tr>
                            <tr>
                                <td class="text-center">6</td>
                                <td class="text-center">19:00 ~ 20:20</td>
                                <td colspan="5">과목별 학과상담 / 성적상담 / 자기주도학습 / 특강</td>
                            </tr>
                            <tr class="active">
                                <td class="text-center">7</td>
                                <td class="text-center">20:40 ~ 22:00</td>
                                <td colspan="5">자기주도학습 / 상담 / 개인별 선택인강 / 개인별 일일평가</td>
                            </tr>
                        </table>
                        <h4>* 국어 1회 / 수학 3회 / 영어 2회 주간 테스트 실시</h4>
                    </div>
                </div>
            </div>
        </div> 

    </div>
    <div class="content-wrap">
        <div class="container">

        </div>
    </div>

</section>
<section>
    <div class="promo promo-dark promo-flat nobottommargin">
        <h3>올 겨울방학을 알차게 보내려고 하는 학생이라면 이르키움과 함께 해보세요!</h3>
        <span>이르키움만의 노하우로 성공적인 겨울방학을 보낼 수 있게 최선을 하겠습니다.</span>
        <a href="/branch" class="button button-dark button-xlarge button-rounded">지점 방문</a>
    </div>
</section>    