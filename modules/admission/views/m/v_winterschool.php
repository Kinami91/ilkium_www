<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>2017 윈터스쿨</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">2017 윈터스쿨</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container nobottommargin clearfix">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10">
                <div class="entry-image">
                    <img src="/images/main/2017-winterschool.png" alt="" />
                </div>
            </div>
            <div class="col-md-1">&nbsp;</div>
            <div class="clear"></div>
        </div> 

    </div>
    <div class="promo promo-dark promo-flat nobottommargin">
        <h3>올 겨울방학을 알차게 보내려고 하는 학생이라면 이르키움과 함께 해보세요!</h3>
        <span>이르키움만의 노하우로 성공적인 겨울방학을 보낼 수 있게 최선을 하겠습니다.</span>
        <a href="/branch" class="button button-dark button-xlarge button-rounded">지점 방문</a>
    </div>               
    
</section>
