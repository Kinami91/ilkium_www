<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admission extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if(BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_admission");
            $this->load->view("inc/v_footer");        
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_admission");
            $this->load->view("inc/m/v_footer");              
        }
    }
    
    public function main() {
        $this->load->view("inc/v_header");
        $this->load->view("v_main");
        $this->load->view("inc/v_footer");        
    }
    
    public function winterschool() {
        $this->load->view("inc/v_header");
        $this->load->view("v_winterschool");
        $this->load->view("inc/v_footer");        
    }

}

/* End of file Admission.php */
/* Location: ./application/controllers/Admission.php */