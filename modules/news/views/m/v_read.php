<?php
//$branch_id = $this->uri->segment(3);
$pk_id = $this->uri->segment(3);
?>
<div class="content board-title no-bottom header-clear half-top">
    <h4><?= $row['title'] ?></h4>
    <div class="board-sub">
        <?= $row['writer'] ?> | <?= $row['ins_date'] ?> | 조회수 : <?= $row['hits'] ?>
    </div>
</div>    
<div class="content half-top" style="min-height: 200px;">
    <?= $row['content'] ?>
</div>
<div class="clear"></div>
