<?php
//$branch_id = $this->uri->segment(3);
$pk_id = $this->uri->segment(3);
?>

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 소식</h1>
        <span>Lastest News</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움 소식</a></li>
            <li class="active">공지사항</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container nobottommargin clearfix">
            <div class="clearfix col_last">
                <!--<div class="heading-block">-->                
                <h3><span id="linkNotice">공지사항</span></h3>
                <!--</div>-->        
            <!--            <p>이르키움 독학재수학원은 코칭 경험이 풍부한 강사진이 직접 자신에게 맞는 인터넷 강의, 
                    과외식 개인지도까지 맟춤형으로 제시 받을 수 있는 곳입니다. <br />
                    한발 앞선 준비가 놀라운 결과를 가져옵니다. 이르키움은 여러분의 성공 수능을 응원합니다.</p>-->
                <div class="table-responsive">
                    <table class="table table-condensed table-hovered">
                        <thead>
                            <tr>
                                <th style="text-align: left !important;"><?= $row['title'] ?></th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%" style="margin-bottom: 0;">
                                        <tr>
                                            <td style="font-size:1.4rem;"><?= $row['writer'] ?></td>
                                            <td style="font-size:1.4rem;"><?= $row['ins_date'] ?></td>
                                            <td style="font-size:1.4rem;">조회수 : <?= $row['hits'] ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: none;"><div class="col-xs-12">
                                        <?= $row['content'] ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>

                        <tfoot>    
                            <tr>
                                <td style="border-top:none;">
                                    <div class="col-xs-6">
                                        <?php
                                        if ($row['admin_id'] == $_SESSION['mem_id']) {
                                            ?>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <!--<button class="button button-3d button-rounded button-amber" id="btnWrite" onclick="location.href = '/notice/write/<?= $branch_id ?>'">새글</button>-->
                                        <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/notice/noticeList/<?= $branch_id ?>'" >목록</button>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4>Comments <span><!--(<?= count($cmt_list) ?>)</span>--></h4>

                    <ol class="commentlist clearfix" id="commentList">
                        <?php
                        for ($i = 0; $i < count($cmt_list); $i++) {
                            $comment = $cmt_list[$i];
                            ?>
                            <li class="comment even thread-even depth-1" id="li-comment-<?= $comment['pk_id'] ?>">
                                <div id="comment-1" class="comment-wrap clearfix">
                                    <div class="comment-content clearfix">
                                        <div class="comment-author"><?= $comment['writer'] ?><span><?= $comment['ins_date'] ?></span></div>
                                        <p><?= nl2br($comment['comment']); ?></p>
                                        <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="<?= $comment['pk_id'] ?>" data-backdrop="static" data-keyboard="false" href="#myModal"><i class="icon-remove"></i></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>                        
                            </li>
                            <?php
                        }
                        ?>
                    </ol>
                    <div class="commentlist">
                        <div class="item-body form-inline">
                            <form method="post" action="" id="commentAdd">
                                <div class="form-inline">
                                    이름 : <input type="text" name="comment_writer" id="comment_writer" class="form-control input-small"/>
                                    비밀번호 : <input type="password" name="comment_pwd" id="comment_pwd" class="form-control input-small" />
                                </div>
                                <div class="form-inline">
                                    그림의 숫자를 먼저 입력하세요.
                                    <img src='/images/load_kcaptcha.gif' style='width: 150px; height: 35px; border: 0; margin: 5px 0;' id='imgCaptcha'/>
                                    <input type="text" name="secret_letter" id="secret_letter" class="form-control input-small"/>
                                </div>              
                                <div class="form-inline">
                                    <textarea name="comment_body" id="comment_body" cols="90" rows="3" class="form-control"></textarea>

                                    <button class="btn btn-warning" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnCommentSubmit" disabled>등록</button>
                                </div>
                            </form>
                        </div>
                    </div>        
                </div>
                <div id="myModal" class="modal" role="dialog" aria-labelledby="test">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">삭제하시겠습니까??</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" name="comment_id" value=""/>
                                비밀번호를 입력해주세요.
                                <input type="password" name="comment_delete_pwd" id="comment_delete_pwd" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="btnCommentDelete">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    var notice_id = <?= $pk_id ? $pk_id : "null" ?>;
                </script>
            </div><!-- .postcontent end -->

            <!--            <div class="sidebar nobottommargin clearfix">
                            
                        </div> .sidebar end -->

        </div>
</section>
<script type="text/javascript">
    var norobot_val = null;
//    $("#imgCaptcha").on("click", function () {
//        $.getJSON("/api/kcaptcha/image", function (r) {
//            $("#imgCaptcha").attr("src", "http://image.ilkium.co.kr/captcha/" + r.filename);
//            norobot_val = r.word;
//        });
//    });
    $("#imgCaptcha").on("click", function () {
        $.getJSON("/api/kcaptcha/image", function (r) {
            $("#imgCaptcha").attr("src", "/img/captcha/" + r.filename);
            norobot_val = r.word;
        });
    });
    $("#imgCaptcha").trigger('click');

    $("#secret_letter").on("keyup", function () {
        var md5_key = hex_md5($(this).val());

        if (md5_key == norobot_val) {

            $("#btnSubmit, #btnCommentSubmit").addClass("btn-primary").attr("disabled", false);
        } else {
            $("#btnSubmit, #btnCommentSubmit").removeClass("btn-primary").attr("disabled", true);
        }
    });

    $("#btnCommentSubmit").on("click", function (e) {
        e.preventDefault();

        Util.api("addNoticeComment", {
            notice_id: notice_id,
            writer: $("#comment_writer").val(),
            comment_pwd: $("#comment_pwd").val(),
            comment: $("#comment_body").val()
        }, function (r) {
            console.log(r);
            insertCommentTemplate(r);
        });
    });

    var insertCommentTemplate = function (data) {
        var $str = $([
            '<li class="comment even thread-even depth-1" id="li-comment-1">',
            '    <div id="comment-1" class="comment-wrap clearfix">',
            '        <div class="comment-content clearfix">',
            '            <div class="comment-author">' + data.writer + '<span>' + data.ins_date + '</span></div>',
            '            <p> ' + data.comment + '</p>',
            '            <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="' + data.pk_id + '" data-backdrop="static" data-keyboard="false" href="#myModal"><i class="icon-remove"></i></a>',
            '        </div>',
            '        <div class="clear"></div>',
            '    </div>',
            '</li>'
        ].join("\n"));

        $str.prependTo($("#commentList"));
        $("#imgCaptcha").trigger("click");
        $("#comment_writer, #comment_pwd, #comment_body, #secret_letter").val("");
        location.hash = '#linkNotice';
    };

    $(".confirm-delete").click(function (e) {
        e.preventDefault();

//        $("#myModal").data('id', id).modal('show');
        $("#myModal").on("shown.bs.modal", function (d) {
            var id = $(d.relatedTarget).data("id");
            console.log(id);
            $(d.currentTarget).find('input[name="comment_id"]').val(id);
            $(".modal").css('display', 'block');
        });
    });

    $("#btnCommentDelete").on("click", function (e) {
        e.preventDefault();
//        console.log($("input[name='comment_id']").val());
        var comment_id = $("input[name='comment_id']").val();
        Util.api("delNoticeComment", {
            comment_id: comment_id,
            comment_pwd: $("input[name='comment_delete_pwd']").val()
        }, function (r) {
            console.log(r);
            if (r == "E00") {
                $("input[name='comment_delete_pwd']").val("");
                $("#myModal").modal('hide');
                $("#li-comment-" + comment_id).remove();
            } else {

            }
        });
    });

</script>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/md5.js"></script>