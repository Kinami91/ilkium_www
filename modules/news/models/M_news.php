<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_news extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getNews($pk_id) {
        return $this->db->from("i_common.b_news")
                        ->where("pk_id", $pk_id)
                        ->get()->row_array();
    }

    public function increaseHits($pk_id) {
        $this->load->helper("cookie");
        $cookie_idx = $pk_id . "-ilkiumNews";
        $cookie = array(
            "name" => $cookie_idx,
            "value" => "read",
            "expire" => 86400,
            "domain" => ".ilkium.co.kr",
            "path" => "/"
            );
        if (!get_cookie($cookie_idx)) {
            $this->db->set("hits", "hits + 1", false)
                    ->where("pk_id", $pk_id)
                    ->update("i_common.b_news");
            set_cookie($cookie);
        } 
    }

    public function newsCount($field = null, $keyword = null) {
        if ($field && $keyword)
            $this->db->like($field, $keyword);

        return $this->db->from("i_common.b_news")
                        ->where("title != ''")
                        ->where("notice_yn", "N")
                        ->count_all_results();
    }

    public function newsList($page = null, $limit = 20, $field = null, $keyword = null) {
        if (!$page) $page = 1;
        $offset = ($page - 1) * $limit;

        if ($field && $keyword)
            $this->db->like($field, $keyword);

        $this->db->select("pk_id, notice_no, title, writer, notice_no, hits")
                ->select("DATE(ins_date) ins_date", false);
        $rows = $this->db->from("i_common.b_news")
                        ->where("notice_yn", "N")
                        ->where("title != ''")
                        ->where("delete_yn", "N")
                        ->order_by("pk_id", "desc")
                        ->limit($limit, $offset)
                        ->get()->result_array();

        return $rows;
    }
    
    public function noticeNotice() {
        $this->db->select("*");
        return $this->db->from("i_common.b_news")
                ->where("notice_yn", "Y")
                ->where("delete_yn", "N")
                ->get()->result_array();
        
    }
    
    /*
     * Comment 관련 Methods
     */
    public function getNoticeComment($pk_id) {
        $row = $this->db->from("i_common.b_news_comment")
                ->where("pk_id", $pk_id)
                ->get()->row_array();
        $row['comment'] = nl2br($row['comment']);
        
        return $row;
    }
    
    public function addNoticeComment($p) {
        $sno = $this->getCommentSortNo($p['notice_id']);

        $addData = array(
            "notice_id"=>$p['notice_id'],
            "sort_no"=>$sno,
            "sort_depth"=>0,
            "writer"=>$p['writer'],
            "comment_pwd"=>$p['comment_pwd'],
            "comment"=>$p['comment'],
            "ins_date" => date("Y-m-d H:i:s"),
            "upd_date" => date("Y-m-d H:i:s")
        );
        
        $this->db->set($addData);
        
        $this->db->insert("i_common.b_news_comment");
       
        $pk_id = $this->db->insert_id();
        $this->updateCommentCount($p['notice_id']);
        
        return $this->getNoticeComment($pk_id);
    }
    
    public function addNoticeRelyComment($p) {
        $sno = $this->getCommentSortNo(null, $p['pk_id']);
        
        $this->db->set("sort_no", "sort_no + 1", false)
                ->where("notice_id", $p['notice_id'])
                ->where("sort_no >= " . $sno['sort_no'])
                ->update("i_common.b_news_comment");
                 
        $addData = array(
            "notice_id"=>$p['notice_id'],
            "sort_no"=> $sno['sort_no'] ,
            "sort_depth"=>$sno['sort_depth'],
            "writer"=>$p['writer'],
            "comment_pwd"=>$p['comment_pwd'],
            "comment"=>$p['comment'],
            "ins_date" => date("Y-m-d H:i:s"),
            "upd_date" => date("Y-m-d H:i:s")
        );
        
        $this->db->set($addData);
        
        $this->db->insert("i_common.b_news_comment");        
        
        $this->updateCommentCount($p['notice_id']);        
    }
    
    public function getCommentSortNo($notice_id, $comment_id=null) {
        if($comment_id) {
            $sno = $this->db->select("sort_no, sort_depth")
                    ->from("i_common.b_news_comment")
                    ->where("pk_id", $comment_id)
                    ->get()->row_array();
            return $sno;
        } 
        if($notice_id) {
            $sno = $this->db->select("MAX(sort_no) + 1 AS sno", false)
                    ->from("i_common.b_news_comment")
                    ->where("notice_id", $notice_id)
                    ->get()->row_array();
            return $sno['sno'] ? $sno['sno'] : 1;
        }
        
        
    }
    
    public function updateNoticeComment($p) {
        if($this->confirmCommentPwd($p['pk_id'], $p['comment_pwd'])) {
            $updData = array(
               "comment"=>$p['comment'],
               "upd_date" => date("Y-m-d H:i:s")
           );

           $this->db->set($updData);   

           $this->db->update("i_common.b_news_comment")
                   ->where("pk_id", $p['pk_id']);
           $this->updateCommentCount($p['notice_id']);
        } else {
            return "E99"; // 비밀번호 틀림
        }
    }
    
    public function deleteNoticeComment($p) {
        if($this->confirmCommentPwd($p['comment_id'], $p['comment_pwd'])) {
            $this->db->set("delete_yn", "Y")
                    ->where("pk_id", $p['comment_id'])
                    ->update("i_common.b_news_comment");
            $this->updateCommentCount($p['comment_id']);
            return "E00";
        } else {
            return "E99"; // 비밀번호 틀림
        }
    }
    
    public function confirmCommentPwd($pk_id, $comment_pwd) {
        $cnt = $this->db->from("i_common.b_news_comment")
                ->where("pk_id", $pk_id)
                ->where("comment_pwd", $comment_pwd)
                ->count_all_results();
        
        return $cnt > 0 ? true : false;
    }
    
    public function getNoticeCommentList($notice_id) {
        $rows = $this->db->from("i_common.b_news_comment")
                ->where("notice_id", $notice_id)
                ->where("delete_yn", "N")
                ->order_by("sort_no", "DESC")
                ->get()->result_array();
        
        return $rows;
    }
    
    private function updateCommentCount($notice_id) {
        $cnt = $this->db->from("i_common.b_news_comment")
                ->where("notice_id", $notice_id)
                ->count_all_results();
        
        $this->db->set("comment_count", $cnt)
                ->where("pk_id", $notice_id)
                ->update("i_common.b_board");
    }        

}

/* End of file M_notice.php */
/* Location: ./application/models/M_notice.php */