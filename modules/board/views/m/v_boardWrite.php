<div class="page-title-strip header-clear no-bottom">
    <h3>Q&amp;A</h3>
</div>

<div class="content no-bottom half-top">
    <form action="/board/boardAct" id="boardWriteForm" method="post">
    <div class="one-third">
        지점선택
    </div>
    <div class="two-third last-column">
        <select name="branch_id" id="branch_id" class="input-text-box">
            <option value=""></option>
            <?php
            for ($i = 0; $i < count($branchList); $i++) {
                ?>
                <option value="<?= $branchList[$i]['branch_id'] ?>"><?= $branchList[$i]['branch_name'] ?></option>
                <?php
            }
            ?>
        </select>        
    </div>
    <div class="clear"></div>
    <div class="one-half-responsive">
        <input type="text" name="title" id="title" class='input-text-box' placeholder="제목을 입력해주세요" value="<?= $row['title'] ?>" required/>
    </div>
    <div class="clear"></div>
    <div class="one-half">
        <input type="text" name="writer" id="writer" placeholder="작성자 이름" value="<?= $row['writer'] ?>" class="input-text-box" required/>
    </div>
    <div class="one-half last-column">
        <input type="password" name="board_pwd" id="board_pwd" placeholder="4자리이상 비밀번호" class="input-text-box" required/>
    </div>
    <div class="clear"></div>

    <div class="one-half-responsive" style="min-height: 200px;">
        <textarea name="boardContent" id="boardContent" cols="30" rows="10" class="contactTextarea" required><?= $row['content'] ?></textarea>
    </div>
    <div class="clear"></div>

    <div class="one-half">
        <img src='/assets/images/load_kcaptcha.gif' style='width: 150px; height: 35px; border: 0; margin: 5px 0;' id='imgCaptcha'/>
    </div>
    <div class="one-half last-column">
        <input type="text" name="secret_letter" id="secret_letter" placeholder="그림의 숫자를 먼저 입력하세요." class="input-text-box"/>
        <input type="hidden" name="pk_id" value="<?= $row['pk_id'] ?>" />
    </div>
    <div class="clear"></div>
    <div class="one-half-responsive text-center">
        <button class="button button-green" id="btnList" onclick="location.href = '/board/boardList'" >목록</button>
        <button class="button " id="btnSubmit" type="submit" disabled>쓰기</button>
    </div>    
    </form>
</div>
<script type="text/javascript" src="/assets/scripts//jquery.validate.min.js"></script>
<!--<script type="text/javascript" src="/js/board.js"></script>-->
<script type="text/javascript" src="/assets/scripts/ckeditor/ckeditor.js"></script>


</section><!-- #content end -->        
<script type="text/javascript">
    var branch_id = "<?= $info['branch_id'] ?>";
    CKEDITOR.replace('boardContent', {
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
            {name: 'document', groups: ['document', 'doctools']},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
//            {name: 'clipboard', groups: ['clipboard', 'undo']},
//            {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
            {name: 'forms', groups: ['forms']},
            '/',
//            {name: 'styles', groups: ['styles']},
//            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
//            {name: 'links', groups: ['links']},
//            {name: 'insert', groups: ['insert']},
//            {name: 'colors', groups: ['colors']},
//            {name: 'tools', groups: ['tools']},
//            {name: 'others', groups: ['others']},
//            {name: 'about', groups: ['about']}
        ],
        removeButtons: 'Templates,Save,NewPage,Preview,Print,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,PageBreak,Iframe,About,Language,Checkbox',
        filebrowserUploadUrl: "/api/uploadImage/boardImageUpload"
    });

    var norobot_val = null;
    $("#imgCaptcha").on("click", function () {
        $.getJSON("/api/kcaptcha/image", function (r) {
            console.log(r);
            $("#imgCaptcha").attr("src", "/img/captcha/" + r.filename);
            norobot_val = r.word;
        });
    });
    $("#imgCaptcha").trigger('click');

    $("#secret_letter").on("keyup", function () {
        var md5_key = hex_md5($(this).val());

        if (md5_key == norobot_val) {

            $("#btnSubmit, #btnCommentSubmit").addClass("button-blue").attr("disabled", false);
        } else {
            $("#btnSubmit, #btnCommentSubmit").removeClass("button-blue").attr("disabled", true);
        }
    });

    $("#boardWriteForm").validate({
        ignore: [],
        debug: false,
        rules: {
            branch_id: {
                required: true
            },
            title: {
                required: true
            },
            writer: {
                required: true
            },
            board_pwd: {
                required: true,
                minlength: 4
            },
            boardContent: {
                required: function () {
                    CKEDITOR.instances.boardContent.updateElement()
                }
            }

        }
    });


    $("#btnSubmit").on("click", function (e) {
        $("#boardWriteForm").submit();
    });


</script>
<script type="text/javascript" src="/assets/scripts//app.js"></script>
<script type="text/javascript" src="/assets/scripts//md5.js"></script>