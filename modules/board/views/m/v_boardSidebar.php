<?php
$arUrl = $this->uri->segment_array();
$url = $arUrl[2] != "faqs" ? $arUrl[1] : $arUrl[2];
?>
<!-- Page Sub Menu
============================================= -->
<div id="page-menu">

    <div id="page-menu-wrap">

        <div class="container clearfix">

            <div class="menu-title">SUB MENU</div>

            <nav>
                <ul>
                    <li><a href="/notice/noticeList" <?= $url == "notice" ? "class='toast-title'" : ""?>><div>공지사항</div></a></li>
                    <li><a href="/board/boardList" <?= $url == "board" ? "class='toast-title'" : ""?>><div>묻고 답하기</div></a></li>
                    <li><a href="/notice/faqs" <?= $url == "faqs" ? "class='toast-title'" : ""?>><div>FAQ</div></a></li>

                </ul>
            </nav>
            <div id="page-submenu-trigger"><i class="icon-reorder"></i></div>
        </div>
    </div>
</div><!-- #page-menu end -->
<!--
            <div class="sidebar nobottommargin clearfix hidden-xs">
                <div class="sidebar-widgets-wrap">

                    <div class="widget widget_links">

                        <h4>sub menu</h4>
                        <ul>
                            <li><a href="/notice/noticeList" <?= $url == "notice" ? "class='toast-title'" : ""?>><div>공지사항</div></a></li>
                            <li><a href="/board/boardList" <?= $url == "board" ? "class='toast-title'" : ""?>><div>묻고 답하기</div></a></li>
                            <li><a href="/notice/faqs" <?= $url == "faqs" ? "class='toast-title'" : ""?>><div>FAQ</div></a></li>
                        </ul>

                    </div>

                </div>
            </div>
            <div class="visible-xs clearfix">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                         Brand and toggle get grouped for better mobile display 
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-9">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">SUB MENU</a>
                        </div>
                
                         Collect the nav links, forms, and other content for toggling 
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-9">
                            <ul class="nav navbar-nav">
                            <li <?= $url == "notice" ? "class='active'" : ""?>><a href="/notice/noticeList"><div>공지사항</div></a></li>
                            <li <?= $url == "board" ? "class='active'" : ""?>><a href="/board/boardList"><div>묻고 답하기</div></a></li>
                            <li <?= $url == "faqs" ? "class='active'" : ""?>><a href="/notice/faqs"><div>FAQ</div></a></li>
                            </ul>
                        </div> /.navbar-collapse 
                    </div> /.container-fluid 
                </nav>
            </div>-->