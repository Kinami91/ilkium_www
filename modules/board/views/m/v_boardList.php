<div class="page-title-strip header-clear no-bottom">
    <h3>Q&amp;A</h3>
</div>
<div class="content no-bottom half-top">
    <ul class="board_list">
        <?php
        if ($rows) {
//                                printr($rows[0]);
            for ($i = 0; $i < count($rows); $i++) {
                $row = $rows[$i];
                $offset = ($page - 1) * $limit;
                $no = $totalcnt - $offset - $i;
                $row['ins_date'] = substr($row['ins_date'], 0, 10);

                $isNew = (time() - strtotime($row['ins_date'])) < (86400 * 3) ? "<span class=\"label label-warning\">New</span>" : "";
//                                    $link = "/notice/read/" . $this->m_notice->branch_id . "/" . $row['pk_id'];
                $link = "/board/boardRead/" . $row['pk_id'];
                $badge = $row['comment_count'] > 0 ? "<span class=\"badge\">" . $row['comment_count'] . "</span>" : "";
                ?>
                <li class="board-item">
                    <a href="/board/boardRead/<?=$row['pk_id']?>" class="item-title"><?=$row['title']?><?=$isNew?></a>
                    <div class="item-cate"><?=$row['branch_name']?></div>
                    <div class="item-sub">
                        <span class="item-writer"><?=$row['writer']?></span> |
                        <span class="item-date"><?=$row['ins_date']?></span> | 
                        조회수 : <span class="item-hits"><?=$row['hits']?></span> |
                        댓글 : <span class="item-comment"><?=$row['comment_count']?></span>
                    </div>
                </li>        

           <?php
            }
            ?>
        <?php
    } else {
        ?>
                <li class="board-item">
                    <div class="item-no-content">등록된 글이 없습니다.</div>
                </li>
        <?php
    }
    ?>        
    </ul>
    <a href="/board/boardWrite" class="button button-xs button-yellow">New</a>
</div>
<div class="content text-center">
    <?= $pagination ?>
</div>

