<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/board.js"></script>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 소식</h1>
        <span>Lastest News</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움 소식</a></li>
            <li class="active">묻고 답하기</li>
            <!--<li><a href="/notice/noticeList">공지사항</a></li>-->
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap" style="padding-top: 50px !important;">
        <div class="container clearfix">
            <div class="nobottommargin col_last">
                <div class="fancy-title title-border">
                    <h2>Q & A<span style="font-size: 16px;font-weight: 400;font-style: italic;color: #aaa;padding-left: 15px;"><?= $info['branch_name'] ?></span></h2>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <form action="/board/boardAct" id="boardWriteForm" method="post">
                        <div class="row padding-top-10px form-group">
                            <div class="col-md-2">지점선택(*)</div>
                            <div class="col-md-3">
                                <select name="branch_id" id="branch_id" class="form-control input-small">
                                    <option value=""></option>
                                    <?php
                                    for($i=0; $i<count($branchList); $i++) {
                                        $selected = $branchList[$i]['branch_id'] == $row['branch_id'] ? "selected" : "";
                                    ?>
                                    <option value="<?=$branchList[$i]['branch_id']?>" <?=$selected?>><?=$branchList[$i]['branch_name']?></option>
                                    
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>                           
                        <div class="row padding-top-10px form-group">
                            <div class="col-md-2">제목(*)</div>
                            <div class="col-md-10">
                                <input type="text" name="title" id="title" class='form-control' placeholder="제목을 입력해주세요" value="<?=$row['title']?>" required/>
                            </div>
                        </div>
                        <div class="row padding-top-10px">
                            <div class="col-md-2">이름(*)</div>
                            <div class="col-md-4 form-group"><input type="text" name="writer" id="writer" placeholder="작성자 이름" value="<?=$row['writer']?>" class="form-control input-small" required/></div>
                            <div class="col-md-2">비밀번호(*)</div>
                            <div class="col-md-4 form-group"><input type="password" name="board_pwd" id="board_pwd" placeholder="4자리이상 비밀번호" class="form-control input-small" data-msg="Please fill this field" required/></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="boardContent" id="boardContent" cols="30" rows="15" class="form-control" required><?=$row['content']?></textarea>
                                <input type="hidden" name="pk_id" value="<?=$row['pk_id']?>"/>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnSubmit" type="submit">수정</button>
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/board/boardList'" >목록</button>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="sidebar nobottommargin notopmargin">
            </div>-->

        </div>

    </div>

</section><!-- #content end -->        
<script type="text/javascript">
    var branch_id = "<?=$info['branch_id']?>";
    CKEDITOR.replace('boardContent', {
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
            {name: 'document', groups: ['mode', 'document', 'doctools']},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
            {name: 'forms', groups: ['forms']},
            '/',
            {name: 'styles', groups: ['styles']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
            {name: 'links', groups: ['links']},
            {name: 'insert', groups: ['insert']},
            {name: 'colors', groups: ['colors']},
            {name: 'tools', groups: ['tools']},
            {name: 'others', groups: ['others']},
            {name: 'about', groups: ['about']}
        ],
        removeButtons: 'Templates,Save,NewPage,Preview,Print,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,PageBreak,Iframe,About,Language,Checkbox',
        filebrowserUploadUrl: "/api/uploadImage/boardImageUpload"
    });

    $("#boardWriteForm").validate({
        ignore: [],
        debug: false,
        rules: {
            branch_id: {
                required: true
            },
            title: {
                required: true
            },
            writer: {
                required: true
            },
            board_pwd: {
                required: true,
                minlength: 4
            },
            boardContent: {
                required: function () {
                    CKEDITOR.instances.boardContent.updateElement();
                }
            }
            
        }
    });
    
   
    $("#btnSubmit").on("click", function (e) {
        $("#boardWriteForm").submit();
    });
    
    
</script>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/md5.js"></script>