<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<!--<script type="text/javascript" src="/js/board.js"></script>-->
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 소식</h1>
        <span>Lastest News</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움 소식</a></li>
            <li class="active">묻고 답하기</li>
            <!--<li><a href="/notice/noticeList">공지사항</a></li>-->
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container nobottommargin clearfix">
            <div class="clearfix col_last">
                <!--<div class="heading-block">-->                
                <h3><span>묻고 답하기</span></h3>

                <div class="clear"></div>
                <div class="row">
                    <form action="/board/boardAct" id="boardWriteForm" method="post">
                        <div class="row padding-top-10px form-group">
                            <div class="col-md-2">지점선택(*)</div>
                            <div class="col-md-3">
                                <select name="branch_id" id="branch_id" class="form-control input-small">
                                    <option value=""></option>
                                    <?php
                                    for($i=0; $i<count($branchList); $i++) {
                                    ?>
                                    <option value="<?=$branchList[$i]['branch_id']?>"><?=$branchList[$i]['branch_name']?></option>
                                    
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>                        
                        <div class="row padding-top-10px form-group">
                            <div class="col-md-2">제목(*)</div>
                            <div class="col-md-10">
                                <input type="text" name="title" id="title" class='form-control' placeholder="제목을 입력해주세요" value="<?=$row['title']?>" required/>
                            </div>
                        </div>
                        <div class="row padding-top-10px">
                            <div class="col-md-2">이름(*)</div>
                            <div class="col-md-4 form-group"><input type="text" name="writer" id="writer" placeholder="작성자 이름" value="<?=$row['writer']?>" class="form-control input-small" required/></div>
                            <div class="col-md-2">비밀번호(*)</div>
                            <div class="col-md-4 form-group"><input type="password" name="board_pwd" id="board_pwd" placeholder="4자리이상 비밀번호" class="form-control input-small" required/></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="boardContent" id="boardContent" cols="30" rows="10" class="form-control" required><?=$row['content']?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <img src='/images/load_kcaptcha.gif' style='width: 150px; height: 50px; border: 0' id='imgCaptcha'/>
                            </div>
                            <div class="col-md-5">
                                <input type="text" name="secret_letter" id="secret_letter" class="form-control input-xsmall pull-left" />
                                <div class="text-primary">(좌측의 숫자를 올바르게 입력하면 글쓰기 버튼이 활성화됩니다.)</div>
                            </div>
                            <div class="col-md-4 center-align">
                                <input type="hidden" name="pk_id" value="<?=$row['pk_id']?>" />
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/board/boardList'" >목록</button>
                            <button class="button button-3d button-rounded button-leaf" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnSubmit" type="submit" disabled>쓰기</button>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="sidebar nobottommargin notopmargin">
                
            </div>-->

        </div>

    </div>

</section><!-- #content end -->        
<script type="text/javascript">
    var branch_id = "<?=$info['branch_id']?>";
    CKEDITOR.replace('boardContent', {
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
            {name: 'document', groups: ['mode', 'document', 'doctools']},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
            {name: 'forms', groups: ['forms']},
            '/',
            {name: 'styles', groups: ['styles']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
            {name: 'links', groups: ['links']},
            {name: 'insert', groups: ['insert']},
            {name: 'colors', groups: ['colors']},
            {name: 'tools', groups: ['tools']},
            {name: 'others', groups: ['others']},
            {name: 'about', groups: ['about']}
        ],
        removeButtons: 'Templates,Save,NewPage,Preview,Print,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,PageBreak,Iframe,About,Language,Checkbox',
        filebrowserUploadUrl: "/api/uploadImage/boardImageUpload"
    });

    var norobot_val = null;
    $("#imgCaptcha").on("click", function () {
        $.getJSON("/api/kcaptcha/image", function (r) {
            console.log(r);
            $("#imgCaptcha").attr("src", "/img/captcha/" + r.filename);
            norobot_val = r.word;
        });
    });
    $("#imgCaptcha").trigger('click');

    $("#secret_letter").on("keyup", function () {
        var md5_key = hex_md5($(this).val());

        if (md5_key == norobot_val) {

            $("#btnSubmit, #btnCommentSubmit").removeClass("button-leaf").addClass("button-amber").attr("disabled", false);
        } else {
            $("#btnSubmit, #btnCommentSubmit").removeClass("button-amber").addClass("button-leaf").attr("disabled", true);
        }
    });

    $("#boardWriteForm").validate({
        ignore: [],
        debug: false,
        rules: {
            branch_id: {
                required: true
            },
            title: {
                required: true
            },
            writer: {
                required: true
            },
            board_pwd: {
                required: true,
                minlength: 4
            },
            boardContent: {
                required: function () {
                    CKEDITOR.instances.boardContent.updateElement()
                }
            }
            
        }
    });
    
   
    $("#btnSubmit").on("click", function (e) {
        $("#boardWriteForm").submit();
    });
    
    
</script>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/md5.js"></script>