<?php
$branch_id = $this->uri->segment(3);
$pk_id = $this->uri->segment(4);
?>
<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 소식</h1>
        <span>Lastest News</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움 소식</a></li>
            <li class="active">묻고 답하기</li>
            <!--<li><a href="/notice/noticeList">공지사항</a></li>-->
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<!-- Content
============================================= -->
<link rel="stylesheet" href="/css/nivo-slider.css" />
<script type="text/javascript" src="/js/jquery.nivo.js"></script>
<section id="content">

    <div class="content-wrap" style="padding-top: 50px !important;">
        <div class="container clearfix">
            <div class="nobottommargin col_last">

                <div class="fancy-title title-border">
                    <h2>Q & A<span style="font-size: 16px;font-weight: 400;font-style: italic;color: #aaa;padding-left: 15px;"><?= $info['branch_name'] ?></span></h2>
                </div>

                <div class="clear"></div>

                <div class="table-responsive">
                    <table class="table table-condensed table-hovered">
                        <thead>
                            <tr>
                                <th style="text-align: left !important;"><span class="label label-warning"><?= $row['branch_name'] ?>  </span>  <?= $row['title'] ?></th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%" style="margin-bottom: 0;">
                                        <tr>
                                            <td style="font-size:1.4rem;"><?= $row['writer'] ?></td>
                                            <td style="font-size:1.4rem;"><?= $row['ins_date'] ?></td>
                                            <td style="font-size:1.4rem;">조회수 : <?= $row['hits'] ?></td>
                                        </tr>
                                    </table>
                                    <!--                                    <div class="col_one_fifth"></div>
                                                                        <div class="col_two_fifth font-sm">입력일 : </div>
                                                                        <div class="col_two_fifth col_last"></div>-->
                                </td>
                            </tr>
                            <tr>
                                <td><div class="col-xs-12" style="min-height: 200px;">
                                        <?= $row['content'] ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>

                        <tfoot>    
                            <tr>
                                <td>
                                    <div class="col-xs-6">
                                        <button class="button button-3d button-rounded button-aqua" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnModify" onclick="location.href = '/board/boardModify/<?= $row['pk_id'] ?>'">수정</button>
                                        <button class="button button-3d button-rounded button-aqua" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnDelete" onclick="location.href = '/board/boardDelete/<?= $row['pk_id'] ?>'">삭제</button>

                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <!--<button class="button button-3d button-rounded button-amber" id="btnWrite" onclick="location.href = '/notice/write/<?= $branch_id ?>'">새글</button>-->
                                        <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/board/boardList'" >목록</button>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4>Comments <span><!--(<?= count($cmt_list) ?>)</span>--></h4>

                    <ol class="commentlist clearfix" id="commentList">
                        <?php
                        for ($i = 0; $i < count($cmt_list); $i++) {
                            $comment = $cmt_list[$i];
                            ?>
                            <li class="comment even thread-even depth-1" id="li-comment-<?= $comment['pk_id'] ?>">
                                <div id="comment-1" class="comment-wrap clearfix">
                                    <div class="comment-content clearfix">
                                        <div class="comment-author"><?= $comment['writer'] ?><span><?= $comment['ins_date'] ?></span></div>
                                        <p><?= nl2br($comment['comment']); ?></p>
                                        <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="<?= $comment['pk_id'] ?>" 
                                           data-backdrop="static" data-keyboard="false"  data-target="#myModal"><i class="icon-remove"></i></a>
    <!--                                        <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="<?= $comment['pk_id'] ?>" 
                                           data-target=".bs-example-modal-sm"><i class="icon-remove"></i></a>                                           -->

                                    </div>
                                    <div class="clear"></div>
                                </div>                        
                            </li>
                            <?php
                        }
                        ?>
                    </ol>
                    <div class="commentlist">
                        <div class="item-body form-inline">
                            <form method="post" action="" id="commentAdd">
                                <div class="form-inline">
                                    이름 : <input type="text" name="comment_writer" id="comment_writer" class="form-control input-small"/>
                                    비밀번호 : <input type="password" name="comment_pwd" id="comment_pwd" class="form-control input-small" />
                                </div>
                                <div class="form-inline">
                                    그림의 숫자를 먼저 입력하세요.
                                    <img src='/images/load_kcaptcha.gif' style='width: 150px; height: 35px; border: 0; margin: 5px 0;' id='imgCaptcha'/>
                                    <input type="text" name="secret_letter" id="secret_letter" class="form-control input-small"/>
                                </div>              
                                <div class="form-inline">
                                    <textarea name="comment_body" id="comment_body" cols="90" rows="3" class="form-control"></textarea>

                                    <button class="btn btn-warning" id="btnCommentSubmit" disabled>등록</button>
                                </div>
                            </form>
                        </div>
                    </div>        
                </div>
                <div id="myModal" class="modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">삭제하시겠습니까??</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" name="comment_id" value=""/>
                                <label for="comment_delete_pwd">비밀번호를 입력해주세요.</label>
                                <input type="password" name="comment_delete_pwd" id="comment_delete_pwd" />
                                <label class="error" style="display:none;">비밀번호가 일치하지 않습니다. </label>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="btnCommentDelete">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>
                
                                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Modal Heading</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="nobottommargin">Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, 
                                                        vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla 
                                                        non metus auctor fringilla.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                -->

                <script type="text/javascript">
                    var notice_id = <?= $pk_id ? $pk_id : "null" ?>;
                </script>                    
            </div>


        </div>

    </div>

</section><!-- #content end -->        

<script type="text/javascript">
    var norobot_val = null,
            board_id = <?= $row['pk_id'] ? $row['pk_id'] : "null" ?>;
    $("#imgCaptcha").on("click", function () {
        $.getJSON("/api/kcaptcha/image", function (r) {
            $("#imgCaptcha").attr("src", "/img/captcha/" + r.filename);
            norobot_val = r.word;
        });
    });
    $("#imgCaptcha").trigger('click');

    $("#secret_letter").on("keyup", function () {
        var md5_key = hex_md5($(this).val());

        if (md5_key == norobot_val) {

            $("#btnSubmit, #btnCommentSubmit").addClass("btn-primary").attr("disabled", false);
        } else {
            $("#btnSubmit, #btnCommentSubmit").removeClass("btn-primary").attr("disabled", true);
        }
    });

    $("#btnCommentSubmit").on("click", function (e) {
        e.preventDefault();

        Util.api("addBoardComment", {
            board_id: board_id,
            writer: $("#comment_writer").val(),
            comment_pwd: $("#comment_pwd").val(),
            comment: $("#comment_body").val()
        }, function (r) {
            console.log(r);
            insertCommentTemplate(r);
        });
    });

    var insertCommentTemplate = function (data) {
        var $str = $([
            '<li class="comment even thread-even depth-1" id="li-comment-1">',
            '    <div id="comment-1" class="comment-wrap clearfix">',
            '        <div class="comment-content clearfix">',
            '            <div class="comment-author">' + data.writer + '<span>' + data.ins_date + '</span></div>',
            '            <p> ' + data.comment + '</p>',
            '            <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="' + data.pk_id + '" data-backdrop="static" data-keyboard="false"><i class="icon-remove"></i></a>',
            '        </div>',
            '        <div class="clear"></div>',
            '    </div>',
            '</li>'
        ].join("\n"));

        $str.prependTo($("#commentList"));
        $("#imgCaptcha").trigger("click");
        $("#comment_writer, #comment_pwd, #comment_body, #secret_letter").val("");
        location.hash = '#linkNotice';
    };

    $(".confirm-delete").click(function (e) {
        e.preventDefault();
//        alert("tttt");
        $("#myModal").modal('show');
//        $("#myModal").css('display', 'block');
//        
        $("#myModal").on("shown.bs.modal", function (d) {
            $(this).css('display', 'block');
            var id = $(d.relatedTarget).data("id");
            console.log(id);
            $(d.currentTarget).find('input[name="comment_id"]').val(id);
//            $(".modal").css('display', 'block');
//            
        });
    });

    $("#btnCommentDelete").on("click", function (e) {
        e.preventDefault();
//        console.log($("input[name='comment_id']").val());
        var comment_id = $("input[name='comment_id']").val();
        Util.api("deleteBoardComment", {
            comment_id: comment_id,
            comment_pwd: $("input[name='comment_delete_pwd']").val()
        }, function (r) {
            console.log(r);
            if (r == "E00") {
                $("input[name='comment_delete_pwd']").val("");
                $("#myModal").modal('hide');
                $("#li-comment-" + comment_id).remove();
            } else {
                $(".modal-body label.error").text("비밀번호가 일치하지 않습니다.").show();
            }
        });
    });

</script>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/md5.js"></script>