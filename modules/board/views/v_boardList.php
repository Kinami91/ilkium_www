<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 소식</h1>
        <span>Lastest News</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움 소식</a></li>
            <li class="active">묻고 답하기</li>
            <!--<li><a href="/notice/noticeList">공지사항</a></li>-->
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container nobottommargin clearfix">


            <div class="clearfix col_last">
                <!--<div class="heading-block">-->                
                <h3><span>묻고 답하기</span></h3>
                <!--</div>-->        
            <!--            <p>이르키움 독학재수학원은 코칭 경험이 풍부한 강사진이 직접 자신에게 맞는 인터넷 강의, 
                    과외식 개인지도까지 맟춤형으로 제시 받을 수 있는 곳입니다. <br />
                    한발 앞선 준비가 놀라운 결과를 가져옵니다. 이르키움은 여러분의 성공 수능을 응원합니다.</p>-->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <colgroup>
                            <col width='5%'/>
                            <col width='15%'/>
                            <col/>
                            <col width='15%'/>
                            <col width='15%'/>
                            <col width='10%'/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>지점명</th>
                                <th>제목</th>
                                <th>글쓴이</th>
                                <th>날짜</th>
                                <th>조회수</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < count($notices); $i++) {
                                $notice = $notices[$i];

                                $notice['ins_date'] = substr($notice['ins_date'], 0, 10);
                                $link = "/notice/read/" . $this->m_notice->branch_id . "/" . $notice['pk_id'];
                                $isNew = (time() - strtotime($notice['ins_date'])) < (86400 * 3) ? "<span class=\"label label-warning\">New</span>" : "";
                                ?>
                                <tr class='info'>
                                    <td class='text-center'><span class="label label-warning">공지</span></td>
                                    <td><a href="<?= $link ?>"><?= $notice['title'] ?></a> &nbsp; <?= $isNew ?></td>
                                    <td class='text-center'><?= $notice['ins_date'] ?></td>
                                    <td class='text-center'><?= $notice['hits'] ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <?php
                            if ($rows) {
//                                printr($rows[0]);
                                for ($i = 0; $i < count($rows); $i++) {
                                    $row = $rows[$i];
                                    $offset = ($page - 1) * $limit;
                                    $no = $totalcnt - $offset - $i;
                                    $row['ins_date'] = substr($row['ins_date'], 0, 10);

                                    $isNew = (time() - strtotime($row['ins_date'])) < (86400 * 3) ? "<span class=\"label label-warning\">New</span>" : "";
//                                    $link = "/notice/read/" . $this->m_notice->branch_id . "/" . $row['pk_id'];
                                    $link = "/board/boardRead/" . $row['pk_id'];
                                    $badge = $row['comment_count'] > 0 ? "<span class=\"badge\">" . $row['comment_count'] . "</span>" : "";
                                    ?>
                                    <tr>
                                        <td class='text-center'><?= $no ?></td>
                                        <td class='text-center'><?= $row['branch_name'] ?></td>
                                        <td><a href="<?= $link ?>"> <?= $row['title'] ?></a> &nbsp; <?= $isNew ?> &nbsp; <?= $badge ?></td>
                                        <td class='text-center'> <?= $row['writer'] ?> </td>
                                        <td class='text-center'> <?= $row['ins_date'] ?> </td>
                                        <td class='text-center'> <?= $row['hits'] ?> </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            <tfoot>
                                <tr>
                                    <td colspan="6" style="border-top: none;"></td>
                                </tr>
                            </tfoot>
                            <?php
                        } else {
                            ?>
                            <tfoot>
                                <tr>
                                    <td class='text-center' colspan='6' style="height: 150px; vertical-align: middle;"> 등록된 글이 없습니다. </td>
                                </tr>
                            </tfoot>
                            <?php
                        }
                        ?>
                        </tbody>

                    </table>
                    <div class="row">
                        <div class="col-md-7 text-right"><?= $pagination ?></div>
                        <div class="col-md-5 text-right">
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnWrite" onclick="location.href = '/board/boardWrite'">새글</button>
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/board/boardList'" >목록</button>
                        </div>
                    </div>
                </div>
            </div><!-- .postcontent end -->



        </div>
</section>
