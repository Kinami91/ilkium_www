<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_board extends CI_Model {

    private $branch_id = null;

    public function __construct() {
        parent::__construct();
    }

    public function getBranchId() {
        return $this->branch_id;
    }

    public function setBranchId($branch_id) {
        $this->branch_id = $branch_id;
    }

    public function writeBoard($p) {
        $addData = array(
            "branch_id" => $this->branch_id,
            "board_no" => $this->getMaxBoardNo(),
            "title" => $p['title'],
            "writer" => $p['writer'],
            "board_pwd" => $p['board_pwd'],
            "content" => $p['boardContent'],
            "hits" => 0,
            "comment_count" => 0,
            "notice_yn" => $p['notice_yn'] ? $p['notice_yn'] : "N",
            "ins_date" => date("Y-m-d H:i:s"),
            "upd_date" => date("Y-m-d H:i:s")
        );

        $this->db->set($addData);

        $this->db->insert("i_common.b_board");
//        echo $this->db->last_query();
        
        if($this->db->affected_rows()) {
            $this->sendSMS($p['branch_id']);
        }

        return $this->db->insert_id();
    }

    public function sendSMS($branch_id) {
        $row = $this->db->select("branch_name, ceo_name, mobile")
                        ->from("i_common.branch")
                        ->where("branch_id", $branch_id)
                        ->get()->row_array();

        $ch = curl_init();

        $message = $row['branch_name'] . " Q&A 게시판에 새글이 등록되었습니다. 확인부탁드립니다.";
        $sender = "01045232495";
        $username = "kinami";
        $recipients = str_replace("-", "", $row['mobile']);
        $key = "gqlxcnSeUjS5D4w";

        $msg = base64_encode(iconv("utf-8", "euc-kr", $message));

        $postvars = "message=" . urlencode($msg)
                . "&sender=" . urlencode($sender)
                . "&username=" . urlencode($username)
                . "&recipients=" . urlencode($recipients)
                . "&key=" . urlencode($key);

        $url = "https://directsend.co.kr/index.php/api/v1/sms";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        
        curl_close($ch);
        
        $ret = json_decode($response, true);
        $ret_msg = $ret['status'] == "0" ? "성공" : "오류";
        
        $this->db->set("branch_id", $this->branch_id)
                ->set("receiver_name", $row['ceo_name'])
                ->set("receiver_no", $row['mobile'])
                ->set("message", $message)
                ->set("sender_no", $sender)
                ->set("send_time", "NOW()", false)
//                ->set("msg_type", $msg_type)
                ->set("send_result", $ret['status'])
                ->set("error_msg", $ret_msg)
                ->set("ins_date", "NOW()", false)
                ->insert("i_common.message_send");
    }

    private function getMaxBoardNo() {
        $query = "SELECT MAX(board_no) + 1 mid FROM i_common.b_board WHERE branch_id='" . $this->branch_id . "'";
        $mid = $this->db->query($query)->row_array();

        return $mid['mid'] ? $mid['mid'] : 1;
    }

    public function updateBoard($p) {
        $updData = array(
            "title" => $p['title'],
            "writer" => $p['writer'],
            "board_pwd" => $p['board_pwd'],
            "content" => $p['boardContent'],
            "notice_yn" => $p['notice_yn'] ? $p['notice_yn'] : "N",
            "upd_date" => date("Y-m-d H:i:s")
        );

        $this->db->set($updData)
                ->where("branch_id", $this->branch_id)
                ->where("pk_id", $p['pk_id'])
                ->update("i_common.b_board");
    }

    public function deleteBoard($pk_id) {
        $this->db->set("delete_yn", "Y")
                ->where("pk_id", $pk_id);
        if ($this->branch_id != "all")
            $this->db->where("branch_id", $this->branch_id);

        $this->db->update("i_common.b_board");
    }

    public function chkArticlePwd($pk_id, $pwd) {
        $this->db->from("i_common.b_board")
                ->where("pk_id", $pk_id)
                ->where("board_pwd", $pwd);
        if ($this->branch_id != "all")
            $this->db->where("branch_id", $this->branch_id);

        $cnt = $this->db->count_all_results();

        return $cnt > 0 ? true : false;
    }

    public function getArticle($pk_id) {
        $this->db->select("*")
                ->select("i_common.BRANCH_NAME(branch_id) branch_name", false)
                ->from("i_common.b_board")
                ->where("pk_id", $pk_id);
        if ($this->branch_id != "all")
            $this->db->where("branch_id", $this->branch_id);
        $row = $this->db->get()->row_array();

//        echo $this->db->last_query();
        $row['cmt_list'] = $this->getBoardCommentList($pk_id);
        return $row;
    }

    public function increaseHits($pk_id) {
        $this->load->helper("cookie");
        $cookie_idx = $this->branch_id . $pk_id . "-ilkiumBoard";
        $cookie = array(
            "name" => $cookie_idx,
            "value" => "read",
            "expire" => 86400,
            "domain" => ".ilkium.co.kr",
            "path" => "/"
        );
        if (!get_cookie($cookie_idx)) {
            $this->db->set("hits", "hits+1", false)
                    ->where("pk_id", $pk_id)
                    ->update("i_common.b_board");
            set_cookie($cookie);
        }
    }

    public function getBaordCnt($field, $keyword) {
        if ($field && $keyword) {
            $this->db->like($field, $keyword);
        }
//        $this->db->from("i_common.b_board")
//                ->where("branch_id", $this->branch_id);
        $this->db->where("notice_yn", "N")
                ->where("delete_yn", "N");

        if ($this->branch_id != "all")
            $this->db->where("branch_id", $this->branch_id);
        $cnt = $this->db->from("i_common.b_board")->count_all_results();

        return $cnt;
    }

    public function getBoardList($field, $keyword, $offset, $limit) {
        if ($field && $keyword) {
            $this->db->like($field, $keyword);
        }

        $this->db->select("pk_id, branch_id, board_no, title, writer, hits, comment_count")
                ->select("DATE(ins_date) ins_date", false)
                ->select("i_common.BRANCH_NAME(branch_id) branch_name", false)
                ->from("i_common.b_board")
                ->where("notice_yn", "N")
                ->where("delete_yn", "N")
                ->order_by("pk_id", "DESC")
                ->limit($limit, $offset);
        if ($this->branch_id != "all")
            $this->db->where("branch_id", $this->branch_id);
        $rows = $this->db->get()->result_array();
//        echo $this->db->last_query();
//        $rows['query'] = $this->db->last_query();
        return $rows;
    }

    public function getBoardNotice() {
        $rows = $this->db->from("i_common.b_board")
                        ->where("branch_id", $this->branch_id)
                        ->where("delete_yn", "N")
                        ->where("notice_yn", "Y")
                        ->get()->result_array();

        return $rows();
    }

    /*
     * Comment 관련 Methods
     */

    public function getBoardComment($pk_id) {
        $row = $this->db->from("i_common.b_board_comment")
                        ->where("pk_id", $pk_id)
                        ->get()->row_array();
        $row['comment'] = nl2br($row['comment']);

        return $row;
    }

    public function addBoardComment($p) {
//        $p['board_id'] = $_SESSION['branch_id'];
        $sno = $this->getCommentSortNo($p['board_id']);

        $addData = array(
            "board_id" => $p['board_id'],
            "sort_no" => $sno,
            "sort_depth" => 0,
            "writer" => $p['writer'],
            "comment_pwd" => $p['comment_pwd'],
            "comment" => $p['comment'],
            "ins_date" => date("Y-m-d H:i:s"),
            "upd_date" => date("Y-m-d H:i:s")
        );

        $this->db->set($addData);

        $this->db->insert("i_common.b_board_comment");

        $pk_id = $this->db->insert_id();
//        console.log($pk_id);
        $this->updateCommentCount($p['board_id']);

        return $this->getBoardComment($pk_id);
    }

    public function addBoardRelyComment($p) {
        $sno = $this->getCommentSortNo(null, $p['pk_id']);

        $this->db->set("sort_no = sort_no + 1", null, false)
                ->where("board_id", $p['board_id'])
                ->where("sort_no >= " . $sno['sort_no'])
                ->update("i_common.b_board_comment");

        $addData = array(
            "board_id" => $p['board_id'],
            "sort_no" => $sno['sort_no'],
            "sort_depth" => $sno['sort_depth'] + 1,
            "writer" => $p['writer'],
            "comment_pwd" => $p['comment_pwd'],
            "comment" => $p['comment'],
            "ins_date" => date("Y-m-d H:i:s"),
            "upd_date" => date("Y-m-d H:i:s")
        );

        $this->db->set($addData);

        $this->db->insert("i_common.b_board_comment");

        $this->updateCommentCount($p['board_id']);
    }

    public function getCommentSortNo($board_id, $comment_id = null) {
        if ($comment_id) {
            $sno = $this->db->select("sort_no, sort_depth")
                            ->from("i_common.b_board_comment")
                            ->where("pk_id", $comment_id)
                            ->get()->row_array();
            return $sno;
        }
        if ($board_id) {
            $sno = $this->db->select("MAX(sort_no) + 1 AS sno", false)
                            ->from("i_common.b_board_comment")
                            ->where("board_id", $board_id)
                            ->get()->row_array();
            return $sno['sno'] ? $sno['sno'] : 1;
        }
    }

    public function updateBoardComment($p) {
        if ($this->confirmCommentPwd($p['pk_id'], $p['comment_pwd'])) {
            $updData = array(
                "comment" => $p['comment'],
                "upd_date" => date("Y-m-d H:i:s")
            );

            $this->db->set($updData);

            $this->db->update("i_common.b_board_comment")
                    ->where("pk_id", $p['pk_id']);
            $this->updateCommentCount($p['board_id']);
        } else {
            return "E99"; // 비밀번호 틀림
        }
    }

    public function deleteBoardComment($p) {
        if ($this->confirmCommentPwd($p['comment_id'], $p['comment_pwd'])) {
            $this->db->set("delete_yn", "Y")
                    ->set("upd_date", "NOW()", false)
                    ->where("pk_id", $p['comment_id'])
                    ->update("i_common.b_board_comment");
            if ($this->db->affected_rows() > 0) {
                $this->updateCommentCount($p['board_id']);
                return "E00";
            }
        } else {
            return "E99"; // 비밀번호 틀림
        }
    }

    public function confirmCommentPwd($pk_id, $comment_pwd) {
        $cnt = $this->db->from("i_common.b_board_comment")
                ->where("pk_id", $pk_id)
                ->where("comment_pwd", $comment_pwd)
                ->count_all_results();

        return $cnt > 0 ? true : false;
    }

    public function getBoardCommentList($board_id) {
        $rows = $this->db->from("i_common.b_board_comment")
                        ->where("board_id", $board_id)
                        ->where("delete_yn", "N")
                        ->order_by("sort_no", "DESC")
                        ->get()->result_array();

        return $rows;
    }

    private function updateCommentCount($board_id) {
        $cnt = $this->db->from("i_common.b_board_comment")
                ->where("board_id", $board_id)
                ->where("delete_yn", "N")
                ->count_all_results();

        $this->db->set("comment_count", $cnt)
                ->where("pk_id", $board_id)
                ->update("i_common.b_board");
    }

}

/* End of file M_board.php */
/* Location: ./application/models/M_board.php */