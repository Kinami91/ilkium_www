<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Board extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model("m_board");
        $this->m_board->setBranchId("all");
    }
    
    public function index() {
        $this->boardList();
    }

    public function boardList($page = 1) {

        $p = $this->input->post();
        $this->m_board->setBranchId("all");

        $data['totalcnt'] = $this->m_board->getBaordCnt($p['field'], $p['keyword']);

        $data['limit'] = 10;
        $offset = ($page - 1) * $data['limit'];
        $data['page'] = $page;

        $data['rows'] = $this->m_board->getBoardList($p['field'], $p['keyword'], $offset, $data['limit']);

        $this->load->library('pagination');
        $config['per_page'] = $data['limit'];
        $config['base_url'] = "/board/boardList/";
        $config['total_rows'] = $data['totalcnt'];
        $this->load->config('pagination', TRUE);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
//        printr($data['pagination']);
        $data['sidebar'] = $this->load->view("v_boardSidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardList", $data);
            $this->load->view("inc/v_footer");
            
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_boardList", $data);
            $this->load->view("inc/m/v_footer");
        }
    }
    
    public function boardWrite() {
        $this->load->model("branch/m_branch");
        $data['branchList'] = $this->m_branch->getBranchSimpleList();
        $data['sidebar'] = $this->load->view("v_boardSidebar", $data, true);
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardWrite", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_boardWrite", $data);
            $this->load->view("inc/m/v_footer");
        }
    }
    
    public function boardAct() {
        $p = $this->input->post();
        
//        printr($p);
//        printr($_SESSION);
//        exit;

        $this->load->model("board/m_board");

        $this->m_board->setBranchId($p['branch_id']);
        if (!$p['pk_id']) {

            if ($p['secret_letter'] == $_SESSION['captcha_key']) {
                $this->m_board->writeBoard($p);
                redirect("http://".$_SERVER['HTTP_HOST']."/board/boardList/");
            } else {
                echo "입력시간이 초과되었거나 잘못된 접근입니다.";
            }
        }

        if ($p['pk_id'] && $p['board_pwd']) {
            if ($this->m_board->chkArticlePwd($p['pk_id'], $p['board_pwd'])) {
                $this->m_board->updateBoard($p);

                redirect("http://".$_SERVER['HTTP_HOST']."/board/boardRead/" . $p['pk_id']);
            } else {
                alert("비밀번호가 일치하지 않습니다.", "");
            }
        }
    }    
    
    public function boardRead($pk_id) {
        $data['row'] = $this->m_board->getArticle($pk_id);
        $this->m_board->increaseHits($pk_id);
        
        $data['cmt_list'] = $this->m_board->getBoardCommentList($pk_id);
        
        $data['sidebar'] = $this->load->view("v_boardSidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardRead", $data);
            $this->load->view("inc/v_footer", $data);        
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_boardRead", $data);
            $this->load->view("inc/m/v_footer", $data);        
        }
    }
    
    public function boardModify($pk_id) {
        $this->load->model("branch/m_branch");
        
        $data['branchList'] = $this->m_branch->getBranchSimpleList();
        $data['row'] = $this->m_board->getArticle($pk_id);

        $data['sidebar'] = $this->load->view("v_boardSidebar", $data, true);
        
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardModify", $data);
            $this->load->view("inc/v_footer", $data);        
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_boardModify", $data);
            $this->load->view("inc/m/v_footer", $data);        
        }
    }
    
    public function boardDelete($pk_id) {
        $this->load->model("m_board");

        $data['row'] = $this->m_board->getArticle($pk_id);

        $data['sidebar'] = $this->load->view("v_boardSidebar", $data, true);
        
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardDelete", $data);
            $this->load->view("inc/v_footer", $data);        
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_boardDelete", $data);
            $this->load->view("inc/m/v_footer", $data);        
        }
    }
    
    public function boardDeleteAct() {
        $this->load->model("m_board");
        
        $p = $this->input->post();

        if($this->m_board->chkArticlePwd($p['pk_id'], $p['board_pwd'])) {
            $this->m_board->deleteBoard($p['pk_id']);
            redirect("http://".$_SERVER['HTTP_HOST']."/board/boardList/");
        } else {
            alert("비밀번호가 일치하지 않습니다.", "");
        }
        
        
        
    }
    
    public function test() {
        $this->output->enable_profiler(TRUE);
        $this->load->model("m_board");
        
        $this->m_board->sendSMS("pchon");
    }
    

}

/* End of file Board.php */
/* Location: ./application/controllers/Board.php */