<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notice extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        
        $this->load->model("m_notice");
        $this->m_notice->setBranchId("all");
    }

    public function index() {
        $this->noticeList();
    }
    
    public function noticeList($page = 1) {
        $p = $this->input->post(null, true);        
//        $this->load->model("m_notice", "main");

        if(!$page) $page = 1;
        $data['totalcnt'] = $this->m_notice->noticeCount($this->m_notice->branch_id, $p['field'], $p['keyword']);
        $data['limit'] = 10;
        $data['start'] = ($page - 1) * $data['limit'];
        $data['page'] = $page;
        
        $data['rows'] = $this->m_notice->noticeList($this->m_notice->branch_id, $page, $data['limit'], $p['field'], $p['keyword']);
        
       
        $this->load->library('pagination');
        $config['per_page'] = $data['limit'];
        $config['base_url'] = "/notice/noticeList/";
        $config['total_rows'] = $data['totalcnt'];
        $this->load->config('pagination', TRUE);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();     
        
        $data['sidebar'] = $this->load->view("board/v_boardSidebar", $data, true);
        
        if(BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_noticeList", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_noticeList", $data);
            $this->load->view("inc/m/v_footer");
        }
    }

    public function read($pk_id) {
        
        $this->m_notice->increaseHits($this->m_notice->branch_id, $pk_id);
        $data['row'] = $this->m_notice->getNotice($pk_id);
        
        $data['cmt_list'] = $this->m_notice->getNoticeCommentList($pk_id);
        
        $data['sidebar'] = $this->load->view("board/v_boardSidebar", $data, true);
        if(BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_read", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_read", $data);
            $this->load->view("inc/m/v_footer");
        }
    }    
    
    public function faqs() {
        $data['sidebar'] = $this->load->view("board/v_boardSidebar", $data, true);
        if(BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_faqs", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_faqs", $data);
            $this->load->view("inc/m/v_footer");
        }
        
    }
}

/* End of file Notice.php */
/* Location: ./application/controllers/Notice.php */