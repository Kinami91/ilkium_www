<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 소식</h1>
        <span>Lastest News</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움 소식</a></li>
            <li class="active">FAQ</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container nobottommargin clearfix">
            <div class="clearfix col_last">
                <div id="faqs" class="faqs">

                    <div id="faqs-list" class="fancy-title title-bottom-border">
                        <h3>Frequently asked questions</h3>
                    </div>

                    <ul class="iconlist faqlist">
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-1">학원비는 얼마인가요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-2">학원 자체 특강(논술)은 있습니까?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-3">반을 나누는 기준이 따로 있나요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-4">학습감독은 어떻게 하나요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-5">재학생이 방학기간 중에 신청가능한가요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-6">중식과 석식은 어떻게 해결하나요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-7">책상은 어떤 종류인가요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-8">셔틀버스는 운행하나요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-9">인터넷강의는 편한 시간에 언제든 들을 수 있나요?</a></strong></li>
                        <li><i class="icon-caret-right"></i><strong><a href="#" data-scrollto="#faq-10">질의응답 선생님은 상주 하시나요?</a></strong></li>
                    </ul>

                    <div class="divider"><i class="icon-circle"></i></div>

                    <h3 id="faq-1"><strong>Q.</strong> 학원비는 얼마인가요?</h3>
                    <p>지역에 따라 지점별로 약간의 차이가 있습니다. 각 지점에 직접 문의하시면 정확한 정보를 알 수 있습니다.</p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-2"><strong>Q.</strong> 학원 자체 특강(논술)은 있습니까?</h3>
                    <p>수강생들이 원하는 경우 특강수업을 진행할 수 있습니다.(3명 이상) 비용은 학원비의 절반 정도로 저렴합니다. </p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-3"><strong>Q.</strong> 반을 나누는 기준이 따로 있나요?</h3>
                    <p>없습니다. 성별에 관계없이 선착순으로 자리를 배치합니다. 하지만 지점에 따라 성별에 대한 구역을 나누기는 합니다. </p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-4"><strong>Q.</strong> 학습감독은 어떻게 하나요?</h3>
                    <p>오전, 오후, 저녁 3부분으로 나뉘어 매 교시 관리 staff들이 방문하여 학습상태를 판단하고 그 결과를 홈페이지에 기록하며, 그 기록을 토대로 매달 학습 및 생활 상담에 대한 기초자료로 활용합니다.</p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-5"><strong>Q.</strong> 재학생이 방학기간 중에 신청가능한가요?</h3>
                    <p>재학생은 겨울방학기간 동안 신청을 할 수 있습니다. 상황에 따라 여름에도 가능하기도 하지만, 지점에 따라 다르므로 지점별로 확인 바랍니다. </p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-6"><strong>Q.</strong>중식과 석식은 어떻게 해결하나요?</h3>
                    <p>중식과 석식은 저희 이르키움 협력법인인 ‘좋은도시락’에서 제공합니다. 가격은 지점별로 약간의 차이는 있으나, 대부분 끼니 당 5,000~5,300원 사이입니다. 식사는 자기 자리에서 먹게 됩니다.</p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-7"><strong>Q.</strong> 책상은 어떤 종류인가요?</h3>
                    <p>이르키움은 도서관처럼 운영되기 때문에 일반 교실책상과는 다른 자체로 특별 제작한 책상이 제공됩니다</p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-8"><strong>Q.</strong>셔틀버스는 운행하나요?</h3>
                    <p>이르키움 전 지점에서 현재 셔틀버스를 운영하는 지점은 한 곳도 없습니다. 죄송합니다. </p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-9"><strong>Q.</strong>인터넷강의는 편한 시간에 언제든 들을 수 있나요?</h3>
                    <p>모든 수업이 인강으로 대체되기 때문에 인강은 언제든지 들을 수 있고, 개인 노트북 혹은 테블릿 PC를 가지고 오셔도 됩니다. </p>

                    <div class="divider divider-right"><a href="#" data-scrollto="#faqs-list"><i class="icon-chevron-up"></i></a></div>

                    <h3 id="faq-10"><strong>Q.</strong> 질의응답 선생님은 상주 하시나요?</h3>
                    <p>각 지점마다 다릅니다. 하지만 대부분의 지점 원장님들이 오랜 시간동안 재종반 및 입시학원에서 종사하시던 분들이라 해당 과목의 질의응답은 언제든지 가능하고, 
                       해당 과목의 선생님들이 안계신 지점들은 과목별 주 2회 혹은 3회 방문을 하시고, 인터넷으로도 질의응답을 받으시기 때문에 질의응답에 전혀 불편함이 없습니다.</p>

                </div>
            </div><!-- .postcontent end -->


        </div>
</section>
