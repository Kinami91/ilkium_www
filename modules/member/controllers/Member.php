<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->helper("text");
        
        $this->load->model("m_member");
        $data['boardList'] = $this->m_member->getBoardAllList();
        $data['noticeList'] = $this->m_member->getNoticeAllList();
        $data['newsList'] = $this->m_member->getNewsAllList();
        
//        printr($data['noticeList']);
        
        if(BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_main", $data);
            $this->load->view("inc/v_footer");            
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_main", $data);
            $this->load->view("inc/m/v_footer");        
            
        }
        

    }
    
    public function index2() {
        $this->load->view("inc/v_header");
        
        $this->load->view("v_main1");
        
        $this->load->view("inc/v_footer");
    }

}

/* End of file Member.php */
/* Location: ./application/controllers/Member.php */