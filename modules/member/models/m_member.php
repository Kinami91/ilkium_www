<?php

/**
 * M_member
 * 
 * Description...
 * 
 * @package m_member
 * @author kinami <kinami91@gmail.com>
 * @version 0.0.0
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_member extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*
     * 관리자 및 선생님 정보 입력
     */
    public function addMember($p) {
        $this->db->set("mem_id", $p['mem_id']);
        $this->db->set("mem_pwd", $p['mem_pwd']);
        $this->db->set("grade", $p['grade']);
        $this->db->set("mem_email", $p['email']);
        $this->db->set("mem_phone", $p['mem_phone']);
        $this->db->set("mem_photo", $p['mem_photo']);
        $this->db->set("ins_date", "NOW()", false);
        $this->db->set("upd_date", "NOW()", false);
        
        $this->db->insert("i_common.member");
    }
    
    /*
     * 관리자 및 선생님 정보 수정
     */
    public function updateMember($p) {
        
        $this->db->set("mem_id", $p['mem_id']);
        $this->db->set("mem_pwd", $p['mem_pwd']);
        $this->db->set("grade", $p['grade']);
        $this->db->set("mem_email", $p['email']);
        $this->db->set("mem_phone", $p['mem_phone']);
        $this->db->set("mem_photo", $p['mem_photo']);
        $this->db->set("upd_date", "NOW()", false);       
        
        $this->db->update("i_common.member")->where("pk_id", $p['pk_id']);
        
    }
    
    /*
     * 관리자 및 선생님 정보 
     */
    public function getMemberInfo($p) {
        $this->db->select("*")
                ->from("i_common.member")
                ->where("pk_id", $p['pk_id']);
        
        $row = $this->db->get()->row_array();
        
        return $row;
    }
    
    /**
     * 관리자 및 선생님 갯수
     */
    public function getMemberCount($keyword) {
        if($keyword) $this->db->where("mem_id", $keyword)
                ->or_like("mem_name", $p['mem_name']);
        
        return $this->db->from("i_common.member")->count_all_results();
    }
    
    /*
     * 관리자 및 선생님 리스트
     */
    public function getMemberList($keyword, $start, $limit) {
        if($keyword) $this->db->where("mem_id", $keyword)
                ->or_like("mem_name", $p['mem_name']);
     
        return $this->db->select("*")
                ->from("i_common.member")
                ->order_by("pk_id", "desc")
                ->limit($limit, $start)
                ->get()->result_array();
    }
    
    /****
     * 지점정보
     */
    public function addBranch($p) {
        $addData = array(
            "branch_id" => $p['branch_id'],
            "branch_name" => $p['branch_name'],
            "branch_code" => $p['branch_code']
        );
        $this->db->set("ins_date", "NOW()", false);
        $this->db->set("upd_date", "NOW()", false);
        $this->db->insert("i_commmon.branch", $addData);
        
    }
    
    /*
     * 지점정보 수정
     */
    public function updateBranchInfo($field, $cont, $pk_id) {
        $this->db->set($field, $cont)
                ->set("upd_date", "NOW()", false);
        
        $this->db->where("pk_id", $pk_id)
                ->update("i_common.branch");
    }
    
    /*
     * 지점정보
     */
    public function getBranchInfo($pk_id=null, $branch_code=null) {
        if($pk_id) $this->db->where("pk_id", $pk_id);
        if($branch_code) $this->db->where("branch_code", $branch_code);
        
        return $this->db->select("*")
                ->from("i_common.branch")
               ->get()->row_array();
    }
    /*
     * 지점리스트
     */
    public function getBranchList($field, $keyword) {
         if($keyword) $this->db->where("mem_id", $keyword)
                ->or_like("mem_name", $p['mem_name']);
        
        return $this->db->from("i_common.member")->count_all_results();       
    }
    
    /*
     * 게시판 전체 리스트
     */
    public function getBoardAllList() {
        $rows = $this->db->from("i_common.b_board")
                        ->where("delete_yn", "N")
                        ->order_by("pk_id", "DESC")
                        ->limit(5)
                        ->get()->result_array();
        return $rows;
    }    
    
    /*
     * 공지사항 전체리스트
     */
    public function getNoticeAllList() {
        $rows = $this->db->from("i_common.b_notice")
                        ->where("delete_yn", "N")
                        ->order_by("pk_id", "DESC")
                        ->limit(5)
                        ->get()->result_array();
        return $rows;
    }    
    
}

/* End of file m_member.php */
/* Location: ./application/models/m_member.php */