<section id="slider" class="slider-parallax swiper_wrapper clearfix">

    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">
            <!--#01-->
            <div class="swiper-slide" style="background-image: url('/images/main/slide/slide_0001.jpg');">
                <div class="container  clearfix ">
                    <div class="slider-caption slider-caption-center dark">
                        <p data-caption-animate="fadeInDown" data-caption-delay="200">너의 현재가 너의 미래다! <br>“<span style="color:#ffff66;">더 나은 나</span>”로 바꾸기 위한 개인밀착 수능 프로젝트!!</p>
                        <p data-caption-animate="fadeInDown" data-caption-delay="200"><br />2019학년도</p>
                        <h1 data-caption-animate="fadeInUp" data-caption-delay="300">재수정규관리반</h1>
                        <p data-caption-animate="fadeInUp" data-caption-delay="300">개강일: 2018년 1월 8일부터 상시모집</p>
                        <p data-caption-animate="fadeInUp" data-caption-delay="300"><a href="/admission/main" class="button button-border button-circle button-dark">상세보기</a></p>
                    </div>
                </div>
            </div>
            <!--            #02 -->
            <div class="swiper-slide dark" style="background-image: url('/images/main/slide/slide_15.jpg'); background-position: center top;">

                <div class="trans_back_all">
                    <div class="container clearfix">
                        <div class="slider-caption slider-caption-center">
                            <h3 data-caption-animate="fadeInUp" data-caption-delay="200">이르키움 정시 배치상담은 이제 모두 끝났습니다.</h3>
                            <h2 data-caption-animate="fadeInUp">수험생 여러분 고생 많으셨습니다.</h2>
                            <h3 data-caption-animate="fadeInUp" data-caption-delay="200">모두의 대입 합격을 기원합니다.</h3>
                        </div>
                    </div>
                </div>
            </div> 
            <!--            #03 -->
            <div class="swiper-slide dark" style="background-image: url('/images/main/slide/slide_21.jpg'); background-position: center top;">

                <div class="trans_back_all">
                    <div class="container clearfix">
                        <div class="slider-caption slider-caption-center">
                            <p data-caption-animate="fadeInUp" data-caption-delay="200">이르키움이 수 년간에 걸쳐 경험한 최고의 재수관리 시스템 </p>
                            <h2 data-caption-animate="fadeInUp">STUDY POSITIONING SYSTEM(S.P.S.)</h2>
                            <p data-caption-animate="fadeInUp" data-caption-delay="200">직접 지점별 학원에서 느껴보세요.</p>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
        <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
        <!-- Add Pagination -->
        <!--<div class="swiper-pagination"></div>-->
<!--<div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div>-->
    </div>

    <script>
        jQuery(document).ready(function ($) {
            swiperSlider = new Swiper('.swiper-parent', {
                paginationClickable: false,
//                pagination: '.swiper-pagination',
                slidesPerView: 1,
                grabCursor: true,
                loop: true,
                autoplay: 5000,
                onSwiperCreated: function (swiper) {
                    $('[data-caption-animate]').each(function () {
                        var $toAnimateElement = $(this);
                        var toAnimateDelay = $(this).attr('data-caption-delay');
                        var toAnimateDelayTime = 0;
                        if (toAnimateDelay) {
                            toAnimateDelayTime = Number(toAnimateDelay) + 750;
                        } else {
                            toAnimateDelayTime = 750;
                        }
                        if (!$toAnimateElement.hasClass('animated')) {
                            $toAnimateElement.addClass('not-animated');
                            var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                            setTimeout(function () {
                                $toAnimateElement.removeClass('not-animated').addClass(elementAnimation + ' animated');
                            }, toAnimateDelayTime);
                        }
                    });
                },
                onSlideChangeStart: function (swiper) {
                    $('#slide-number-current').html(swiper.activeIndex + 1);
                    $('[data-caption-animate]').each(function () {
                        var $toAnimateElement = $(this);
                        var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                        $toAnimateElement.removeClass('animated').removeClass(elementAnimation).addClass('not-animated');
                    });
                },
                onSlideChangeEnd: function (swiper) {
                    $('#slider .swiper-slide').each(function () {
                        if ($(this).find('video').length > 0) {
                            $(this).find('video').get(0).pause();
                        }
                    });
                    $('#slider .swiper-slide:not(".swiper-slide-active")').each(function () {
                        if ($(this).find('video').length > 0) {
                            if ($(this).find('video').get(0).currentTime != 0)
                                $(this).find('video').get(0).currentTime = 0;
                        }
                    });
                    if ($('#slider .swiper-slide.swiper-slide-active').find('video').length > 0) {
                        $('#slider .swiper-slide.swiper-slide-active').find('video').get(0).play();
                    }

                    $('#slider .swiper-slide.swiper-slide-active [data-caption-animate]').each(function () {
                        var $toAnimateElement = $(this);
                        var toAnimateDelay = $(this).attr('data-caption-delay');
                        var toAnimateDelayTime = 0;
                        if (toAnimateDelay) {
                            toAnimateDelayTime = Number(toAnimateDelay) + 300;
                        } else {
                            toAnimateDelayTime = 300;
                        }
                        if (!$toAnimateElement.hasClass('animated')) {
                            $toAnimateElement.addClass('not-animated');
                            var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                            setTimeout(function () {
                                $toAnimateElement.removeClass('not-animated').addClass(elementAnimation + ' animated');
                            }, toAnimateDelayTime);
                        }
                    });
                }
            });

            $('#slider-arrow-left').on('click', function (e) {
                e.preventDefault();
                swiperSlider.swipePrev();
            });

            $('#slider-arrow-right').on('click', function (e) {
                e.preventDefault();
                swiperSlider.swipeNext();
            });

            $('#slide-number-current').html(swiperSlider.activeIndex + 1);
            $('#slide-number-total').html(swiperSlider.slides.length);
        });
    </script>

</section>
<section id="content">
    <div class="container notopmargin nobottommargin nobottompadding">
        <div class="content-wrap">
            <div class="col_half">
                <div class="fancy-title title-bottom-border">
                    <h4>묻고 답하기<span class="label label-default pull-right"><a href="/board/boardList" style="color: #fff !important">More</a></span></h4>
                </div>
                <div class="list-group nobottommargin">
                    <?php
                    for ($i = 0; $i < count($boardList); $i++) {
                        $row = $boardList[$i];
                        $link = "/board/boardRead/" . $row['pk_id'];
                        $row['title'] = character_limiter($row['title'], 22, "...");
                        $branch_name = trim(str_replace("이르키움", "", $row['branch_name']));
                        ?>
                        <li class="list-unstyled"><span class="label label-primary"><?= $branch_name ?></span> 
                            <a href="<?= $link ?>" class="notice-title" style="font-size: 14px;"> <?= $row['title'] ?> </a><span class="notice-date"> <?= substr($row['ins_date'], 0, 10) ?></span></li>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col_half col_last">
                <div class="fancy-title title-bottom-border">
                    <h4>공지사항<span class="label label-default pull-right"><a href="/notice/noticeList" style="color: #fff !important">More</a></span></h4>

                </div>
                <div class="list-group nobottommargin">
                    <?php
                    for ($i = 0; $i < count($noticeList); $i++) {
                        $row = $noticeList[$i];
                        $row['title'] = character_limiter($row['title'], 18, "...");
                        $row['branch_name'] = $row['branch_name'] ? $row['branch_name'] : "메인";
                        $link = "/notice/read/" . $row['pk_id'];
                        $branch_name = trim(str_replace("이르키움", "", $row['branch_name']));
                        ?>
                        <li class="list-unstyled"><span class="label label-primary"><?= $branch_name ?></span> 
                            <a href="<?= $link ?>" class="notice-title" style="font-size: 14px;"><?= $row['title'] ?></a>
                            <span class="notice-date"> <?= substr($row['ins_date'], 0, 10) ?></span></li>
                        <?php
                    }
                    ?>
                </div>                
            </div>
        </div>
    </div>    
    <div class="section notopmargin nobottompadding">
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>독학재수관리학원의 시작과 끝</h2>
                <span>2010년부터 오로지 독학재수관리의 길만 걸어왔습니다.</span>
            </div>

            <!--            <div class="clear bottommargin-sm"></div>
                        <div class="col_one_third nobottommargin">
            
                            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                                <h4>Why choose <span>Us</span>.</h4>
                            </div>
            
                            <p>변화된 대입시험에 대응하기 위해서 </p>
            
                        </div>
                        <div class="col_one_third nobottommargin">
            
                            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                                <h4>What we <span>Do</span>.</h4>
                            </div>
            
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quidem minus id omnis, nam expedita, ea fuga commodi voluptas iusto, hic autem deleniti dolores explicabo labore enim repellat earum perspiciatis.</p>
            
                        </div>
                        <div class="col_one_third col_last nobottommargin">
            
                            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                                <h4>Our <span>Mission</span>.</h4>
                            </div>
            
                            <p>오랜 기간 독학재수관리학원의 경험을 통한 차별화된 생활관리, 성적관리, 입시관리를 통해 수험생들을 대입성공의 길로 이끕니다.</p>
            
                        </div>-->



        </div>
    </div>
    <div class="container clearfix">
        <div class="title-block">
            <h3><span>차별화</span>된 생활<span>관리</span></h3>
            <span>스스로 하는 것은 <b>공부</b> 이르키움과 함께하는 것은 <b>관리</b></span>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-10 col_last">

            <blockquote class="quote">
                <p>이르키움의 생활관리는 상벌이 아닌 자신의 생활패턴을 만들어 나가는데 초점을 맞추어 최선을 다하고 있습니다. 
                    금지사항에서 권장사항에 이르기까지 스스로 지켜나가며 수능전날까지 자기주도적 수험생활을 유지할 수 있도록 
                    이르키움의 선생님들이 항상 같이 합니다.
                </p>
            </blockquote>

            <img src="/images/main/manage_life.png" class="aligncenter fadeIn animated" data-animate="fadeIn">

        </div>
    </div>
    <div class="section">
        <div class="container clearfix">
            <div class="title-block">
                <h3><span>성적향상</span>을 부르는 <span>학습</span>관리</h3>
                <span>인강을 통해 익히는 것은 <b>학(學)</b>, 이르키움과 함께 스스로 하는 것은 <b>습(習)</b></span>
            </div>
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-11 col_last">

                <blockquote class="quote">
                    <p>이르키움의 학습관리는 각 과목에 대한 개인별 수준파악을 시작으로해서 학과별 심층상담과
                        질의응답에 초점을맞추어 학습의 효율성을 강조하여 궁극에는 원하는 대학의 성적에 이를 수 
                        있도록 구성되어 있습니다.</p>
                </blockquote>
                <div class="row">
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect"  data-animate="fadeIn">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-comments"></i></a>
                            </div>
                            <h3>과목별 학과상담
                                <span class="subtitle">과목별 학과 선생님과의 심층상담 후 현위치 분석 및 목표대학 설정</span></h3>
                        </div>
                    </div>                        
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect" data-animate="fadeIn">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt icon-hand-up"></i></a>
                            </div>
                            <h3>강좌선택
                                <span class="subtitle">과목별 선호 강좌 선택 or 추천</span></h3>
                        </div>
                    </div>                        
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect" data-animate="fadeIn">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt icon-calendar"></i></a>
                            </div>
                            <h3>학습계획작성
                                <span class="subtitle">학습플래너 작성 및 점검을 통한 학습량 분석</span></h3>
                        </div>
                    </div>                        
                </div>
                <div class="row">
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect" data-animate="fadeIn">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-comments"></i></a>
                            </div>
                            <h3>질의응답
                                <span class="subtitle">PB질문지 작성제출 or IB질문지 작성 업로드.<br>1:1현장응답 or 인강을 통한 응답지 제공</span></h3>
                        </div>
                    </div>                        
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect" data-animate="fadeIn">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt icon-laptop2"></i></a>
                            </div>
                            <h3>화상질의응답
                                <span class="subtitle">실시간 화상을 통한 질의응답 or 화상학과상담</span></h3>
                        </div>
                    </div>                        
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect" data-animate="fadeIn">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt icon-bar-chart"></i></a>
                            </div>
                            <h3>모의고사
                                <span class="subtitle">매월 모의고사 실시 / 홈페이지 기록분석 성적변화 추적 후 상담실시</span></h3>
                        </div>
                    </div>                           
                </div>
            </div>
        </div>
    </div>
    <div class="container clearfix">
        <div class="title-block">
            <h3><span><b>합격</b></span>으로 이끄는 입시<span><b>관리</b></span></h3>
            <span>스스로 받는 것은 점수, 함께 하는 것이 입시</span>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-11 col_last">

            <blockquote class="quote"><p>이르키움의 성적 및 입시관리는 매월 실시하는 모의고사의 성적을 토대로 
                    성적의 향상여부를 점검하고 <br />그 성적에 따른 맞춤형 성적 및 입시상담이 
                    개별적으로 이루어지며, <br />생활관리와는 달리 매월 필수적으로 1회는 상담이
                    이루어질 수 있도록 구성되어 있습니다.</p></blockquote>

            <div class="row">
                <div class="col_one_third">
                    <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect" data-animate="fadeIn">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-file-text"></i></a>
                        </div>
                        <h3>월모의고사
                            <span class="subtitle">매월 모의고사 실시 후 목표대학 합격 가능성 분석</span></h3>
                    </div>
                </div>                        
                <div class="col_one_third">
                    <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect" data-animate="fadeIn">
                        <div class="fbox-icon">
                            <a href="#"><i class="i-alt icon-flag-checkered"></i></a>
                        </div>
                        <h3>목표대학 재설정
                            <span class="subtitle">다양한 입시자료로 입시상담 후 목표대학 재설정 or 수정</span></h3>
                    </div>
                </div>                        
                <div class="col_one_third col_last">
                    <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect" data-animate="fadeIn">
                        <div class="fbox-icon">
                            <a href="#"><i class="i-alt icon-calendar2"></i></a>
                        </div>
                        <h3>6,9월 평가원 모의고사 실시
                            <span class="subtitle">6,9월 평가원 모의고사 실시 후 부모님과의 입시상담</span></h3>
                    </div>
                </div>                        
            </div>
            <div class="row">
                <div class="col_one_third">
                    <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect" data-animate="fadeIn">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-search3"></i></a>
                        </div>
                        <h3>수시 적합성 분석
                            <span class="subtitle">9월 평가원 모의고사 실시 후 수시적합성 상담</span></h3>
                    </div>
                </div>                        
                <div class="col_one_third">
                    <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect" data-animate="fadeIn">
                        <div class="fbox-icon">
                            <a href="#"><i class="i-alt icon-chart"></i></a>
                        </div>
                        <h3>과목별 분석
                            <span class="subtitle">취약과목 분석 및 학과별 강사와의 밀착상담</span></h3>
                    </div>
                </div>                        
                <div class="col_one_third col_last">
                    <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect" data-animate="fadeIn">
                        <div class="fbox-icon">
                            <a href="#"><i class="i-alt icon-comment-alt"></i></a>
                        </div>
                        <h3>무료정시상담
                            <span class="subtitle">대학수학능력시험 후 무료 정시상담</span></h3>
                    </div>
                </div>                           
            </div>
        </div>

        <div class="clear"></div>

    </div>
    <div class="promo promo-light promo-full topmargin-lg ">
        <div class="container clearfix">
            <h3>재수,N수를 준비하는 학생이라면 이르키움과 함께 해보세요!</h3>
            <span>오로지 독학재수관리만 해온 이르키움만의 노하우로 수험생들의 성공을 위해 끝까지 함께하겠습니다.</span>
            <a href="/branch" class="button button-dark button-xlarge button-rounded">이르키움 지점보기</a>
        </div>
    </div>    

    <!--    <div class="content-wrap">
            <div class="container clearfix">
                <h3>수능일까지</h3>
                <div id="countdown-ex3" class="countdown countdown-large"></div>
                <script>
                    jQuery(document).ready(function ($) {
                        var newDate = new Date('11/17/2016');
                        $('#countdown-ex3').countdown({until: newDate});
                    });
                </script>
            </div>
        </div>-->

</section>

<?php
$this->load->library("user_agent");

//echo $this->agent->browser().' '.$this->agent->version();
?>