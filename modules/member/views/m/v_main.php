<div id="page-content-scroll" class="header-clear"><!--Enables this element to be scrolled --> 

    <div class="content-fullscreen no-bottom">
        <div class="home-fader animate-top animate-time-1000">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="/admission"><img src="/images/main/slide/banner_02.png" class="responsive-image" alt="img"></a>
                </div>
                <div class="swiper-slide">
                    <img src="/images/main/slide/banner_01.png" class="responsive-image" alt="img">
                </div>
                <div class="swiper-slide">
                    <img src="/images/main/slide/banner_03.png" class="responsive-image" alt="img">
                </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div> 
        </div>
     
    </div>
    <div class="background-block background-block-fullscreen no-top no-bottom bg-5">
        <img src="/assets/images/main/why_front.png" class="responsive-image full-bottom animate-top animate-delay-200" alt="" />
        <!--
        <div class="one-half left-padding last-column no-bottom">
            <table class="table no-border  no-bottom">
                <colgroup>
                    <col width="40%"/>
                    <col width="23%"/>
                    <col width="23%"/>
                    <col width="14%"/>
                </colgroup>
                <tr>
                    <td class="no-padding"><img src="/assets/images/main/sky_text.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/4.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/2.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/count.png" alt="" class="responsive-image"/></td>
                </tr>
            </table>
        </div>
        <div class="one-half left-padding last-column no-bottom">
            <table class="table no-border  no-bottom">
                <colgroup>
                    <col width="40%"/>
                    <col width="23%"/>
                    <col width="23%"/>
                    <col width="14%"/>
                </colgroup>                
                <tr>
                    <td class="no-padding"><img src="/assets/images/main/med_text.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/3.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/0.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/count.png" alt="" class="responsive-image"/></td>
                </tr>
            </table>
        </div>
        <div class="one-half left-padding last-column">
            <table class="table no-border no-bottom">
                <colgroup>
                    <col width="40%"/>
                    <col width="23%"/>
                    <col width="23%"/>
                    <col width="14%"/>
                </colgroup>                
                <tr>
                    <td class="no-padding"><img src="/assets/images/main/inseoul_text.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/5.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/5.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/count.png" alt="" class="responsive-image"/></td>
                </tr>
            </table>
        </div>
        <div class="one-half left-padding last-column">
            <table class="table no-border no-bottom">
                <colgroup>
                    <col width="40%"/>
                    <col width="23%"/>
                    <col width="23%"/>
                    <col width="14%"/>
                </colgroup>                
                <tr>
                    <td class="no-padding"><img src="/assets/images/main/special_text.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/8.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/0.png" alt="" class="responsive-image"/></td>
                    <td class="no-padding"><img src="/assets/images/main/count.png" alt="" class="responsive-image"/></td>
                </tr>
            </table>
        </div>        
        -->
        <a href="/intro/why#needstudy"><img src="/assets/images/main/why_need.png" class="responsive-image no-bottom padding-left padding-right" /></a>
        <a href="/intro/why#success"><img src="/assets/images/main/why_success.png" class="responsive-image no-bottom padding-left padding-right" /></a>
        <!--<div class="overlay"></div>-->
    </div>   
    <div class="content-fullscreen no-padding no-bottom">
        <table class="main-menu-icon no-bottom">
            <colgroup>
                <col width="33%"/>
                <col width="33%"/>
                <col width="33%"/>
            </colgroup>
            <tr>
                <td onclick="location.href='/intro'">
                    <i class="ilkium-logo-icon color-blue-dark"></i>
                    <h5>이르키움 소개</h5>
                </td>
                <td onclick="location.href='/intro/manage'">
                    <i class="ion-gear-a color-night-light"></i>
                    <h5>관리시스템</h5>
                </td>
                <td onclick="location.href='/success/story'">
                    <i class="ion-star color-night-light"></i>
                    <h5>성공사례</h5>
                </td>
            </tr>
            <tr>
                <td onclick="location.href='/intro/teacher'">
                    <i class="ion-person-stalker color-blue-dark"></i>
                    <h5>강사 소개</h5>
                </td>
                <td onclick="location.href='/admission'">
                    <i class="ion-clipboard color-night-light"></i>
                    <h5>입학안내</h5>
                </td>
                <td onclick="location.href='/news'">
                    <i class="ion-speakerphone color-night-light"></i>
                    <h5>입시정보</h5>
                </td>
            </tr>
        </table>

    </div>

    <div class="heading-block bg-black-light">
        <h3 class="left-text-mobile">대학 입시정보</h3>
        <span class="text-more-link" onclick="location.href='/news'">더보기</span>
        <hr class="colored" />
        <!--<div class="overlay dark-overlay"></div>-->
        <ul class="main-board-list no-bottom">
            <?php
            for($i=0; $i<count($newsList); $i++) {
                $row = $newsList[$i];
                $row['title'] = ellipsize($row['title'], 18);
//                $row['branch_name'] = $row['branch_name'] ? $row['branch_name'] : "메인";
                $link = "/news/read/".$row['pk_id'];
//                $branch_name = trim(str_replace("이르키움", "", $row['branch_name']));
            ?>            
            <li >
                <a href="<?=$link?>" class="items-black">
                    <div class="li_title"><?=$row['title']?></div>
                    <div class="li_date"><?= substr($row['ins_date'], 0, 10)?></div>
                </a>
            </li>
            <?php
            }
            ?>            
        </ul>
    </div>
    <div class="content">
        <h3 class="left-text-mobile">공지사항</h3>
        <span class="text-more-link-light"><a href="/notice">더보기</a></span>
        <!--<div class="overlay dark-overlay"></div>-->
        <ul class="main-board-list no-bottom">
            <?php
            for($i=0; $i<count($noticeList); $i++) {
                $row = $noticeList[$i];
                $row['title'] = ellipsize($row['title'], 18);
                $row['branch_name'] = $row['branch_name'] ? $row['branch_name'] : "메인";
                $link = "/notice/read/".$row['pk_id'];
                $branch_name = trim(str_replace("이르키움", "", $row['branch_name']));
            ?>            
            <li >
                <a href="<?=$link?>" class="items-white">
                    <div class="li_title"><span class="text-badge"><?=$branch_name?></span><?=$row['title']?></div>
                    <div class="li_date"><?= substr($row['ins_date'], 0, 10)?></div>
                </a>
            </li>
            <?php
            }
            ?>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="decoration decoration-margins"></div>
    <div class="content">
        <h3 class="left-text-mobile">Q&A</h3>
        <span class="text-more-link-light"><a href="/board">더보기</a></span>
        <!--<div class="overlay dark-overlay"></div>-->
        <ul class="main-board-list no-bottom">
            <?php
//            printr($boardList[0]);
            for($i=0; $i<count($boardList); $i++) {
                $row = $boardList[$i];
                $link = "/board/boardRead/".$row['pk_id'];
                $row['title'] = ellipsize($row['title'], 15);
                $branch_name = trim(str_replace("이르키움", "", $row['branch_name']));
            ?>
            <li >
                <a href="<?=$link?>" class="items-white">
                    <div class="li_title"><span class="text-badge"><?=$branch_name?></span><?=$row['title']?></div>
                    <div class="li_date"><?=substr($row['ins_date'], 0, 10)?></div>
                </a>
            </li>
            <?php
            }
            ?>
        </ul>
    </div>
