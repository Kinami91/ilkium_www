<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Branch extends CI_Controller {

    private $branch_id = null;

    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->model("m_branch");
        $http_host = $_SERVER['HTTP_HOST'];
        if ($http_host != "dev.www.ilkium.co.kr" && $http_host != "www.ilkium.co.kr") {
            $host = explode(".", $http_host);

            $this->branch_id = $host[0];
        }
    }

    public function getBranchId() {
        return $this->branch_id;
    }

    public function setBranchId($branch_id) {
        $this->branch_id = $branch_id;
    }

    public function index() {
        $data['mapData'] = $this->m_branch->getBranchList();
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_branchMain", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_branchHeader");
            $this->load->view("m/v_branchMain", $data);
            $this->load->view("inc/m/v_footer");
        }
    }

    public function intro($branch_id = null) {
//        print_r($this->uri->segments);
//        if(!$branch_id) 
//            $this->branch_id = "isan";
        if ($branch_id)
            $this->setBranchId($branch_id);

        $data['info'] = $this->m_branch->getBranchIntro($this->branch_id);

        if ($data['info']['branch_intro1'] == null) {
            $data['info']['branch_intro1'] = '<div class="fancy-title  ilkium-title">
                        <h1>' . $data['info']['branch_name'] . '</h1>
                        </div>';
        }

        if ($data['info']['branch_intro2'] == null) {
            $data['info']['branch_intro2'] = '<p><br />
                주소 : <strong>&nbsp;' . $data['info']['address'] . '&nbsp;</strong><br />
                문의전화 : <strong>' . $data['info']['phone'] . '</strong></p>';
        }

        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_branchIntro", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_branchIntro", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function preview($branch_id = null) {
        if ($branch_id)
            $this->setBranchId($branch_id);
        $arBranch = $this->m_branch->getBranchData($this->branch_id);

        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);
        $data['images'] = $arBranch;
        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_preview", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $data['info'] = $this->m_branch->getBranchIntro($this->branch_id);        
            
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_preview", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function admission($branch_id = null) {
        if ($branch_id)
            $this->setBranchId($branch_id);
        $data['info'] = $this->m_branch->getBranchIntro($this->branch_id);
        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_admission", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_admission", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function notice($branch_id = null) {
        if ($branch_id)
            $this->setBranchId($branch_id);

        $p = $this->input->post(null, true);

        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);

        $this->load->model("notice/m_notice");
        $this->m_notice->setBranchId($this->branch_id);

        if (!$p['page'])
            $p['page'] = 1;
        $data['totalcnt'] = $this->m_notice->noticeCount($this->branch_id, $p['field'], $p['keyword']);
        $data['limit'] = 10;
        $data['start'] = ($p['page'] - 1) * $data['limit'];
        $data['rows'] = $this->m_notice->noticeList($this->branch_id, $p['page'], $p['limit'], $p['field'], $p['keyword']);

        $data['notices'] = $this->m_notice->noticeNotice($this->branch_id);

        $this->load->library('pagination');
        $config['per_page'] = $data['limit'];
        $config['base_url'] = "/notice/noticeList/";
        $config['total_rows'] = $data['totalcnt'];
        $this->load->config('pagination', TRUE);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);
        
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_noticeList", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_noticeList", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function noticeRead($branch_id, $pk_id) {
        if ($branch_id)
            $this->setBranchId($branch_id);

        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);
        $this->load->model("notice/m_notice");
        $this->m_notice->setBranchId($this->branch_id);

        $this->m_notice->increaseHits($this->m_notice->branch_id, $pk_id);
        $data['row'] = $this->m_notice->getNotice($pk_id);

        $data['cmt_list'] = $this->m_notice->getNoticeCommentList($pk_id);
        
        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_noticeRead", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_noticeRead", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function boardList($branch_id = null) {
//        $this->output->enable_profiler(TRUE);
        if ($branch_id)
            $this->setBranchId($branch_id);
        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);

        $this->load->model("board/m_board");
        $this->m_board->setBranchId($this->branch_id);
        $p = $this->input->post(null, true);

        if (!$p['page'])
            $p['page'] = 1;
        $data['totalcnt'] = $this->m_board->getBaordCnt($p['field'], $p['keyword']);
        $data['limit'] = 10;
        $data['start'] = ($p['page'] - 1) * $data['limit'];
        $data['rows'] = $this->m_board->getBoardList($p['field'], $p['keyword'], $p['start'], $p['limit']);

        $this->load->library('pagination');
        $config['per_page'] = $data['limit'];
        $config['base_url'] = "/movie/movieList/";
        $config['total_rows'] = $data['totalcnt'];
        $this->load->config('pagination', TRUE);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardList", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_boardList", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function boardWrite($branch_id = null) {
        if ($branch_id)
            $this->setBranchId($branch_id);
        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);

        $this->load->model("board/m_board");

        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);
        
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardWrite", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_boardWrite", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function boardAct() {
        $p = $this->input->post();

        $this->load->model("board/m_board");

        $this->m_board->setBranchId($p['branch_id']);
        if (!$p['pk_id']) {

            if ($p['secret_letter'] == $_SESSION['captcha_key']) {
                $this->m_board->writeBoard($p);
                redirect("http://".$_SERVER['HTTP_HOST']."/branch/boardList/");
            } else {
                echo "입력시간이 초과되었거나 잘못된 접근입니다.";
            }
        }

        if ($p['pk_id'] && $p['board_pwd']) {
            if ($this->m_board->chkArticlePwd($p['pk_id'], $p['board_pwd'])) {
                $this->m_board->updateBoard($p);

                redirect("http://".$_SERVER['HTTP_HOST']."/branch/boardRead/" . $p['branch_id'] . "/" . $p['pk_id']);
            } else {
                alert("비밀번호가 일치하지 않습니다.", "");
            }
        }
    }

    public function boardRead($branch_id = null, $pk_id) {
        if ($branch_id)
            $this->setBranchId($branch_id);
        $this->load->model("board/m_board");
        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);

        $this->m_board->setBranchId($branch_id);

        $data['row'] = $this->m_board->getArticle($pk_id);
        $this->m_board->increaseHits($pk_id);

        $data['cmt_list'] = $this->m_board->getBoardCommentList($pk_id);
        
        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);

        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardRead", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_boardRead", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }

    public function boardModify($branch_id = null, $pk_id) {
        if ($branch_id)
            $this->setBranchId($branch_id);
        $this->load->model("board/m_board");
        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);

        $this->m_board->setBranchId($branch_id);

        $data['row'] = $this->m_board->getArticle($pk_id);

        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);
        
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardModify", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_boardModify", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }
    
    public function boardDelete($branch_id = null, $pk_id) {
        if ($branch_id)
            $this->setBranchId($branch_id);
        $this->load->model("board/m_board");
        $data['info'] = $this->m_branch->getBranchSimpleInfo($this->branch_id);

        $this->m_board->setBranchId($branch_id);

        $data['row'] = $this->m_board->getArticle($pk_id);

        $data['sidebar'] = $this->load->view("v_sidebar", $data, true);
        
        if (BROWSER_TYPE == "W") {
            $this->load->view("inc/v_header");
            $this->load->view("v_boardDelete", $data);
            $this->load->view("inc/v_footer", $data);
        } else {
            $this->load->view("m/v_branchHeader", $data);
            $this->load->view("m/v_boardDelete", $data);
            $this->load->view("inc/m/v_footer", $data);
        }
    }
    
    public function boardDeleteAct() {
        $p = $this->input->post();

        $this->load->model("board/m_board");

        $this->m_board->setBranchId($p['branch_id']);
        if ($p['pk_id'] && $p['board_pwd']) {
            if ($this->m_board->chkArticlePwd($p['pk_id'], $p['board_pwd'])) {
                $this->m_board->deleteBoard($p['pk_id']);

                redirect("http://".$_SERVER['HTTP_HOST']."/branch/boardList/");
            } else {
                alert("비밀번호가 일치하지 않습니다.", "");
            }
        }

    }

    public function getxy() {
        $addr = $this->input->get("addr");
        $key = "78858565fbb4ed86793e495f241e0426";
        $addr = "서울특별시 관악구 봉천동 37-86";
        $addr = urlencode($addr);

        $url = "http://openapi.map.naver.com/api/geocode.php?key=" . $key . "&output=json&encoding=utf-8&coord=latlng&query=" . $addr;
        //echo $url."<br/>";
        echo file_get_contents($url);
        exit;

        $url = parse_url($url);

        $fp = fsockopen($url['host'], 80, $errno, $errstr);
        fputs($fp, "GET " . $url["path"] . ($url["query"] ? '?' . $url["query"] : '') . " HTTP/1.0\r\n");
        fputs($fp, "Host: " . $url["host"] . "\r\n");
        fputs($fp, "User-Agent: PHP Script\r\n");
        fputs($fp, "Connection: close\r\n\r\n");
        $api_txt = fread($fp, 1024);

        preg_match('/<x>.+<\/x>/', $api_txt, $x);
        preg_match('/<y>.+<\/y>/', $api_txt, $y);
        $x_point = preg_replace('/[^.0-9]/', '', $x[0]);
        $y_point = preg_replace('/[^.0-9]/', '', $y[0]);
    }

}

/* End of file Branch.php */
/* Location: ./application/controllers/Branch.php */