<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-parallax page-title-dark nobottommargin" style="background-image: url('/images/main/intro_bg.jpg');" data-stellar-background-ratio="0.3">

    <div class="container clearfix">
        <h1 data-animate="fadeInUp"><?=$info['branch_name']?></h1>
        <span data-animate="fadeInUp" data-delay="300"><?=$info['address']?><br>TEL. <?=$info['phone']?></span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active"><?=$info['branch_name']?></li>
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-top: 50px !important;">

        <div class="container clearfix">

            <div class="nobottommargin">

                <div class="fancy-title title-border">
                    <h2>공지사항<span style="font-size: 16px;font-weight: 400;font-style: italic;color: #aaa;padding-left: 15px;"><?=$info['branch_name']?></span></h2>
                </div>

                <div class="clear"></div>

                <div class="row">
                    <table class="table table-condensed table-hovered">
                        <colgroup>
                            <col width='10%'/>
                            <col/>
                            <col width='15%'/>
                            <col width='10%'/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>제목</th>
                                <th>날짜</th>
                                <th>조회수</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < count($notices); $i++) {
                                $notice = $notices[$i];

                                $notice['ins_date'] = substr($notice['ins_date'], 0, 10);
                                $link = "/branch/noticeRead/" . $this->m_notice->branch_id . "/" . $notice['pk_id'];
                                $isNew = (time() - strtotime($notice['ins_date'])) < (86400 * 3) ? "<span class=\"label label-warning\">New</span>" : "";
                                ?>
                                <tr class='info'>
                                    <td class='text-center'><span class="label label-warning">공지</span></td>
                                    <td><a href="<?= $link ?>"><?= $notice['title'] ?></a> &nbsp; <?= $isNew ?></td>
                                    <td class='text-center'><?= $notice['ins_date'] ?></td>
                                    <td class='text-center'><?= $notice['hits'] ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <?php
                            if ($rows) {
                                for ($i = 0; $i < count($rows); $i++) {
                                    $row = $rows[$i];
                                    $row['ins_date'] = substr($row['ins_date'], 0, 10);

                                    $isNew = (time() - strtotime($row['ins_date'])) < (86400 * 3) ? "<span class=\"label label-warning\">New</span>" : "";
                                    $link = "/branch/noticeRead/" . $this->m_notice->branch_id . "/" . $row['pk_id'];
                                    ?>
                                    <tr>
                                        <td class='text-center'><?= $row['notice_no'] ?></td>
                                        <td><a href="<?= $link ?>"> <?= $row['title'] ?></a> &nbsp; <?= $isNew ?></td>
                                        <td class='text-center'> <?= $row['ins_date'] ?> </td>
                                        <td class='text-center'> <?= $row['hits'] ?> </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        <tfoot>
                            <tr>
                                <td colspan="4" style="border-top: none;"></td>
                            </tr>
                        </tfoot>
                        <?php
                            } else {
                                ?>
                            <tfoot>
                                <tr>
                                    <td class='text-center' colspan='4' style="height: 150px; vertical-align: middle;"> 등록된 글이 없습니다. </td>
                                </tr>
                            </tfoot>
                            <?php
                        }
                        ?>
                        </tbody>

                    </table>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/notice/noticeList'" >목록</button>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>

</section><!-- #content end -->        

<script type="text/javascript">
    $(function () {
        $("#header").addClass("full-header");
    })
</script>                                        