<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-parallax page-title-dark nobottommargin" style="background-image: url('/images/main/intro_bg.jpg');" data-stellar-background-ratio="0.3">

    <div class="container clearfix">
        <h1 data-animate="fadeInUp"><?=$info['branch_name']?></h1>
        <span data-animate="fadeInUp" data-delay="300"><?=$info['address']?><br>TEL. <?=$info['phone']?></span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active"><?=$info['branch_name']?></li>
        </ol>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<?= $sidebar ?>
<section id="content">

    <div class="content-wrap" style="padding-top: 50px !important;">

        <div class="container clearfix">

            <div class="nobottommargin">
            <?php 
            echo $info['apply_info'];
            ?>

            </div>
        </div>

    </div>

</section><!-- #content end -->        