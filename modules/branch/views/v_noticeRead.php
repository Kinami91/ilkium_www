<?php
$branch_id = $this->uri->segment(3);
$pk_id = $this->uri->segment(4);
?>


<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-parallax page-title-dark nobottommargin" style="background-image: url('/images/main/intro_bg.jpg');" data-stellar-background-ratio="0.3">

    <div class="container clearfix">
        <h1 data-animate="fadeInUp"><?=$info['branch_name']?></h1>
        <span data-animate="fadeInUp" data-delay="300"><?=$info['address']?><br>TEL. <?=$info['phone']?></span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active"><?=$info['branch_name']?></li>
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<!-- Content
============================================= -->
<link rel="stylesheet" href="/css/nivo-slider.css" />
<script type="text/javascript" src="/js/jquery.nivo.js"></script>
<section id="content">

    <div class="content-wrap" style="padding-top: 50px !important;">

        <div class="container clearfix">

            <div class="nobottommargin">

                <div class="fancy-title title-border">
                    <h2>공지사항<span style="font-size: 16px;font-weight: 400;font-style: italic;color: #aaa;padding-left: 15px;"><?=$info['branch_name']?></span></h2>
                </div>

                <div class="clear"></div>

                <div class="row">
                    <table class="table table-condensed table-hovered">
                        <thead>
                            <tr>
                                <th style="text-align: left !important;"><?= $row['title'] ?></th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td><div class="col-xs-5"><?= $row['writer'] ?></div>
                                    <div class="col-xs-5 font-sm">입력일 : <?= $row['ins_date'] ?></div>
                                    <div class="col-xs-2">조회수 : <?= $row['hits'] ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td><div class="col-xs-12">
                                        <?= $row['content'] ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>

                        <tfoot>    
                            <tr>
                                <td>
                                    <div class="col-xs-6">
                                        <?php
                                        if ($row['admin_id'] == $_SESSION['mem_id']) {
                                            ?>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/branch/notice/<?= $branch_id ?>'" >목록</button>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <h4>Comments <span>(<?= count($cmt_list) ?>)</span></h4>

                    <ol class="commentlist clearfix" id="commentList">
                        <?php
                        for ($i = 0; $i < count($cmt_list); $i++) {
                            $comment = $cmt_list[$i];
                            ?>
                            <li class="comment even thread-even depth-1" id="li-comment-<?=$comment['pk_id']?>">
                                <div id="comment-1" class="comment-wrap clearfix">
                                    <div class="comment-content clearfix">
                                        <div class="comment-author"><?= $comment['writer'] ?><span><?= $comment['ins_date'] ?></span></div>
                                        <p><?= nl2br($comment['comment']); ?></p>
                                        <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="<?=$comment['pk_id']?>" data-backdrop="static" data-keyboard="false" href="#myModal"><i class="icon-remove"></i></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>                        
                            </li>
                            <?php
                        }
                        ?>
                    </ol>
                    <div class="commentlist">
                        <div class="item-body form-inline">
                            <form method="post" action="" id="commentAdd">
                                <div class="form-inline">
                                    이름 : <input type="text" name="comment_writer" id="comment_writer" class="form-control input-small"/>
                                    비밀번호 : <input type="password" name="comment_pwd" id="comment_pwd" class="form-control input-small" />
                                </div>
                                <div class="form-inline">
                                    그림의 숫자를 먼저 입력하세요.
                                    <img src='/images/load_kcaptcha.gif' style='width: 150px; height: 35px; border: 0; margin: 5px 0;' id='imgCaptcha'/>
                                    <input type="text" name="secret_letter" id="secret_letter" class="form-control input-small"/>
                                </div>              
                                <div class="form-inline">
                                    <textarea name="comment_body" id="comment_body" cols="90" rows="3" class="form-control"></textarea>

                                    <button class="btn btn-warning" id="btnCommentSubmit" disabled>등록</button>
                                </div>
                            </form>
                        </div>
                    </div>        
                </div>
                <div id="myModal" class="modal" role="dialog" aria-labelledby="test">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">삭제하시겠습니까??</h4>
                        </div>
                        <div class="modal-body">
                          <input type="hidden" name="comment_id" value=""/>
                          비밀번호를 입력해주세요.
                          <input type="password" name="comment_delete_pwd" id="comment_delete_pwd" />
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" id="btnCommentDelete">Delete</button>
                        </div>
                      </div>
                    </div>
                </div>

                <script type="text/javascript">
                    var notice_id = <?= $pk_id ? $pk_id : "null" ?>;
                </script>                    
            </div>
        </div>
    </div>

</section><!-- #content end -->        

<script type="text/javascript">
    var norobot_val = null;
    $("#imgCaptcha").on("click", function () {
        $.getJSON("/api/kcaptcha/image", function (r) {
            $("#imgCaptcha").attr("src", "/img/captcha/" + r.filename);
            norobot_val = r.word;
        });
    });
    $("#imgCaptcha").trigger('click');

    $("#secret_letter").on("keyup", function () {
        var md5_key = hex_md5($(this).val());

        if (md5_key == norobot_val) {

            $("#btnSubmit, #btnCommentSubmit").addClass("btn-primary").attr("disabled", false);
        } else {
            $("#btnSubmit, #btnCommentSubmit").removeClass("btn-primary").attr("disabled", true);
        }
    });

    $("#btnCommentSubmit").on("click", function (e) {
        e.preventDefault();

        Util.api("addNoticeComment", {
            notice_id: notice_id,
            writer: $("#comment_writer").val(),
            comment_pwd: $("#comment_pwd").val(),
            comment: $("#comment_body").val()
        }, function (r) {
            console.log(r);
            insertCommentTemplate(r);
        });
    });

    var insertCommentTemplate = function (data) {
        var $str = $([
            '<li class="comment even thread-even depth-1" id="li-comment-1">',
            '    <div id="comment-1" class="comment-wrap clearfix">',
            '        <div class="comment-content clearfix">',
            '            <div class="comment-author">' + data.writer + '<span>' + data.ins_date + '</span></div>',
            '            <p> ' + data.comment + '</p>',
            '            <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="' + data.pk_id + '" data-backdrop="static" data-keyboard="false" href="#myModal"><i class="icon-remove"></i></a>',
            '        </div>',
            '        <div class="clear"></div>',
            '    </div>',
            '</li>'
        ].join("\n"));

        $str.prependTo($("#commentList"));
        $("#imgCaptcha").trigger("click");
        $("#comment_writer, #comment_pwd, #comment_body, #secret_letter").val("");
        location.hash = '#linkNotice';
    };
    
    $(".confirm-delete").click(function(e) {
        e.preventDefault();

//        $("#myModal").data('id', id).modal('show');
        $("#myModal").on("shown.bs.modal", function (d) {
            d.preventDefalut();
            var id = $(d.relatedTarget).data("id");       
            console.log(id);
            $(d.currentTarget).find('input[name="comment_id"]').val(id);
//            $(".modal").css('display', 'block');
            $(this).show();
        });
    });
    
    $("#btnCommentDelete").on("click", function (e) {
        e.preventDefault();
//        console.log($("input[name='comment_id']").val());
        var comment_id = $("input[name='comment_id']").val();
        Util.api("delNoticeComment", {
            comment_id: comment_id,
            comment_pwd: $("input[name='comment_delete_pwd']").val()
        }, function (r) {
            console.log(r);
            if(r == "E00") {
                $("input[name='comment_delete_pwd']").val("");
                $("#myModal").modal('hide');
                $("#li-comment-" + comment_id).remove();
            } else {
                
            }
        });
    });
    
</script>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/md5.js"></script>