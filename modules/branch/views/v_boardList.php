<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-parallax page-title-dark nobottommargin" style="background-image: url('/images/main/intro_bg.jpg');" data-stellar-background-ratio="0.3">

    <div class="container clearfix">
        <h1 data-animate="fadeInUp"><?= $info['branch_name'] ?></h1>
        <span data-animate="fadeInUp" data-delay="300"><?= $info['address'] ?><br>TEL. <?= $info['phone'] ?></span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active"><?= $info['branch_name'] ?></li>
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<section id="content">

    <div class="content-wrap" style="padding-top: 50px !important;">

        <div class="container clearfix">
            <div class="nobottommargin">

                <div class="fancy-title title-border">
                    <h2>Q & A<span style="font-size: 16px;font-weight: 400;font-style: italic;color: #aaa;padding-left: 15px;"><?= $info['branch_name'] ?></span></h2>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                    </div>
                    <div class="col-md-6 form-inline text-right col_last">
                        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                            <select name="field" id="field" class='form-control input-mini'>
                                <option value="title">제목</option>
                                <option value="writer">글쓴이</option>
                                <option value="content">내용</option>
                            </select>
                            <input type="text" name="keyword" id="keyword" class="form-control input-mini" />
                            <button type="submit" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini"  class="btn btn-primary">검색</button>
                        </form>
                    </div>
                </div>                

                <div class="clear"></div>

                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <colgroup>
                            <col width='10%'/>
                            <col width='50%'/>
                            <col width='15%'/>
                            <col width='15%'/>
                            <col width='10%'/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>제목</th>
                                <th>성명</th>
                                <th>날짜</th>
                                <th>조회수</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($rows) > 0) {
//                                printr($rows[0]);
                                for ($i = 0; $i < count($rows); $i++) {
                                    $row = $rows[$i];
                                    $badge = $row['comment_count'] > 0 ? "<span class=\"badge\">" . $row['comment_count'] . "</span>" : "";
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $row['board_no'] ?></td>
                                        <td><a href="/branch/boardRead/<?= $row['branch_id'] ?>/<?= $row['pk_id'] ?>"><?= $row['title'] ?></a> 
                                            <?= $badge ?></td>
                                        <td class="text-center"><?= $row['writer'] ?></td>
                                        <td class="text-center"><?= substr($row['ins_date'], 0, 10) ?></td>
                                        <td class="text-center"><?= $row['hits'] ?></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="5" class='text-center'>등록된 글이 없습니다.</td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <?= $pagination ?>
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/branch/boardList'" >목록</button>
                            <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/branch/boardWrite'" >쓰기</button>
                        </div>
                    </div>
                </div>


            </div>


        </div>

    </div>

</section><!-- #content end -->        
