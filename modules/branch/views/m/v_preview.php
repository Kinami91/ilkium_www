<div class="content-fullscreen page-title-strip no-top1">
    <h3><?= $info['branch_name'] ?> - 학원소개</h3>
</div>
<div class="content no-top branch-intro">
<?php
    $info['branch_intro1'] = preg_replace("!<div class=\"fancy-title\">(.*?)<\/div>!is", "", $info['branch_intro1']);
    $info['branch_intro1'] = preg_replace("!<h4>(.*?)<\/h4>!is", "", $info['branch_intro1']);

?>
 <?=$info['branch_intro1']?>
</div>
<div class="content half-bottom">
    <div class="gallery gallery-square no-bottom">
        <?php
//                        printr($images);
        for($i=0; $i<count($images); $i++) {
            $image = $images[$i];
        ?>
        <a href="http://image.ilkium.co.kr/uploads/branch/<?=$image['branch_id']?>/<?=$image['img_path']?>" title="<?=$image['label_title']?>" class="show-gallery animate-fade">
            <img src="http://image.ilkium.co.kr/uploads/branch/<?=$image['branch_id']?>/<?=$image['img_path']?>" alt="<?=$image['label_title']?>" class="preload-image responsive-image">
        </a>

        <?php
        }
        ?>
    </div>
</div>
<div class="clear"></div>       

<script type="text/javascript">
    $(function () {
        $("#header").addClass("full-header");
    })
</script>

<?php
if($info['branch_id'] == "pchon") {
?>
<!-- 전환페이지 설정 -->
<script type="text/javascript" src="//wcs.naver.net/wcslog.js"></script> 
<script type="text/javascript"> 
var _nasa={};
_nasa["cnv"] = wcs.cnv("5","10"); // 전환유형, 전환가치 설정해야함. 설치매뉴얼 참고
</script> 


<?php
}
?>