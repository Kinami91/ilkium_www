<div class="content-fullscreen page-title-strip no-top1">
    <h3><?= $info['branch_name'] ?> - 공지사항</h3>
</div>
<div class="content full-top">
    <ul class="board_list">
        <?php
        if ($rows) {
            for ($i = 0; $i < count($rows); $i++) {
                $row = $rows[$i];
                $offset = ($page - 1) * $limit;
                $no = $totalcnt - $offset - $i;                                    
                $row['ins_date'] = substr($row['ins_date'], 0, 10);

                $isNew = (time() - strtotime($row['ins_date'])) < (86400 * 3) ? "<span class=\"label label-warning\">New</span>" : "";
                $link = "/branch/noticeRead/" . $this->m_notice->branch_id . "/" . $row['pk_id'];
                
                ?>
                <li class="board-item">
                    <a href="<?=$link?>" class="item-title"><?=$isNew?><?=$row['title']?></a>
                    <div class="item-cate"><?=$row['branch_name']?></div>
                    <div class="item-sub">
                        <span class="item-date"><?=$row['ins_date']?></span> | 
                        조회수 : <span class="item-hits"><?=$row['hits']?></span> |
                        댓글 : <span class="item-comment"><?=$row['comment_count']?></span>
                    </div>
                </li>        

           <?php
            }
            ?>
        <?php
    } else {
        ?>
                <li class="board-item">
                    <div class="item-no-content">등록된 글이 없습니다.</div>
                </li>
        <?php
    }
    ?>        
    </ul>
</div>
<div class="content text-center">
    <?= $pagination ?>
</div>
                            