    <div class="content-fullscreen no-bottom">
<!--        <div class="home-fader animate-top animate-time-1000">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="/images/1.jpg" class="responsive-image" alt="img">
                </div>
                <div class="swiper-slide">
                    <img src="/images/2.jpg" class="responsive-image" alt="img">
                </div>
                <div class="swiper-slide">
                    <img src="/images/3.jpg" class="responsive-image" alt="img">
                </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>                                    
        </div>-->
     
    </div>
    <div class="content half-top">
        <h3 class="left-text-mobile">학원소개</h3>
        <span class="text-more-link-light">더보기</span>
        <hr class="colored-light" />
        <p class="branch-p-text">안녕하세요. 이르키움 일산본원입니다. <br>
독학재수관리에 대한 생각을 가지고 실천해온 지가 벌써 7년에 접어듭니다.
처음 많은 사람들이 실천과 실현이 어렵다고들 했지만, 지금의 독학재수에 
관련되어 일어나고 있는 모든 것들은 그 당시의 생각들을 머쓱하게 만들어 
버렸습니다.</p>
        
    </div>
    <div class="swiper-container half-bottom">
        <div id="flickr-widget" class="swiper-wrapper">
            <?php
        //                        printr($images);
            for($i=0; $i<count($images); $i++) {
                $image = $images[$i];
            ?>
            <div class="swiper-slide">
                <img src="http://image.ilkium.co.kr/uploads/branch/<?=$image['branch_id']?>/<?=$image['img_path']?>" alt="<?=$image['label_title']?>" class="">
            </div>
        
            <?php
            }
            ?>
        </div>
    </div>

<div class="content">
    <h3 class="left-text-mobile">모집요강</h3>
    <hr class="colored-light" />
        <ul class="main-board-list no-bottom">
            <li >
                <a href="#" class="items-white">
                    <div class="li_title">강남 이르키움 윈터스쿨 모집 요강</div>
                </a>
            </li>
        </ul>
</div>

    <div class="heading-block bg-black-light">
        <h3 class="left-text-mobile">대학 입시정보</h3>
        <span class="text-more-link">더보기</span>
        <hr class="colored" />
        <!--<div class="overlay dark-overlay"></div>-->
        <ul class="main-board-list no-bottom">
            <li >
                <a href="#" class="items-black">
                    <div class="li_title">11월 학원비</div>
                    <div class="li_date">2017-12-01</div>
                </a>
            </li>
            <li >
                <a href="#" class="items-black">
                    <div class="li_title">2018년 정시지원시 중점 고려사항.</div>
                    <div class="li_date">2017-12-01</div>
                </a>                
            </li>
            <li>
                <a href="#" class="items-black">
                    <div class="li_title">2017 수능 사탐 난이도 조절 실패</div>
                    <div class="li_date">2017-12-01</div>
                </a>                
            </li>
        </ul>
    </div>
    <div class="content">
        <h3 class="left-text-mobile">공지사항</h3>
        <span class="text-more-link-light">더보기</span>
        <!--<div class="overlay dark-overlay"></div>-->
        <ul class="main-board-list no-bottom">
            <li >
                <a href="#" class="items-white">
                    <div class="li_title"><span class="text-badge">일산</span>11월 학원비</div>
                    <div class="li_date">2017-12-01</div>
                </a>
            </li>
            <li >
                <a href="#" class="items-white">
                    <div class="li_title"><span class="text-badge">분당</span>2018년 정시지원시 중점 고려사항.</div>
                    <div class="li_date">2017-12-01</div>
                </a>                
            </li>
            <li>
                <a href="#" class="items-white">
                    <div class="li_title"><span class="text-badge">일산</span>2017 수능 사탐 난이도 조절 실패</div>
                    <div class="li_date">2017-12-01</div>
                </a>                
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="decoration decoration-margins"></div>
    <div class="content">
        <h3 class="left-text-mobile">Q&A</h3>
        <span class="text-more-link-light">더보기</span>
        <!--<div class="overlay dark-overlay"></div>-->
        <ul class="main-board-list no-bottom">
            <li >
                <a href="#" class="items-white">
                    <div class="li_title"><span class="text-badge">일산</span>11월 학원비</div>
                    <div class="li_date">2017-12-01</div>
                </a>
            </li>
            <li >
                <a href="#" class="items-white">
                    <div class="li_title"><span class="text-badge">분당</span>2018년 정시지원시 중점 고려사항.</div>
                    <div class="li_date">2017-12-01</div>
                </a>                
            </li>
            <li>
                <a href="#" class="items-white">
                    <div class="li_title"><span class="text-badge">일산</span>2017 수능 사탐 난이도 조절 실패</div>
                    <div class="li_date">2017-12-01</div>
                </a>                
            </li>
        </ul>
    </div>
<script type="text/javascript">
//$('#flickr-widget').slick({
//  dots: false,
//  infinite: false,
//  speed: 300,
//  slidesToShow: 1,
//  centerMode: false,
//  variableWidth: true
//});

    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 4,
      spaceBetween: 10,
      centeredSlides: true,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      nextButton: '.swiper-button-next', 
      prevButton: '.swiper-button-prev',
    });
</script>