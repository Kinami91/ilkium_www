<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/board.js"></script>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-parallax page-title-dark nobottommargin" style="background-image: url('/images/main/intro_bg.jpg');" data-stellar-background-ratio="0.3">
    <div class="container clearfix">
        <h1 data-animate="fadeInUp"><?=$info['branch_name']?></h1>
        <span data-animate="fadeInUp" data-delay="300"><?=$info['address']?><br>TEL. <?=$info['phone']?></span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active"><?=$info['branch_name']?></li>
        </ol>
    </div>
</section>
<?= $sidebar ?>
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap" style="padding-top: 50px !important;">
        <div class="container clearfix">
            <div class="nobottommargin">
                <div class="fancy-title title-border">
                    <h2>Q & A<span style="font-size: 16px;font-weight: 400;font-style: italic;color: #aaa;padding-left: 15px;"><?= $info['branch_name'] ?></span></h2>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <form action="/branch/boardDeleteAct" id="boardDeleteForm" method="post">
                        <div class="row padding-top-10px">
                            <table class="table table-condensed table-hovered">
                                <thead>
                                    <tr>
                                        <th style="text-align: left !important;"><?= $row['title'] ?></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td><div class="col-xs-5"><?= $row['writer'] ?></div>
                                            <div class="col-xs-5 font-sm">입력일 : <?= $row['ins_date'] ?></div>
                                            <div class="col-xs-2">조회수 : <?= $row['hits'] ?></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><div class="col-xs-12 topmargin-lg bottommargin-lg">
                                                <div class="text-center">
                                                    글 작성시 입력했던 비밀번호를 입력해주세요!
                                                </div>
                                                <div class="text-center">
                                                    <input type="password" name="board_pwd" id="board_pwd" class="input-small"/>
                                                    <input type="hidden" name="pk_id" value="<?=$row['pk_id']?>" />
                                                    <input type="hidden" name="branch_id" value="<?=$row['branch_id']?>" />
                                                    <button class="button button-3d button-rounded button-amber" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnSubmit" type="submit">삭제</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="text-right">
                                            <button class="button button-3d button-rounded button-purple" data-class-md="button-default" data-class-xs="button-small" data-class-xxs="button-mini" id="btnList" onclick="location.href = '/branch/boardList'" >목록</button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</section><!-- #content end -->        
<script type="text/javascript">
    var branch_id = "<?= $info['branch_id'] ?>";

    $("#boardWriteForm").validate({
        rules: {
            title: {
                required: true
            },
            writer: {
                required: true
            },
            board_pwd: {
                required: true,
                minlength: 4
            }
        }
    });

    $("#btnSubmit").on("click", function (e) {
        $("#boardWriteForm").submit();
    });
</script>
<script type="text/javascript" src="/js/app.js"></script>
