<?php
$branch_id = $this->uri->segment(3);
$pk_id = $this->uri->segment(4);
?>
<div class="content board-title no-bottom">
    <h4><?= $row['title'] ?></h4>
    <div class="board-sub">
        <?= $row['writer'] ?> | <?= $row['ins_date'] ?> | 조회수 : <?= $row['hits'] ?>
    </div>
</div>    
<div class="content half-top board-content" style="min-height: 200px;">
    <?= $row['content'] ?>
</div>
<div class="clear"></div>
<div class="content board-title half-top no-bottom">
    <div class="decoration-margins"></div>
    <div class="commentlist">
        <form method="post" action="#" id="commentAdd">
            <input type="text" name="comment_writer" id="comment_writer" class="input-text-box" placeholder="이름"/>
            <input type="password" name="comment_pwd" id="comment_pwd" class="input-text-box" placeholder="비밀번호"/>


            <div class="item-body form-inline">
                <!--<div class="form-inline">-->
                그림의 숫자를 먼저 입력하세요.
                <div class="one-half">
                    <img src='/assets/images/load_kcaptcha.gif' style='width: 150px; height: 35px; border: 0; margin: 5px 0;' id='imgCaptcha'/>
                </div>
                <div class="one-half last-column">
                    <input type="text" name="secret_letter" id="secret_letter" class="input-text-box"/>
                </div>
                <!--</div>-->              
                <!--<div class="form-inline">-->
                <textarea name="comment_body" id="comment_body" cols="" rows="3" class="contactTextarea"></textarea>

                <button class="button button-xxs" id="btnCommentSubmit" disabled>댓글등록</button>
                <!--</div>-->
            </div>
        </form>
    </div> 
</div>    
<div class="content comment-block ">
    <div class="decoration decoration-margins"></div>
    
    <ol class="commentlist clearfix" id="commentList">
        <?php
        for ($i = 0; $i < count($cmt_list); $i++) {
            $comment = $cmt_list[$i];
            ?>
            <li class="comment even thread-even depth-1" id="li-comment-<?= $comment['pk_id'] ?>">
                <div id="comment-1" class="comment-wrap clearfix">
                    <div class="comment-content clearfix">
                        <div class="comment-author"><?= $comment['writer'] ?><span><?= $comment['ins_date'] ?></span></div>
                        <p><?= nl2br($comment['comment']); ?></p>
                        <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="<?= $comment['pk_id'] ?>" 
                           data-backdrop="static" data-keyboard="false"  data-target="#myModal"><i class="icon-remove"></i></a>
                    </div>
                    <div class="clear"></div>
                </div>                        
            </li>
            <?php
        }
        ?>
    </ol>    
</div>
<div class="one-half-responsive text-center">
    <button class="button button-xxs button-yellow" id="btnList" onclick="location.href = '/branch/notice/<?=$branch_id?>'" >목록</button>
</div>


<section id="content" style="display: none;">

    <div class="content-wrap" style="padding-top: 50px !important;">
        <div class="container clearfix">
            <div class="nobottommargin col_last">
                <div class="table-responsive">


                </div>
                <div id="myModal" class="modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">삭제하시겠습니까??</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" name="comment_id" value=""/>
                                <label for="comment_delete_pwd">비밀번호를 입력해주세요.</label>
                                <input type="password" name="comment_delete_pwd" id="comment_delete_pwd" />
                                <label class="error" style="display:none;">비밀번호가 일치하지 않습니다. </label>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="btnCommentDelete">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    var notice_id = <?= $pk_id ? $pk_id : "null" ?>;
                </script>                    
            </div>


        </div>

    </div>

</section><!-- #content end -->        

<script type="text/javascript">
    var norobot_val = null,
            board_id = <?= $row['pk_id'] ? $row['pk_id'] : "null" ?>;
    $("#imgCaptcha").on("click", function () {
        $.getJSON("/api/kcaptcha/image", function (r) {
            $("#imgCaptcha").attr("src", "/img/captcha/" + r.filename);
            norobot_val = r.word;
        });
    });
    $("#imgCaptcha").trigger('click');

    $("#secret_letter").on("keyup", function () {
        var md5_key = hex_md5($(this).val());

        if (md5_key == norobot_val) {

            $("#btnSubmit, #btnCommentSubmit").addClass("button-blue").attr("disabled", false);
        } else {
            $("#btnSubmit, #btnCommentSubmit").removeClass("button-blue").attr("disabled", true);
        }
    });

    $("#btnCommentSubmit").on("click", function (e) {
        e.preventDefault();
        if (!$("#comment_writer").val()) {
            alert("이름을 입력해주세요");
            $("#comment_writer").focus();
            return false;
        }
        if (!$("#comment_pwd").val()) {
            alert("비밀번호를 입력해주세요");
            $("#comment_pwd").focus();
            return false;
        }
        if (!$("#comment_body").val()) {
            alert("내용을 입력해주세요");
            $("#comment_body").focus();
            return false;
        }

        Util.api("addBoardComment", {
            board_id: board_id,
            writer: $("#comment_writer").val(),
            comment_pwd: $("#comment_pwd").val(),
            comment: $("#comment_body").val()
        }, function (r) {
            console.log(r);
            insertCommentTemplate(r);
        });
    });

    var insertCommentTemplate = function (data) {
        var $str = $([
            '<li class="comment even thread-even depth-1" id="li-comment-1">',
            '    <div id="comment-1" class="comment-wrap clearfix">',
            '        <div class="comment-content clearfix">',
            '            <div class="comment-author">' + data.writer + '<span>' + data.ins_date + '</span></div>',
            '            <p> ' + data.comment + '</p>',
            '            <a class="comment-reply-link confirm-delete" data-toggle="modal" data-id="' + data.pk_id + '" data-backdrop="static" data-keyboard="false"><i class="icon-remove"></i></a>',
            '        </div>',
            '        <div class="clear"></div>',
            '    </div>',
            '</li>'
        ].join("\n"));

        $str.prependTo($("#commentList"));
        $("#imgCaptcha").trigger("click");
        $("#comment_writer, #comment_pwd, #comment_body, #secret_letter").val("");
        location.hash = '#linkNotice';
    };

    $(".confirm-delete").click(function (e) {
        e.preventDefault();
//        alert("tttt");
        $("#myModal").modal('show');
//        $("#myModal").css('display', 'block');
//        
        $("#myModal").on("shown.bs.modal", function (d) {
            $(this).css('display', 'block');
            var id = $(d.relatedTarget).data("id");
            console.log(id);
            $(d.currentTarget).find('input[name="comment_id"]').val(id);
//            $(".modal").css('display', 'block');
//            
        });
    });

    $("#btnCommentDelete").on("click", function (e) {
        e.preventDefault();
//        console.log($("input[name='comment_id']").val());
        var comment_id = $("input[name='comment_id']").val();
        Util.api("deleteBoardComment", {
            comment_id: comment_id,
            comment_pwd: $("input[name='comment_delete_pwd']").val()
        }, function (r) {
            console.log(r);
            if (r == "E00") {
                $("input[name='comment_delete_pwd']").val("");
                $("#myModal").modal('hide');
                $("#li-comment-" + comment_id).remove();
            } else {
                $(".modal-body label.error").text("비밀번호가 일치하지 않습니다.").show();
            }
        });
    });

</script>
<script type="text/javascript" src="/assets/scripts//app.js"></script>
<script type="text/javascript" src="/assets/scripts//md5.js"></script>