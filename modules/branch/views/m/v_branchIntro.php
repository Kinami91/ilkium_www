<div class="content-fullscreen page-title-strip no-top1">
    <h3><?= $info['branch_name'] ?> - 위치 및 연락처</h3>
</div>
<div class="content full-top">
    <a href="javascript:tel('<?= $info['phone'] ?>')" class="icon icon-round phone-bg"><i class="ion-ios-telephone"></i></a>
    <span onclick="javascript:tel('<?= $info['phone'] ?>')" class="phone_number"><?= $info['phone'] ?></span><br>
    <span class="phone_desc padding-left">(모바일 환경에서는 번호를 클릭하면 바로 전화로 연결됩니다.)</span>
    <br />
    <a href="#" class="icon icon-round linkedin-bg"><i class="ion-ios-location"></i></a>
    <?= $info['address'] ?>
    <div id="nhn_map" style="width:100%; height:250px; border: 1px solid #000;"></div>
    
<!--    <div class="container bg-pink-light color-black half-top padding-left">
        <a href="#" class="icon icon-round color-night-dark bg-yellow-dark"><i class="ion-ios-chatbubble"></i></a>
        카카오톡 상담<br>
        오픈 채팅방 : http://open.kakao.com/o/   <br>
        (오전 9시 ~ 오후 6시 사이 상담 가능하며 사정에 따라 바로 연락이 되지 않을 수도 있습니다.)
    </div>-->
</div>
<script type="text/javascript" src="http://openapi.map.naver.com/openapi/v3/maps.js?clientId=F2V8zmO2JfESl1LSJ6vf"></script>
<script type="text/javascript">
        var mapData = {'name': '<?= $info['branch_name'] ?>', 'address': '<?= $info['address'] ?>', 'y': <?= $info['map_axisY'] ?>, 'x': <?= $info['map_axisX'] ?>, "phone": "<?= $info['phone'] ?>"};

        var oPoint = new naver.maps.LatLng(mapData.y, mapData.x);
//    nhn.api.map.setDefaultPoint('LatLng');
        var oMap = new naver.maps.Map('nhn_map', {
            center: oPoint,
            zoom: 12, // - 초기 줌 레벨은 6으로 둔다.
            scrollWheel: false,
            mapTypeControl: true,
            zoomControl: true,
            zoomControlOptions: {
                position: naver.maps.Position.TOP_LEFT
            }
        });

        var oSize = new naver.maps.Size(28, 37);
        var oOffset = new naver.maps.Size(14, 37);

        var oInfoWnd;
        var oLabel = [],
                oMarker = [];

        var mapZoom = new naver.maps.ZoomControl(); // - 줌 컨트롤 선언


        var content = '<DIV style="padding: 10px; border-top:1px solid;border-bottom: 2px solid #8E8A8A;opacity: 0.8; width: 300px !important; border-left:1px solid;border-right: 2px solid #8E8A8A;margin-bottom:1px;color:black;background-color: rgb(246, 247, 199); height:auto;">' +
                '<div style="font-size: 16px !important;font-weight: 900;margin-bottom: 5px;text-shadow: 1px 1px 1px #8A8585;">' + mapData.name + '</div>' +
                '<span style="color: #494949 !important;display: inline-block;font-size: 13px !important;letter-spacing: -2px !important; padding: 2px 5px 2px 2px !important;font-weight: 600;">' +
                mapData.address + ' <br /> ' +
                '<span style="margin-top: 5px;color: #000000 !important;display: inline-block;font-size: 14px !important;letter-spacing: -1px !important; padding: 2px 5px 2px 2px !important; font-weight: bold;">전화 : ' +
                mapData.phone + ' <br /> '

                + '<button onclick="javascript:closeInfoWin()" style="margin-top: 10px; background-color:#fff;color:#000;border:1px solid #333; padding: 5px;font-weight:bold;">닫기</button>'
                + '<span></div>';
        oInfoWnd = new naver.maps.InfoWindow({
            content: content,
            borderWidth: 0,
            backgroundColor: 'transparent',
            pixelOffset: new naver.maps.Point(0, 0),
        });
//                        hideMarker(null);
        var marker = new naver.maps.Marker({
            map: oMap,
            position: oPoint,
            icon: {
                url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
                size: oSize,
                anchor: new naver.maps.Point(12, 37),
                origin: oOffset
            }
        });
//                        oInfoWnd.open(oMap, marker);

        function goBranchPage(val) {
            window.location.href = "/";

        }

        function closeInfoWin() {
            oInfoWnd.close();
        }
        //Detecting Mobiles//
        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        
        function tel(phone_no) {
            if (isMobile.any()) {
                window.open("tel:" + phone_no);
            } else {
                alert("전화걸기는 모바일 환경에서만 지원됩니다.");
            }
        }

</script>        
