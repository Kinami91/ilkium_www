<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-parallax page-title-dark nobottommargin" style="background-image: url('/images/main/intro_bg.jpg');" data-stellar-background-ratio="0.3">

    <div class="container clearfix">
        <h1 data-animate="fadeInUp"><?=$info['branch_name']?></h1>
        <span data-animate="fadeInUp" data-delay="300"><?=$info['address']?><br>TEL. <?=$info['phone']?></span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active"><?=$info['branch_name']?></li>
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<!-- Content
============================================= -->
<link rel="stylesheet" href="/css/nivo-slider.css" />
<script type="text/javascript" src="/js/jquery.nivo.js"></script>
<section id="content">

    <div class="content-wrap" style="padding-top: 50px !important;">

        <div class="container clearfix">

            <div class="nobottommargin">

                <div class="fancy-title title-border">
                    <h2><?=$info['branch_name']?> 미리보기</h2>
                </div>

                <div class="clear"></div>

                <div class="bottommargin-lg">
                    <div id="flickr-widget" class="flickr-feed masonry-thumbs" data-id="" data-count="<?php count($images)?>" data-type="group" data-lightbox="gallery">
                        <?php
//                        printr($images);
                        for($i=0; $i<count($images); $i++) {
                            $image = $images[$i];
                        ?>
                        <a href="http://image.ilkium.co.kr/uploads/branch/<?=$image['branch_id']?>/<?=$image['img_path']?>" title="<?=$image['label_title']?>" data-lightbox="gallery-item" style="width: 90px; position: absolute; left: 0px; top: 0px;">
                            <img src="http://image.ilkium.co.kr/uploads/branch/<?=$image['branch_id']?>/<?=$image['img_path']?>" alt="<?=$image['label_title']?>">
                        </a>
                        
                        <?php
                        }
                        ?>
                    </div>
                    
<!--                    <div class="fslider flex-thumb-grid grid-8" data-animation="fade" data-arrows="true" data-thumbs="true">
                        <div class="flexslider">
                            <div class="slider-wrap">
                        <?php
                        for($i=0; $i<count($images); $i++) {
                            $image = $images[$i];
                        ?>
                                <div class="slide" data-thumb="http://image.ilkium.co.kr/uploads/branch/<?=$image['branch_id']?>/<?=$image['img_path']?>">
                                    <a href="#">
                                        <img src="http://image.ilkium.co.kr/uploads/branch/<?=$image['branch_id']?>/<?=$image['img_path']?>" alt="">
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <div class="text-overlay-title">
                                                    <h3><?=$image['label_title']?></h3>
                                                </div>
                                                <div class="text-overlay-meta">
                                                    <span><?=$image['label_desc']?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>                                
                                
                        <?php
                        }
                        ?>

                            </div>
                        </div>
                    </div>-->

                </div>
            </div>
        </div>

    </div>

</section><!-- #content end -->        

<script type="text/javascript">
    $(function () {
        $("#header").addClass("full-header");
    })
</script>

<?php
if($info['branch_id'] == "pchon") {
?>
<!-- 전환페이지 설정 -->
<script type="text/javascript" src="//wcs.naver.net/wcslog.js"></script> 
<script type="text/javascript"> 
var _nasa={};
_nasa["cnv"] = wcs.cnv("5","10"); // 전환유형, 전환가치 설정해야함. 설치매뉴얼 참고
</script> 


<?php
}
?>