
<section id="page-title" class="page-title-parallax page-title-dark nobottommargin" style="background-image: url('/images/main/intro_bg.jpg');" data-stellar-background-ratio="0.3">

    <div class="container clearfix">
        <h1 data-animate="fadeInUp"><?=$info['branch_name']?></h1>
        <span data-animate="fadeInUp" data-delay="300"><?=$info['address']?><br>TEL. <?=$info['phone']?></span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active"><?=$row['branch_name']?></li>
        </ol>
    </div>

</section><!-- #page-title end -->
<?= $sidebar ?>
<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-top: 50px !important;">

        <div class="container clearfix">

            <div class="nobottommargin ">

                <!-- Single Post
                ============================================= -->
                <div class="entry clearfix">

                    <?=$info['branch_intro1']?>
                    <!--<div class="entry-image">-->
                    <div id="nhn_map" style="width:100%; height:400px; border: 1px solid #000;"></div>
                    <!--</div>-->
                    <div class="entry-content">
                    <?=$info['branch_intro2']?>
                    </div>
                </div><!-- .entry end -->
                <div class="clear"></div>
            </div><!-- #comments end -->
        </div>
    </div>

</section><!-- #content end -->        
<script type="text/javascript" src="http://openapi.map.naver.com/openapi/v3/maps.js?clientId=F2V8zmO2JfESl1LSJ6vf"></script>
<script type="text/javascript">
    var mapData = {'name': '<?=$info['branch_name']?>', 'address': '<?=$info['address']?>', 'y': <?=$info['map_axisY']?>, 'x': <?=$info['map_axisX']?>, "phone": "<?=$info['phone']?>"};

    var oPoint = new naver.maps.LatLng(mapData.y, mapData.x);
//    nhn.api.map.setDefaultPoint('LatLng');
                    var oMap = new naver.maps.Map('nhn_map', {
                        center: oPoint,
                        zoom: 12, // - 초기 줌 레벨은 6으로 둔다.
                        scrollWheel: false,
                        mapTypeControl: true,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: naver.maps.Position.TOP_LEFT
                        }
                    });
                    
                    var oSize = new naver.maps.Size(28, 37);
                    var oOffset = new naver.maps.Size(14, 37);

                    var oInfoWnd;
                    var oLabel = [],
                            oMarker = [];

                    var mapZoom = new naver.maps.ZoomControl(); // - 줌 컨트롤 선언
    
    
        var content ='<DIV style="padding: 10px; border-top:1px solid;border-bottom: 2px solid #8E8A8A;opacity: 0.8; width: 300px !important; border-left:1px solid;border-right: 2px solid #8E8A8A;margin-bottom:1px;color:black;background-color: rgb(246, 247, 199); height:auto;">' +
                '<div style="font-size: 16px !important;font-weight: 900;margin-bottom: 5px;text-shadow: 1px 1px 1px #8A8585;">' + mapData.name + '</div>' +
                '<span style="color: #494949 !important;display: inline-block;font-size: 13px !important;letter-spacing: -2px !important; padding: 2px 5px 2px 2px !important;font-weight: 600;">' +
                mapData.address + ' <br /> ' +
                '<span style="margin-top: 5px;color: #000000 !important;display: inline-block;font-size: 14px !important;letter-spacing: -1px !important; padding: 2px 5px 2px 2px !important; font-weight: bold;">전화 : ' +
                mapData.phone + ' <br /> '

                + '<button onclick="javascript:closeInfoWin()" style="margin-top: 10px; background-color:#fff;color:#000;border:1px solid #333; padding: 5px;font-weight:bold;">닫기</button>'
                + '<span></div>';
                        oInfoWnd = new naver.maps.InfoWindow({
                            content: content,
                            borderWidth: 0,
                            backgroundColor: 'transparent',
                            pixelOffset: new naver.maps.Point(0, 0),
                        });
//                        hideMarker(null);
                        var marker = new naver.maps.Marker({
                            map: oMap,
                            position: oPoint,
                            icon: {
                                url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
                                size: oSize,
                                anchor: new naver.maps.Point(12, 37),
                                origin: oOffset
                            }                            
                        });
//                        oInfoWnd.open(oMap, marker);

    function goBranchPage(val) {
        window.location.href = "/";

    }
    
    function closeInfoWin() {
        oInfoWnd.close();
    }


</script>        
