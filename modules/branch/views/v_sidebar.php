<?php
$current_mneu = $this->uri->segment(2);

$branch_list = $this->db->select("branch_id, branch_name")
                ->from("i_common.branch")
                ->where("branch_id !=", $info['branch_id'])
                ->where("map_axisX != ''", NULL, false)
                ->order_by("branch_name")
                ->get()->result_array();
?>
<!-- Page Sub Menu
============================================= -->
<div id="page-menu">

    <div id="page-menu-wrap">

        <div class="container clearfix">

            <div class="menu-title">&nbsp;</div>

            <nav>
                <ul>
                    <li <?= strstr($current_mneu, "intro") ? "class='current'" : ""; ?>><a href="/branch/intro">학원소개</a></li>
                    <li <?= strstr($current_mneu, "preview") ? "class='current'" : ""; ?>><a href="/branch/preview">학원미리보기</a></li>
                    <li <?= strstr($current_mneu, "admission") ? "class='current'" : ""; ?>><a href="/branch/admission">입학안내</a></li>
                    <li <?= strstr($current_mneu, "notice") ? "class='current'" : ""; ?>><a href="/branch/notice">공지사항</a></li>
                    <li <?= strstr($current_mneu, "board") ? "class='current'" : ""; ?>><a href="/branch/boardList">Q & A</a></li>

                    <li><a href="#">다른 지점</a>
                        <ul>
                            <?php
                            for ($i = 0; $i < count($branch_list); $i++) {
                                $bList = $branch_list[$i];
                                ?>
                                <li><a href="http://<?= $bList['branch_id'] ?>.ilkium.co.kr"><?= $bList['branch_name'] ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </nav>
            <div id="page-submenu-trigger"><i class="icon-reorder"></i></div>
        </div>
    </div>
</div><!-- #page-menu end -->
