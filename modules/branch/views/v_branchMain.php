<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>지점안내</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/branch">지점안내</a></li>
            <li class="active">지점안내</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<script type="text/javascript" src="http://openapi.map.naver.com/openapi/v3/maps.js?clientId=F2V8zmO2JfESl1LSJ6vf"></script>
<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container clearfix">
            <div class="row clearfix">

                <div class="col-md-8">

                    <div id = "testMap" style="border:none; width:100%; height:503px; margin:20px;"></div>
                </div>
                <div class="col-md-4" style="margin-top:20px;">
                    <div class="visible-lg">
                        <a href="javascript:goBranchPage(3);" onmouseover="javascript:goBranch(3)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>일산 이르키움</a>
                        <a href="javascript:goBranchPage(5);" onmouseover="javascript:goBranch(5)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>분당 이르키움</a>
                        <a href="javascript:goBranchPage(4);" onmouseover="javascript:goBranch(4)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>노원 이르키움</a>
                        <a href="javascript:goBranchPage(2);" onmouseover="javascript:goBranch(2)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>평촌 이르키움</a>
                        <a href="javascript:goBranchPage(11);" onmouseover="javascript:goBranch(11)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>대치 이르키움</a>
                        <a href="javascript:goBranchPage(12);" onmouseover="javascript:goBranch(12)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강동 이르키움</a>
                        <a href="javascript:goBranchPage(10);" onmouseover="javascript:goBranch(10)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강서 이르키움</a>
                        <a href="javascript:goBranchPage(8);" onmouseover="javascript:goBranch(8)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>구리 이르키움</a>
                        <a href="javascript:goBranchPage(15);" onmouseover="javascript:goBranch(15)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>목동 이르키움</a>
                        <a href="javascript:goBranchPage(16);" onmouseover="javascript:goBranch(16)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>은평 이르키움</a>
                        <a href="javascript:goBranchPage(14);" onmouseover="javascript:goBranch(14)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>잠실 이르키움</a>
                        <a href="javascript:goBranchPage(13);" onmouseover="javascript:goBranch(13)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>화정 이르키움</a>
                        <a href="javascript:goBranchPage(17);" onmouseover="javascript:goBranch(17)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>관악 이르키움</a>
                        <a href="javascript:goBranchPage(18);" onmouseover="javascript:goBranch(18)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강북 이르키움</a>
                        <a href="javascript:goBranchPage(19);" onmouseover="javascript:goBranch(19)" class="button  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>수원 이르키움</a>
                        
                        <a href="javascript:revertPanning()" class="button button-3d button-rounded button-yellow button-light"><i class="icon-line-arrow-right"></i>전체보기</a>
                    </div>
                    <div class="visible-md">
                        <a href="javascript:goBranchPage(3);" onmouseover="javascript:goBranch(3)" class="button button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>일산 이르키움</a>
                        <a href="javascript:goBranchPage(5);" onmouseover="javascript:goBranch(5)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>분당 이르키움</a>
                        <a href="javascript:goBranchPage(4);" onmouseover="javascript:goBranch(4)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>노원 이르키움</a>
                        <!--<a href="javascript:goBranchPage(2);" onmouseover="javascript:goBranch(2)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>평촌 이르키움</a>-->
                        <a href="javascript:goBranchPage(11);" onmouseover="javascript:goBranch(11)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>대치 이르키움</a>
                        <a href="javascript:goBranchPage(12);" onmouseover="javascript:goBranch(12)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강동 이르키움</a>
                        <a href="javascript:goBranchPage(10);" onmouseover="javascript:goBranch(10)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강서 이르키움</a>
                        <a href="javascript:goBranchPage(8);" onmouseover="javascript:goBranch(8)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>구리 이르키움</a>
                        <a href="javascript:goBranchPage(15);" onmouseover="javascript:goBranch(15)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>목동 이르키움</a>
                        <a href="javascript:goBranchPage(16);" onmouseover="javascript:goBranch(16)" class="button  button-small  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>은평 이르키움</a>
                        <a href="javascript:goBranchPage(14);" onmouseover="javascript:goBranch(14)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>잠실 이르키움</a>
                        <a href="javascript:goBranchPage(13);" onmouseover="javascript:goBranch(13)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>화정 이르키움</a>
                        <a href="javascript:goBranchPage(17);" onmouseover="javascript:goBranch(17)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>관악 이르키움</a>
                        <a href="javascript:goBranchPage(18);" onmouseover="javascript:goBranch(18)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강북 이르키움</a>
                        <a href="javascript:goBranchPage(19);" onmouseover="javascript:goBranch(19)" class="button  button-small button-reveal button-rounded"><i class="icon-line-arrow-right"></i>수원 이르키움</a>
                        
                        <a href="javascript:revertPanning()" class="button  button-small button-3d button-rounded button-yellow button-light"><i class="icon-line-arrow-right"></i>전체보기</a>                        
                    </div>
                    <div class="visible-sm visible-xs">
                        <a href="javascript:goBranchPage(3);" onmouseover="javascript:goBranch(3)" class="button button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>일산 이르키움</a>
                        <a href="javascript:goBranchPage(5);" onmouseover="javascript:goBranch(5)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>분당 이르키움</a>
                        <a href="javascript:goBranchPage(4);" onmouseover="javascript:goBranch(4)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>노원 이르키움</a>
                        <!--<a href="javascript:goBranchPage(2);" onmouseover="javascript:goBranch(2)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>평촌 이르키움</a>-->
                        <a href="javascript:goBranchPage(11);" onmouseover="javascript:goBranch(11)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>대치 이르키움</a>
                        <a href="javascript:goBranchPage(12);" onmouseover="javascript:goBranch(12)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강동 이르키움</a>
                        <a href="javascript:goBranchPage(10);" onmouseover="javascript:goBranch(10)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강서 이르키움</a>
                        <a href="javascript:goBranchPage(8);" onmouseover="javascript:goBranch(8)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>구리 이르키움</a>
                        <a href="javascript:goBranchPage(15);" onmouseover="javascript:goBranch(15)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>목동 이르키움</a>
                        <a href="javascript:goBranchPage(16);" onmouseover="javascript:goBranch(16)" class="button  button-mini  button-reveal button-rounded"><i class="icon-line-arrow-right"></i>은평 이르키움</a>
                        <a href="javascript:goBranchPage(14);" onmouseover="javascript:goBranch(14)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>잠실 이르키움</a>
                        <a href="javascript:goBranchPage(13);" onmouseover="javascript:goBranch(13)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>화정 이르키움</a>
                        <a href="javascript:goBranchPage(17);" onmouseover="javascript:goBranch(17)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>관악 이르키움</a>
                        <a href="javascript:goBranchPage(18);" onmouseover="javascript:goBranch(18)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>강북 이르키움</a>
                        <a href="javascript:goBranchPage(19);" onmouseover="javascript:goBranch(19)" class="button  button-mini button-reveal button-rounded"><i class="icon-line-arrow-right"></i>수원 이르키움</a>
                        
                        <a href="javascript:revertPanning()" class="button  button-mini button-3d button-rounded button-yellow button-light"><i class="icon-line-arrow-right"></i>전체보기</a>                        
                    </div>                    
                </div>
                <script type="text/javascript">

                    var mapData = <?= json_encode($mapData) ?>;

                    var testMap = document.getElementById("testMap")
                    var oPoint = new naver.maps.LatLng(37.5010226, 127.0396037);
                    var oMap = new naver.maps.Map('testMap', {
                        center: oPoint,
                        zoom: 5, // - 초기 줌 레벨은 6으로 둔다.
                        mapTypeControl: true
                    });
                    
                    var oSize = new naver.maps.Size(28, 37);
                    var oOffset = new naver.maps.Size(14, 37);

                    var oInfoWnd;
                    var oLabel = [],
                            oMarker = [];

                    var mapZoom = new naver.maps.ZoomControl(); // - 줌 컨트롤 선언

                    $.each(mapData, function (key, value) {
                        setMarker(mapData[key]);
                    });
                    function goBranch(idx) {
                        var oPoint = new naver.maps.LatLng(mapData[idx].y, mapData[idx].x);
                        oMap.setZoom(12);
                        oMap.setCenter(oPoint);

                        var content = '<DIV style="padding: 10px; border-top:1px solid;border-bottom: 2px solid #8E8A8A;opacity: 0.8; width: 300px !important; border-left:1px solid;border-right: 2px solid #8E8A8A;margin-bottom:1px;color:black;background-color: rgb(246, 247, 199); height:auto;">' +
                                '<div style="font-size: 16px !important;font-weight: 900;margin-bottom: 5px;text-shadow: 1px 1px 1px #8A8585;">' + mapData[idx].name + '</div>' +
                                '<span style="color: #494949 !important;display: inline-block;font-size: 14px !important;letter-spacing: -2px !important; padding: 2px 5px 2px 2px !important;font-weight: 600;">' +
                                mapData[idx].address + ' <br /> ' +
                                '<span style="margin-top: 5px;color: #000000 !important;display: inline-block;font-size: 14px !important;letter-spacing: -1px !important; padding: 2px 5px 2px 2px !important; font-weight: bold;">전화 : ' +
                                mapData[idx].phone + ' <br /> '

                                + '<button onclick="javascript:goBranchPage(\'' + mapData[idx].url + '\')" style="margin-top: 10px; background-color:#fff;color:#000;border:1px solid #333; padding: 5px;font-weight:bold;">지점페이지 이동</button>'
                                + '<button onclick="javascript:closeInfoWin()" style="margin-top: 10px; background-color:#fff;color:#000;border:1px solid #333; padding: 5px;font-weight:bold;">닫기</button>'
                                + '<span></div>';
                        oInfoWnd = new naver.maps.InfoWindow({
                            content: content,
                            borderWidth: 0,
                            backgroundColor: 'transparent',
                            pixelOffset: new naver.maps.Point(0, 0),
                        });
//                        hideMarker(null);
                        var marker = new naver.maps.Marker({
                            map: oMap,
                            position: oPoint,
                            icon: {
                                url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
                                size: oSize,
                                anchor: new naver.maps.Point(12, 37),
                                origin: oOffset
                            }                            
                        });
                        oInfoWnd.open(oMap, marker);

                    }

                    function goBranchPage(val) {
                        if (val == null) {
                            window.location.href = "/";
                        } else if (val == 11) {
                            window.open(mapData[val].url, "_blank");
                        } else {
                            window.location.href = mapData[val].url;
                        }
                    }

                    function hideMarker(marker) {

                        if (!marker.setMap())
                            return;
                        marker.setMap(null);
                    }

                    function setMarker(item) {
                        var oPoint = new naver.maps.LatLng(item.y, item.x);
                        var oSize = new naver.maps.Size(28, 37);
                        var oOffset = new naver.maps.Size(14, 37);
//                        oIcon = new naver.maps.Icon('http://static.naver.com/maps2/icons/pin_spot2.png', oSize, oOffset);

                        var marker = new naver.maps.Marker({
                            map: oMap,
                            position: oPoint,
                            title: '이르키움 ' + item.name,
                            icon: {
                                url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
                                size: oSize,
                                anchor: new naver.maps.Point(12, 37),
                                origin: oOffset
                            }
                        });

                        oMarker.push(marker);
                    }

                    function revertPanning() {
                        oPoint = new naver.maps.LatLng(37.5010226, 127.0396037)
                        oMap.setCenter(oPoint);
                        oMap.setZoom(5);
                        closeInfoWin();
                    }
                    function closeInfoWin() {
                        oInfoWnd.close();
                    }

                </script>
            </div>
        </div>

    </div>  
</div>
</section>

<script type="text/javascript">
    $(function () {

        $("#btnSearch").click(function () {
            var queryAddr = "http://openapi.map.naver.com/api/geocode";
            $.getJSON("/branch/getxy", {
                addr: encodeURI($("#address").val())
            }, function (r) {
                console.log(r);
                ret = r.item[0].point;
                $("#result").val("y: " + ret.y + ", x: " + ret.x);
            });
        });
    });

</script>