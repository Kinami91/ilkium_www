<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            .alert {
                display: block;
                background-color: rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .alert h2 { 
                color: #333;
            }

            td, th { background-color: #fff; font-size: 18px;}

        </style>
    </head>
    <body>
        <div class="alert">
            <h1>이용에 불편을 드려 죄송합니다.</h1>
            <h2>인터넷 브라우저를 업그레이드 해주세요.</h2>
            <h2>이르키움 홈페이지는 IE6, IE7, IE8에서는 정상적으로 표시되지 않습니다.</h2>
            <h2>IE 브라우저를 업그레이드 하거나 IE이외의 브라우저(크롬, 사파리, 파이어폭스등)를</h2>
            <h2>사용해 주시기 바랍니다.</h2>
        </div>
        <h3>이르키움 지점 목록</h3>
        <table width="800" cellpadding="3" cellspacing="1" bgcolor="666">
            <tr>
                <th>No</th>
                <th>지점명</th>
                <th>주소</th>
                <th>전화번호</th>
            </tr>
            <?php
            $addr = $this->db->select("branch_name, address, phone")
                            ->from("branch")->get()->result_array();
            for ($i = 0; $i < count($addr); $i++) {
                $row = $addr[$i];
                ?>
                <tr>
                    <td><?= $i + 1 ?></td>
                    <td><?= $row['branch_name'] ?></td>
                    <td><?= $row['address'] ?></td>
                    <td><?= $row['phone'] ?></td>
                </tr>
                <?php
            }
            ?>

        </table>
    </body>
</html>