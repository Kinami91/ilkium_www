<?php

/**
 * M_branch
 * 
 * Description...
 * 
 * @package M_branch
 * @author kinam <your@email.com>
 * @version 0.0.0
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_branch extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function getBranchIntro($branch_id) {
        return $this->db->from("i_common.branch")
                ->where("branch_id", $branch_id)
                ->get()->row_array();
    }
    
    public function getBranchSimpleInfo($branch_id) {
        return $this->db->select("branch_id, branch_name, address, phone, ceo_name, busi_num, fax")
                ->from("i_common.branch")
                ->where("branch_id", $branch_id)
                ->get()->row_array();
    }
    
    
    public function getBranchData($branch_id) {
        return $this->db->from("i_common.branch_image")
                ->where("branch_id", $branch_id)
                ->order_by("sort_order")
                ->get()->result_array();

    }    
    
    public function getBranchList() {
        // {'name': '구리', 'address': '경기도 구리시 수택동 526-3 우진빌딩(스타벅스 건물) 2층', 'y': 37.6006675, 'x': 127.142058, "phone": "031-566-2675", "url": "http://guri.ilkium.co.kr"}
        $rows = $this->db->select("pk_id, branch_name name, address, phone, map_axisX x, map_axisY y, concat('http://', branch_id, '.ilkium.co.kr') url")
                ->from("branch")
                ->where("map_axisX != ''", NULL, false)
//                ->where("map_axisY IS NOT NULL", NULL, false)
                ->order_by("branch_name")
                ->get()->result_array();
        for($i=0; $i<count($rows); $i++) {
            if($rows[$i]['pk_id'] == "9") {
                $rows[$i]['url'] = "http://www.sagwanedu.com/";
                break;
            }
        }
        
        foreach($rows as $key=>$val) {
            $data[$val['pk_id']]['name'] = $val['name'];
            $data[$val['pk_id']]['address'] = $val['address'];
            $data[$val['pk_id']]['phone'] = $val['phone'];
            $data[$val['pk_id']]['x'] = $val['x'];
            $data[$val['pk_id']]['y'] = $val['y'];
            $data[$val['pk_id']]['url'] = $val['url'];
        }
        return $data;
    }
    
    public function getBranchSimpleList() {
        $rows = $this->db->select("branch_id, branch_name")
            ->from("i_common.branch")
            ->where("map_axisX != ''", NULL, false)
//            ->where("map_axisY IS NOT NULL", NULL, false)
            ->order_by("branch_name")
            ->get()->result_array();
        
        return $rows;
    }

}

/* End of file M_branch.php */
/* Location: ./application/models/M_branch.php */