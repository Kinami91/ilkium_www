<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 관리시스템</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움</a></li>
            <li class="active">관리시스템</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="postcontent nobottommargin clearfix">

                <div class="title-block">
                    <h3><span>차별화</span>된 생활<span>관리</span></h3>
                    <span>스스로 하는 것은 <b>공부</b> 이르키움과 함께하는 것은 <b>관리</b></span>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-11 col_last">

                    <blockquote class="quote">
                        <p>이르키움의 생활관리는 상벌이 아닌 자신의 생활패턴을 만들어 나가는데 초점을 맞추어 최선을 다하고 있습니다. 
                            금지사항에서 권장사항에 이르기까지 스스로 지켜나가며 수능전날까지 자기주도적 수험생활을 유지할 수 있도록 
                            이르키움의 선생님들이 항상 같이 합니다.
                        </p>
                    </blockquote>

                    <div class="entry-image">
                        <img src="/images/main/manage_life.png" alt="Blog Single">
                    </div>                    

                </div>
                <div class="line"></div>
                <div class="title-block">
                    <h3><span>성적향상</span>을 부르는 <span>학습</span>관리</h3>
                    <span>인강을 통해 익히는 것은 <b>학(學)</b>, 이르키움과 함께 스스로 하는 것은 <b>습(習)</b></span>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-11 col_last">

                    <blockquote class="quote">
                        <p>이르키움의 학습관리는 각 과목에 대한 개인별 수준파악을 시작으로해서 학과별 심층상담과
                            질의응답에 초점을맞추어 학습의 효율성을 강조하여 궁극에는 원하는 대학의 성적에 이를 수 
                            있도록 구성되어 있습니다.</p>
                    </blockquote>
                    <div class="row">
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="icon-comments"></i></a>
                                </div>
                                <h3>과목별 학과상담
                                    <span class="subtitle">과목별 학과 선생님과의 심층상담 후 현위치 분석 및 목표대학 설정</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-hand-up"></i></a>
                                </div>
                                <h3>강좌선택
                                    <span class="subtitle">과목별 선호 강좌 선택 or 추천</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third col_last">
                            <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-calendar"></i></a>
                                </div>
                                <h3>학습계획작성
                                    <span class="subtitle">학습플래너 작성 및 점검을 통한 학습량 분석</span></h3>
                            </div>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="icon-comments"></i></a>
                                </div>
                                <h3>질의응답
                                    <span class="subtitle">PB질문지 작성제출 or IB질문지 작성 업로드.<br>1:1현장응답 or 인강을 통한 응답지 제공</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-laptop2"></i></a>
                                </div>
                                <h3>화상질의응답
                                    <span class="subtitle">실시간 화상을 통한 질의응답 or 화상학과상담</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third col_last">
                            <div class="feature-box fbox-center fbox-dark fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-bar-chart"></i></a>
                                </div>
                                <h3>모의고사
                                    <span class="subtitle">매월 모의고사 실시 / 홈페이지 기록분석 성적변화 추적 후 상담실시</span></h3>
                            </div>
                        </div>                           
                    </div>
                </div>

                <div class="line"></div>
                <div class="title-block">
                    <h3><span><b>합격</b></span>으로 이끄는 입시<span><b>관리</b></span></h3>
                    <span>스스로 받는 것은 점수, 함께 하는 것이 입시</span>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-11 col_last">

                    <blockquote class="quote"><p>이르키움의 성적 및 입시관리는 매월 실시하는 모의고사의 성적을 토대로 
                        성적의 향상여부를 점검하고 <br />그 성적에 따른 맞춤형 성적 및 입시상담이 
                        개별적으로 이루어지며, <br />생활관리와는 달리 매월 필수적으로 1회는 상담이
                        이루어질 수 있도록 구성되어 있습니다.</p></blockquote>

                    <div class="row">
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="icon-file-text"></i></a>
                                </div>
                                <h3>월모의고사
                                    <span class="subtitle">매월 모의고사 실시 후 목표대학 합격 가능성 분석</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-flag-checkered"></i></a>
                                </div>
                                <h3>목표대학 재설정
                                    <span class="subtitle">다양한 입시자료로 입시상담 후 목표대학 재설정 or 수정</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third col_last">
                            <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-calendar2"></i></a>
                                </div>
                                <h3>6,9월 평가원 모의고사 실시
                                    <span class="subtitle">6,9월 평가원 모의고사 실시 후 부모님과의 입시상담</span></h3>
                            </div>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="icon-search3"></i></a>
                                </div>
                                <h3>수시 적합성 분석
                                    <span class="subtitle">9월 평가원 모의고사 실시 후 수시적합성 상담</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third">
                            <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-chart"></i></a>
                                </div>
                                <h3>과목별 분석
                                    <span class="subtitle">취약과목 분석 및 학과별 강사와의 밀착상담</span></h3>
                            </div>
                        </div>                        
                        <div class="col_one_third col_last">
                            <div class="feature-box fbox-center fbox-bg fbox-outline fbox-effect">
                                <div class="fbox-icon">
                                    <a href="#"><i class="i-alt icon-comment-alt"></i></a>
                                </div>
                                <h3>무료정시상담
                                    <span class="subtitle">대학수학능력시험 후 무료 정시상담</span></h3>
                            </div>
                        </div>                           
                    </div>
                </div>
            </div>
            <!-- Sidebar
            ============================================= -->
            <?php echo $submenu ?>

        </div>

</section>


