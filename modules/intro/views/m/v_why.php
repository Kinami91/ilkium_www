<div class="page-title-strip header-clear no-bottom">
    <h3>왜 이르키움인가?</h3>
</div>
<!-- <div class="decoration decoration-margins"></div> -->

<div class="content bg-black-light left-padding">
    <img src="/assets/images/intro/why_sub-01.png" alt=""  class="responsive-image"/>

    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_01.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">재수,N수의 새로운 패러다임을 이끄는 선두주자. <i class="toggle-180 ion-ios-arrow-down"></i></a>
        
        <div class="accordion-content ">
            <p><img src="/assets/images/intro/why_01.png" alt="" class="responsive-image"/>
                하루가 멀게 변화하는 수능에 대처하고 EBS중심의 다소 쉬워진 수능에 대비하는 유일한 대안을 제공하는 학원. 학(學)만 있고 습(習)이 거의 없는 지금의 환경에서 대학합격으로 가는 안전한 길인 자습을 충분히 제공하는 학원입니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_02.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">최적의 자습공간 제공 <i class="toggle-180 ion-ios-arrow-down"></i></a>
        <div class="accordion-content ">
            <p><img src="/assets/images/intro/why_04.png" alt="" class="responsive-image"/>
                밀폐되어 있지 않으며 넓고 쾌적한 책상에서 급우들과 선의의 경쟁을 할 수 있는 최적의 자습공간을 제공하는 학원. 이르키움학원은 학생들에게 자습을 하는데 최고의 환경을 제공하기 위해 넓은 책상과 수납공간이 주어지며, 1인당 공간이 넓어 편안하고 쾌적하게 자습을 할 수 있습니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_03.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">완벽한 온라인 시스템 구축 <i class="toggle-180 ion-ios-arrow-down"></i></a>
        <div class="accordion-content ">
            <p><img src="/assets/images/intro/why_03.png" alt="" class="responsive-image"/>
                독학생들이 가장 많이 의존하는 학습의 도구인 인강을 편리하고 가장 효율적으로 활용할 수 있도록 하기 위해 완벽한 온라인 시스템을 구축했습니다. 아울러 인터넷을 통한 질문지, 자체 무료인강, 1:1 화상상담등의 온라인서비스도 제공됩니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_04.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">철저한 학원생활 관리 <i class="toggle-180 ion-ios-arrow-down"></i></a>
        <div class="accordion-content ">
            <p><img src="/assets/images/intro/why_02.png" alt="" class="responsive-image"/>
                혼자 공부하면서 자칫 나태해지거나 흐트러지는 것을 방지하기 위해 등하원 시간의 철저한 관리 및 면학분위기를 흐리는 학생에 대한 단호한 제재등 성공적인 재수를 위해 학원내 생활을 철저하게 관리하고 있습니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_05.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">최고의 강사진으로 구성된 질의, 응답과 클리닉 시스템 <i class="toggle-180 ion-ios-arrow-down"></i></a>
        <div class="accordion-content ">
            <p><img src="/assets/images/intro/why_05.png" alt="" class="responsive-image"/>
                10년 이상 재수종합반에서 강의를 해온 최고의 지점원장들과 강사들이 인강에서는 절대로 해결해 주지 못하는 부분들을 명쾌하게 답해드립니다. 마찬가지로 인강에서는 제공할 수 없는 학과상담을 과목별 원장과 강사들이 1:1 대면 상담해 드립니다. 또한 학생의 요청시 학생에게 맞춤 인강 및 화상 강의를 제공합니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_06.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">성적 향상을 부르는 매달 이루어지는 학과 및 입시상담 <i class="toggle-180 ion-ios-arrow-down"></i></a>
        <div class="accordion-content ">
            <p>
                매 달 이루어지는 모의고사의 성적을 토대로 개인당 한 달에 최소 한 번씩은 의무적으로 성적과 입시상담을 하게 되어 어디도 따라올 수 없는 최고의 상담 시스템을 만들어 두었습니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_07.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">수시대비를 위한 논술특강 진행 <i class="toggle-180 ion-ios-arrow-down"></i></a>
        <div class="accordion-content ">
            <p>
                독학은 수능점수를 극대화시키기에 최적화 되어있어 수시를 염두해 두는 학생들에게는 논술에 대한 부담과 스트레스는 적지 않다고 볼 수 있습니다. 그런 이유로 이르키움학원은 논술을 대비한 특강을 준비해 두고 있습니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="accordion-item accordion-bg bg-black-dark half-bottom">
        <img src="/assets/images/intro/medal_img_08.png" alt="" class="medal-no"/>
        <a href="#" class="accordion-toggle">주요과목별 선택 CLINIC 수업 진행 <i class="toggle-180 ion-ios-arrow-down"></i></a>
        <div class="accordion-content ">
            <p>
                인강에서 해결할 수 없거나 실강에 대한 필요성을 느끼는 학생들을 위해 과목별 선택 클리닉 수업을 진행합니다.
            </p>
        </div>
    </div>    
    <div class="clear"></div>
    <div class="decoration decoration-margins"></div>
</div>
<div id="needstudy"></div>
<div class="content">
    <div class="container no-bottom">
        <img src="/assets/images/intro/why_subtitle-02.png" alt="" class="responsive-image"/>
        <div class="circle-thumb-layout">
            <div>
                <img src="/assets/images/intro/need-01.jpg" alt="" class="image-circle" />
                <strong>혼자 공부하고 싶지만 자기 관리가 안되는 학생</strong>
            </div>
            <div>
                <img src="/assets/images/intro/need-02.jpg" alt="" class="image-circle" />
                <strong>개인적인 학습플랜을 갖고 공부하고 싶은 수험생</strong>
            </div>
            <div>
                <img src="/assets/images/intro/need-03.jpg" alt="" class="image-circle" />
                <strong>과목별 편차가 심해 획일적인 재종반 프로그램에 부적합한 수험생</strong>
            </div>
            <div>
                <img src="/assets/images/intro/need-04.jpg" alt="" class="image-circle" />
                <strong>쉬운 수능으로 재종반 수업의 필요성을 느끼지 못하는 최상위권 수험생 </strong>
            </div>
            <div>
                <img src="/assets/images/intro/need-05.jpg" alt="" class="image-circle" />
                <strong>재종반은 불필요한 강의가 많아서 자습시간이 부족하다고 느끼는 수험생</strong>
            </div>
            <div>
                <img src="/assets/images/intro/need-06.jpg" alt="" class="image-circle" />
                <strong>자신이 좋아하는 인강을 보면서 취약부분만 도움받고 체크받고 싶은 수험생</strong>
            </div>
            <div>
                <img src="/assets/images/intro/need-07.jpg" alt="" class="image-circle" />
                <strong>생활관리, 성적관리, 입시 컨설팅 등의 관리 지원을 받으면서 공부하고 싶은 수험생</strong>
            </div>
        </div>
    </div>
</div>
<div id="success"></div>
<div class="content bg-gray-light no-bottom">
    <div class="decoration decoration-margins"></div>
    <img src="/assets/images/intro/why_subtitle-03.png" alt="" class="responsive-image"/>
    <div class="thumb-layout">
        <div>
            <img src="/assets/images/intro/point-01.png" alt="" class="preload-image" />
            <strong>목표대학과 목표학과를 정하라!</strong>
            <em>목표가 있어야 도전할 수 있다.</em>
        </div>
        <div>
            <img src="/assets/images/intro/point-02.png" alt="" class="preload-image" />
            <strong>수시/정시 유불리를 객관적으로 결정하라</strong>
        </div>
        <div>
            <img src="/assets/images/intro/point-03.png" alt="" class="preload-image" />
            <strong>수학 가/나, 탐구과목 2과목을 확실히 결정하라</strong>
        </div>
        <div>
            <img src="/assets/images/intro/point-04.png" alt="" class="preload-image" />
            <strong>자신만의 1년 공부틀을 구축하라</strong>
            <em>(인강, 시기별 공부법, 교재, 학원등)</em>
        </div>
        <div>
            <img src="/assets/images/intro/point-05.png" alt="" class="preload-image" />
            <strong>플래너를 꼼꼼히 작성하고 냉정히 평가하라</strong>
        </div>
        <div>
            <img src="/assets/images/intro/point-06.png" alt="" class="preload-image" />
            <strong>본인만의 Mind Control을 유지하라</strong>
        </div>
    </div>

</div>
