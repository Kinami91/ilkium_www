<div class="page-title-strip header-clear no-bottom">
    <h3>입학안내</h3>
</div>
<!-- <div class="decoration decoration-margins"></div> -->
<div class="background-block background-block-fullscreen bg-9">
    <h2>이르키움 재수관리반</h2>

    <div class="background-block-slider animate-zoom">
        <div class="review-3">
            <i class="color-yellow-dark review-icon ion-quote"></i>
            <p style="margin-top: 0px !important">
                결국은 내 자신이 모든 문제에 대한 해답을 가지고 있다
            </p>
        </div>
    </div>
    <h5>난 누구? 난 어디?</h5>
    <p class="large-text">성공적인 재수의 시작은 <br />
        내 자신에 대한 문제점을 찾는 것부터... <br/>
    </p>
    <div class="overlay dark-overlay"></div>
</div>
<div class="decoration no-bottom"></div>
<div class="content content-fullscreen">
    <div class="home-tabs">
        <a class="activate-tab-1 active-home-tab" href="#">모집요강</a>
        <a class="activate-tab-2" href="#">학습관리시스템</a>
        <a class="activate-tab-3" href="#">시간표</a>
        <div class="clear"></div>
    </div>
</div>

<div class="content" id="tab-1">
 
    <div class="column-icon one-half-responsive">
        <h4><i class="ion-ios-bolt color-red-dark"></i>모집요강</h4>
    </div>      
    <table style="border-spacing: 1px; background-color: #999">
        <colgroup>
            <col width="15%"/>
            <col />
        </colgroup>
        <tr>
            <th class="bg-teal-light">개강일자</th>
            <td class="bg-white">2018년 1월 2일(화)</td>
        </tr>
        <tr>
            <th class="bg-teal-light">모집기간</th>
            <td class="bg-white">2018년 1월 2일(화)부터 상시모집</td>
        </tr>
        <tr>
            <th class="bg-teal-light">모집대상</th>
            <td class="bg-white">2019학년도 대입지원 자격을 갖춘 수험생</td>
        </tr>
        <tr>
            <th class="bg-teal-light">필요서류</th>
            <td class="bg-white">2018학년도 수능 성적표 또는 9월 평가원 모의고사 성적표</td>
        </tr>
    </table>

</div>
<div class="content" id="tab-2">
    <div class="column-icon one-half-responsive">
        <h4><i class="ion-ios-bolt color-red-dark"></i>나의 위치파악</h4>
        <p>
            입시의 모든 시작은 나의 위치 파악이다. 나의 위치를 모른다면 가장 기본적인 위치를 만드는 것이 우선되어야 한다.
        </p>
    </div>   
    <div class="column-icon one-half-responsive">
        <h4><i class="ion-ios-bolt color-red-dark"></i>나만의 약점 공략</h4>
        <p>
            해야할 것보다 더 중요한 것은 하지 않았던 것들을 내 것으로 만드는 것이 입시의 시작이다. 이르키움 재수관리반의 핵심은 지난 시간 동안 놓쳤던 것들을 바로 잡는 것으로 시작
        </p>
    </div>     
    <div class="column-icon one-half-responsive">
        <h4><i class="ion-ios-bolt color-red-dark"></i>STUDY POSITIONING SYSTEM(S.P.S.)</h4>
        <p>
            이르키움의 2018년 재수관리반 프로젝트 명 : STUDY POSITIONING SYSTEM(S.P.S.)    
        </p>
    </div> 
    <img src="/assets/images/intro/SPS_system.png" alt="" class="responsive-image"/>
</div>

<div class="content" id="tab-3">
    <img src="/assets/images/main/timetable.png" alt="" class="responsive-image"/>
</div>

<!--<div class="content half-top ilkium-intro half-bottom">
    <h4>이르키움 재수관리반</h4>
    <em>"결국은 내 자신이 모든 문제에 대한 해답을 가지고 있다"</em>
        <h4>먼저 가까운 이르키움을 찾아 상담해주세요.</h4>
        <p>방문이 어려우면 전화나 카카오톡 상담도 가능합니다. <br />
            이르키움 입학은 아래와 같은 과정을 거쳐 진행되며, <br />
            <span class="color-red-light">심층상담과 분석</span>을 통해 <span class="color-red-light">성공적인 대학 입시</span>를 이루어 드립니다.
        </p>
        
        <h3>최초 등록 PROCESS <i class="icon-ilkium-title"></i></h3>
        <img src="/assets/images/intro/admission_01.png" alt="" class="responsive-image full-bottom"/>
        <h3>등록 후 PROCESS <i class="icon-ilkium-title"></i></h3>
        <img src="/assets/images/intro/admission_02.png" alt="" class="responsive-image"/>
</div>-->
