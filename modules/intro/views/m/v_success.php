<div class="page-title-strip header-clear no-bottom">
    <h3>성공사례</h3>
</div>
<!-- <div class="decoration decoration-margins"></div> -->

<!--<div class="content bg-black-light left-padding">
    <div class="heading-strip">
        <h3>2017년 합격자 현황</h3>
        <div class="overlay dark-overlay"></div>
    </div>
</div>-->
<div class="heading-block bg-4 no-bottom">
    <h4>2017년 합격자 현황</h4>
    <h1 class="color-orange-light animate-top">이르키움 합격률 80%</h1>
    <h5 class="color-gray-light animate-right">일반학원 합격률 30%</h5>
    <!--<i class="ion-ios-color-wand"></i>-->
    <p>
        의치한수 : 30명<br>
        SKY, 카이스트 : 42 명 <br>
        서성한중:  50 여명 <br />
        경외시 80 여명 <br />
        <!--기타 주요 대학교 00 명--> 
    </p>
    <div class="overlay dark-overlay"></div>
</div>



<div class="heading-strip ">
    <h3>합격자 후기</h3>
    <!--<i class="ion-ios-color-wand"></i>-->
    <div class="overlay dark-overlay"></div>
</div>

<div class="content">
    <div class="one-half-responsive">
        <div class="review-6 container">
            <h1>대성고등학교 김태윤</h1>
            <em>&nbsp;</em>
<!--            <i class="ion-quote"></i>-->
            <p class="con">
                제가 이르키움을 다니기 시작한 것은 2월달 친구의 추천을 받아 다니게 되었습니다. 
                재수를 하기로 결심하고 어디서 어떻게 해야 할지 정말 고민을 많이 했습니다. 고3때처럼 독서실을 갈까, 재수종합반 그리고 독학재수학원
                이 세가지 선택중에 독서실은 1년간 꾸준히 처음같은 마음가짐으로 할 자신이 없었고,
                재수종합반은 금전적 부담이 커서 독학재수학원중 이르키움을 선택하게 되었습니다.
                혹시 독학재수학원이라하면 선생님들도 없이 그냥 관리만 해주는게 아닌가 하는 제 걱정과는 달리
                국,영,수 각 영역에서의 수준 높으신 선생님들과 멘토분들이 각 과목에서의 질문과 공부방향 설정에 정말 많은 도움을 주셨습니다.
                제가 학원에서 공부하면서 성적뿐아니라 슬럼프로 힘들때마다 꼼꼼히 관리해주는 방식이 제가 지금 11월까지
                참고 버티는데 가장 큰 힘이 되었던 것 같습니다. 재수 1년간 가족들보다 같이 있는 시간이 많았던 원장님과
                실장님의 진심어린 노고가 이르키움의 큰 장점이라고 생각이 듭니다.
                아마 다시 수능을 준비하시는 분이라면 1년간 정말 힘들도 지칠테지만 이르키움 학원과 같이 희망하는 대학에 꼭 입학하셨으면 좋겠습니다.
                화이팅하세요!!
            </p>
        </div>
        <div class="decoration"></div>
        <div class="review-6 container">
            <h1>분당고등학교  <span>홍석호</span>(서강대 경제학과)</h1>
            <em>&nbsp;</em>
<!--            <i class="ion-quote"></i>-->
            <p class="con">
                처음에 내가 독학재수를 하겠다고 주변사람들에게 말했을 때 모두다 반대했다. <br />
                하지만 나는 종합학원에서 세운 규칙대로 공부하는 것보다 내가 부족한 부분을 스스로 보완하는 식으로 혼자 공부하는 것이 더 효율적이라고 생각했다. <br/>
                여러 독학재수학원을 상담하던 중 수학선생님이면서 재수학원에서 25년간 가르치고 기숙학원까지 운영하신 원장님과 상담하면서 
                신뢰가 가는 분당이르키움에 다니기로 결정하였다. <br />
                재수를 시작할 때 가장 먼저 해야 할 것은 자기가 왜 실패했는지 분석하는 것이다. 만일 현역때와 똑같은 생각을 가지고 똑같이 공부하면
                결과는 항상 같을 것이다.
                사람이 발전을 하려면 적절한 피드백은 필수라고 생각한다. <br />
                수능 때 부진했던 과목, 즉 자신을 재수, N수하게 만든 과목을 파악하고
                그에 따른 올바른 계획을 세우는 것이 필요하다.<br />
                재수를 성공적으로 끝마치면서 하고 싶은 말은, 재수(N수)를 한다고 해서 
                절대로 실패자가 아니라는 것이다. 간혹 친구들 중에 재수에 대해서 너무 부정적으로 생각하는 경우가 있는데
                그렇지 않다. 목표를 위해 도전하고 노력하는 것만큼 행복한 일은 없다고 생각한다.<br />
                이왕 한번 하기로 결심한거 후회하지 않도록 열심히 살았으면 좋겠다.<br />
                여러분들도 분당이르키움학원에서 재도전에 성공하시길 빕니다.
            </p>
        </div>

        <div class="decoration"></div>
        <div class="review-6 container">
            <h1>진관고등학교  <span>류혜림</span></h1>
            <em>&nbsp;</em>
<!--            <i class="ion-quote"></i>-->
            <p class="con">
                재수를 시작해 두 달 정도 집에서 독재를 했었습니다. 하지만 5월이 되면서 점점 풀어지고 의지도 잃어 공부에 소홀하게 되더라구요.<br />
                재수종합반을 가야하나 고민하던 찰나에, 집 근처 이르키움독학재수학원이 있다는 것을 알고 바로 학원에 등록했습니다.<br />
                결론적으로, 이르키움에 다니길 정말 잘한 것임을 깨달았습니다.<br />
                실제로 학원은 내가 원하는 공부를 할 수 있게 해주되 많은 것은 간섭하지 않았고, 내 공부량이 나에게 적절한지, 
                나에게 맞는 더 좋은 인강 및 교재가 어느 것인지 전체적인 관리를 해주었습니다.<br />
                또한 공부를 나의 컨디션에 맞게 조절하면서 할 수 있어서 좋았고 강압적이면서도 강압적이지 않은 분위기라 편안함을 
                가질 수 있었습니다.<br />
                또한 다른 학생들이 열심히 하는 모습을 보면서 공부하다보니 긴장감을 놓치지 않은채 꾸준히 공부를 할 수 있었습니다.<br />
                더불어 부족 과목에 대한 무료수업을 통해 높은 성적의 등급을 받았습니다.
                가장 취약했던 영어 수업을 듣게 되었는데 저에겐 정말 최상이었습니다.<br />
                매주 한 시간 반 정도였지만 개개인별 꼼꼼한 방향제시 및 문제점 해결을 통해 푸는 방법등에서 많은 개선이 있었고
                곧 성적상승이라는 성취감을 맛볼 수 있었습니다. 또한 학원에서 소수정예 논술 수업을 개강해
                멀리 이동하지 않고도 양질의 수업을 들을 수 있어 매우 알차게 학원생활을 한 것 같습니다.<br /><br />
                입시에 대해서도 입시전담원장님과 수시정밀상담도 받았고 많은 정보에 대해서도 도움을 받을 수 있었습니다.
                꾸준하게 일정한 공부시간을 달성할 수 있었고, 혼자만 하지 않다보니 우울한 감정이나 스트레스도 덜 받을 수 있었습니다.<br />
                이르키움학원을 통해 일년이라는 긴 시간동안 잘 버티면서 재수생활을 할 수 있었던 것 같습니다.<br />
                이르키움 너무도 큰 힘이 되어준 것 같습니다. <br />
            </p>
        </div>        
    </div>
</div>

<script type="text/javascript">
    $(function () {
        // Grab all the excerpt class
        $('.con').each(function () {

            // Run formatWord function and specify the length of words display to viewer
            $(this).html(formatWords($(this).html(), 80));

            // Hide the extra words
            $(this).children('span').hide();

            // Apply click event to read more link
        }).click(function () {
            // Grab the hidden span and anchor
            var more_text = $(this).children('span.more_text');
            var more_link = $(this).children('a.more_link');

            // Toggle visibility using hasClass
            // I know you can use is(':visible') but it doesn't work in IE8 somehow...
            if (more_text.hasClass('hide')) {
                more_text.show();
                more_link.html(' &raquo; 줄이기');
                more_text.removeClass('hide');
            } else {
                more_text.hide();
                more_link.html(' &laquo; 더보기');
                more_text.addClass('hide');
            }
            return false;

        });
    });

// Accept a paragraph and return a formatted paragraph with additional html tags
    function formatWords(sentence, show) {
        // split all the words and store it in an array
        var words = sentence.split(' ');
        var new_sentence = '';
        // loop through each word
        for (i = 0; i < words.length; i++) {
            // process words that will visible to viewer
            if (i <= show) {
                new_sentence += words[i] + ' ';

                // process the rest of the words
            } else {

                // add a span at start
                if (i == (show + 1))
                    new_sentence += ' <span class="more_text hide">';
                new_sentence += words[i] + ' ';

                // close the span tag and add read more link in the very end
                if (words[i + 1] == null)
                    new_sentence += '</span><a href="#" class="more_link"> &raquo; 더보기</a>';
            }
        }
        return new_sentence;
    }

</script>