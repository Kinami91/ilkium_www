<div class="page-title-strip header-clear no-bottom">
    <h3>이르키움 소개</h3>
</div>
<!-- <div class="decoration decoration-margins"></div> -->

<div class="content half-top ilkium-intro half-bottom">
    <h3>이르키움의 의미 <i class="icon-ilkium-title"></i></h3>
    <p class="no-bottom">'이르키움'은 실패를 경험한 학생들을 일으켜 세워 수험성공의 길로 이끈다는 의미로, 실패를 경험한 학생들을 일으켜 세워 수험 성공의 길로 전환시키고자 하는 이르키움의 철학을 담고 있습니다.</p>
</div>
<div class="content bg-pink-light">
    <div class='decoration decoration-margins'></div>
    <h3 class="color-black">이르키움의 특징 <i class="icon-ilkium-title"></i></h3>
    <div class="container no-bottom one-half-responsive">
        <a href="#" class="activate-toggle"></a>
        <div class="onoffswitch-3 bg-white">
            <img src="/assets/images/intro/intro_no_01.png" alt="" class="no-title-img"/>
            <h5 class="onoffswitch-text color-night-dark">차별화된 밀착 생활관리</h5>
            <i class="toggle-180 ion-ios-arrow-down"></i>
        </div>
        <p class="toggle-content half-top bg-white" >
            <img src="/assets/images/intro/intro_detail-01.png" alt="" class="responsive-image"/>
        </p>
        <div class="clear"></div>
    </div>
    <div class="container no-bottom one-half-responsive">
        <a href="#" class="activate-toggle"></a>
        <div class="onoffswitch-3 bg-white">
            <img src="/assets/images/intro/intro_no_02.png" alt="" class="no-title-img"/>
            <h5 class="onoffswitch-text color-night-dark">성적 향상을 부르는 학습관리</h5>
            <i class="toggle-180 ion-ios-arrow-down"></i>
        </div>
        <p class="toggle-content half-top bg-white" >
            <img src="/assets/images/intro/intro_detail-02.png" alt="" class="responsive-image"/>
        </p>
        <div class="clear"></div>
    </div>
    <div class="container one-half-responsive">
        <a href="#" class="activate-toggle"></a>
        <div class="onoffswitch-3 bg-white">
            <img src="/assets/images/intro/intro_no_03.png" alt="" class="no-title-img"/>
            <h5 class="onoffswitch-text color-night-dark">합격으로 이끄는 성적관리</h5>
            <i class="toggle-180 ion-ios-arrow-down"></i>
        </div>
        <p class="toggle-content half-top bg-white" >
            <img src="/assets/images/intro/intro_detail-03.png" alt="" class="responsive-image"/>
        </p>
        <div class="clear"></div>
    </div>

</div>
<div class="content half-top ilkium-intro half-bottom">
    <h3>연혁 <i class="icon-ilkium-title"></i></h3>
    <p class="no-bottom">국내 최고, 8년 역사의 독학재수학원 이르키움</p>
</div>
<div class="page-timeline-1 content">
    <div class="timeline-deco"></div>
    <div class="timeline-block-left animate-zoom">
        <i class="ion-text">2010</i>
    </div>
    <div class="timeline-block-right">
        <h1 class="timeline-heading">2010년 2월 목동룩스미아 오픈</h1>
        <h1 class="timeline-subheading">(이르키움의 전신)</h1>
    </div>      
    <div class="timeline-block-left animate-zoom">
        <i class="ion-text">2012</i>
    </div>
    <div class="timeline-block-right">
        <h1 class="timeline-heading">이르키움 일산본원 출범</h1>
        <h1 class="timeline-subheading">&nbsp;</h1>
    </div>   
    <div class="timeline-block-left animate-zoom">
        <i class="ion-text">2013</i>
    </div>
    <div class="timeline-block-right">
        <h1 class="timeline-heading">(주)이르키움 교육법인 설립</h1>
        <h1 class="timeline-heading">평촌이르키움, 목동이르키움 개원</h1>
    </div>   

    <div class="timeline-block-left animate-zoom">
        <i class="ion-text">2014</i>
    </div>
    <div class="timeline-block-right">
        <h1 class="timeline-heading">분당이르키움, 노원이르키움 개원</h1>
        <h1 class="timeline-subheading">이르키움 전지점 마감 신화</h1>
    </div>   

    <div class="timeline-block-left animate-zoom">
        <i class="ion-text">2015</i>
    </div>
    <div class="timeline-block-right">
        <h1 class="timeline-heading">강남, 강동, 강서, 은평, 대치, 광명, 잠실, 구리, 부천, 수원 동시개원</h1>
        <h1 class="timeline-subheading">전국 15개 지점 운영</h1>
    </div>   

    <div class="timeline-block-left animate-zoom">
        <i class="ion-text">2016</i>
    </div>
    <div class="timeline-block-right">
        <h1 class="timeline-heading">관악이르키움, 강북이르키움 개원</h1>
        <h1 class="timeline-subheading">&nbsp;</h1>
    </div>   

    <div class="timeline-block-left animate-zoom">
        <i class="ion-text">2017</i>
    </div>
    <div class="timeline-block-right">
        <h1 class="timeline-heading">~ 현재</h1>
        <h1 class="timeline-subheading">의,치,한의대 30명, SKY 42명, 서성한중 50여명, 경외시 80여명 등 합격</h1>
    </div>   

    
</div>