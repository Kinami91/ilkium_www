<div class="page-title-strip header-clear no-bottom">
    <h3>강사소개</h3>
</div>
<!-- <div class="decoration decoration-margins"></div> -->
<div class="heading-strip bg-1">
    <h3>수능 전문가 집단</h3>
    <p>재종반 10년이상의 강사들과 독학재수관리 전문가들</p>
    <!--<i class="ion-person"></i>-->
    <div class="overlay dark-overlay"></div>
</div>
<div class="content half-top half-bottom ilkium-teacher">

    <h3 class="text-center">국어 스페셜리스트</h3>
    <div class="decoration-slash decoration-margins"></div>
    <div class="one-half">
        <img src="/images/main/teacher/gahyun.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>류가현</h4></div>
        <div class="team-content">
            <p>
                용인메가스터디학원 <br />
                송파메가스터디학언 <br />
                서초유웨이오성학원 <br />
                분당메가스터디학원 <br />
                (전)평촌이르키움학원장 <br />
                (전)수원이르키움학원 <br />
                <span>(현)출강학원 <br />이르키움일산본원</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>
    <div class="one-half">
        <img src="http://www.pixelaura.com/wp-content/uploads/2016/03/persondummy_f_09-300x300.png" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>한보은</h4></div>
        <div class="team-content">
            <p> 
                이화여대 국문과 <br />
                목동대성학원 <br />
                일산유스트학원 <br />
                유비츠 학원 <br />
                웨스턴 학원 <br />
                숭문고, 중앙고 출강 <br />
                나인스쿨 인강 <br />
                <span>(현)출강학원 <br />이르키움일산본원, 강북이르키움, 강서이르키움, 은평이르키움</span>
            </p>
        </div>

    </div>
    <div class="decoration decoration-margins"></div>


    <div class="one-half">
        <img src="/images/main/teacher/kimtaksoo.png" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>김택수</h4></div>
        <div class="team-content">
            <p>
                중계 세일학원<br />
                엄선경 국어전문학원<br />
                광주정일 기숙학원<br />
                씨엔씨 학원<br />
                대치동 이김학원<br/>
                <span>(현)출강학원 <br />노원이르키움, 잠실이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>
    <h3 class="text-center">수학 스페셜리스트</h3>
    <div class="decoration-slash decoration-margins"></div>
    <div class="one-half">
        <img src="/images/main/teacher/jungjin1.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>정진관</h4></div>
        <div class="team-content">
            <p>중앙학원 수학과<br/>
                정진학원 교무부장<br />
                이투스학원 교무부장<br/>
                펜타스 기숙학원 수학과<br />
                남양주 정일학원 부원장<br />
                분당이르키움학원 원장<br />
                <span>(현)출강학원 <br />분당이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>

    <div class="one-half">
        <img src="/images/main/teacher/chosung.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>조성견</h4></div>
        <div class="team-content">
            <p>연세대학교 <br />
                이투스 재수종합반<br />
                비상에듀 재수종합반<br />
                광주종로 재수종합반<br />
                등용문 재수종합반<br />
                한샘 재수종합반<br />
                <span>(현)출강학원 <br />이르키움 일산본원, 강동이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>

    <div class="one-half">
        <img src="/images/main/teacher/moonsh1.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>문석훈</h4></div>
        <div class="team-content">
            <p>일산청솔학원<br />
                부천청솔학원<br />
                평촌종로학원<br />
                남양주정일 기숙학원<br />
                성균관 기숙학원<br />
                정진학원<br />
                구리이르키움학원 원장 <br />
                <span>(현)출강학원 <br />구리이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>

    <div class="clear"></div>

    <div class="one-half">
        <img src="/images/main/teacher/kimdong.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>김동형</h4></div>
        <div class="team-content">
            <p>한양대학교 <br />
                올림피아드학원<br />
                분당중앙학원<br />
                안양엘리크기숙학원<br />
                남양주정일기숙학원<br />
                분당청솔학원<br />
                구리이르키움 원장<br />
                <span>(현)출강학원 <br />구리이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>

    <div class="one-half">
        <img src="/images/main/teacher/bongilhwan.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>봉일환</h4></div>
        <div class="team-content">
            <p>성균관대학교 수학과<br />
                세일학원 재종반 11년 근무<br />
                고구마 수학학원<br />
                이지솔루션 고3 담당 <br />
                <span>(현)출강학원 <br />
                    노원이르키움, 이르키움일산본원, 강서이르키움, 목동이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>
    <h3 class="text-center">영어 스페셜리스트</h3>
    <div class="decoration-slash decoration-margins"></div>
    <div class="one-half">
        <img src="/images/main/teacher/boong1.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>이해붕</h4></div>
        <div class="team-content">
            <p>목동종로학원 <br />
                부평종로학원<br />
                서초유웨이오성 재종반<br />
                비타에듀 재종기숙학원<br />
                대치유웨이 재종반<br />
                Andus 대표<br />
                일산이르키움 본원원장<br />
                <span>(현)출강학원 <br />
                    노원이르키움, 강서이르키움, 은평이르키움</span></p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>
    <div class="one-half">
        <img src="/images/main/teacher/noh1.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>노양래</h4></div>
        <div class="team-content">
            <p>University of Houston, MA<br />
                서울대, 고려대, 경희대 출강<br />
                News TOEFL 연재 강의<br />
                무역아카데미 강사<br />
                현대그룹 TOEIC 강의<br />
                강남역 호야외국어학원 강사<br />
                YBM시사영어학원 <br />
                Pinpoint English<br />
                목동이르키움 원장 <br />
                <span>(현)출강학원 <br />목동이르키움</span></p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>

    <div class="one-half">
        <img src="/images/main/teacher/huhdoo.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>허두</h4></div>
        <div class="team-content">
            <p>한국학원 <br />
                이루리학원<br />
                이수능교육 영어과 실장<br />
                이지수능교육 교무부장<br />
                강북이르키움 원장<br />
                <span>(현)출강학원<br />
                    강북이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>

    <div class="one-half">
        <img src="/images/main/teacher/keum1.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>금윤섭</h4></div>
        <div class="team-content">
            <p>University of Houston<br/>
                호텔경영학 석사 수료<br/>
                중계동 종로엠스쿨 영어 전임강사<br/>
                중계동 위슬런학원 영어 전임강사<br/>
                Pinpoint English<br/>
                영어전문학원 원장(중계동)<br/>
                노원이르키움학원 원장<br/>
                <span>(현)출강학원 <br /> 노원이르키움</span>
            </p>
        </div>
    </div>
    <div class="decoration decoration-margins"></div>
    <h3 class="text-center">컨설트 스페셜리스트</h3>
    <div class="decoration-slash decoration-margins"></div>
    <div class="one-half">
        <img src="/images/main/teacher/hosub.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-desc">
            <div class="team-title"><h4>문호섭</h4></div>
            <div class="team-content">
                <p>이지수능 대표 컨설턴트 <br />
                    은평이르키움 원장<br />
                    강서이르키움 원장<br />
                    <span>(현)출강학원<br />
                        일산이르키움 본원, 강서이르키움<br />
                        은평이르키움</span></p>
            </div>

        </div>
    </div>
    <div class="decoration decoration-margins"></div>    
    <div class="one-half">
        <img src="/images/main/teacher/junkwang.jpg" alt="" class="responsive-image">
    </div>
    <div class="one-half last-column">
        <div class="team-title"><h4>전광환</h4></div>
        <div class="team-content">
            <p>룩스미아입시센터장 <br />
                솔빛입시솔루션 팀장 <br />
                관악이르키움 원장 <br />
                <span>(현)출강학원 <br />
                    관악이르키움</span></p>
        </div>
    </div>
    <div class="clear"></div>
</div>
