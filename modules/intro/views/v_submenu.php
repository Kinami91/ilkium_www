<?php
$sub_title = $this->uri->segment(2);
$sub_classname = "toast-title";
?>

<div class="sidebar nobottommargin col_last clearfix">
    <div class="sidebar-widgets-wrap">

        <div class="widget widget_links clearfix">

            <h4>sub menu</h4>
            <ul>
                <li><a href="/intro" <?= !$sub_title ? "class='$sub_classname'" : "" ?>><div>이르키움 소개</div></a></li>
                <li><a href="/intro/manage" <?= $sub_title == "manage" ? "class='$sub_classname'" : "" ?>><div>관리시스템</div></a></li>
                <li><a href="/intro/teacher" <?= $sub_title == "teacher" ? "class='$sub_classname'" : "" ?>><div>강사소개</div></a></li>
                <li><a href="/intro/why"  <?= $sub_title == "why" ? "class='$sub_classname'" : "" ?>><div>Why 이르키움?</div></a></li>
            </ul>

        </div>

    </div>
</div><!-- .sidebar end -->