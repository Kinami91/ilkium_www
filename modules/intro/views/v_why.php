<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>Why 이르키움?</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움</a></li>
            <li class="active">Why 이르키움?</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap">

        <!--        <div class="container clearfix">
        
                    <div id="side-navigation">
        
                        <div class="col_half nobottommargin">
        
                            <ul class="sidenav">
                                <li class="ui-tabs-active"><a href="#snav-content1"><i class="icon-screen"></i>재수,N수의 새로운 패러다임의 선두주자<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content2"><i class="icon-magic"></i>최적의 자습공간 제공<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content3"><i class="icon-tint"></i>완벽한 온라인 시스템 구축<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content4"><i class="icon-gift"></i>Bootstrap 3.1 Compatible<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content5"><i class="icon-adjust"></i>Light &amp; Dark Scheme<i class="icon-chevron-right"></i></a></li>
                            </ul>
        
                        </div>
        
                        <div class="col_half col_last nobottommargin">
        
                            <div id="snav-content1" aria-labelledby="ui-id-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">
                                <h3>Ultra Responsive Template</h3>
                                <img class="alignright img-responsive" src="images/landing/responsive.png" alt="">
        
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, ex, inventore, tenetur, repellat ipsam soluta libero amet nam aspernatur perspiciatis quos praesentium et debitis ea odit enim illo aliquid eligendi numquam neque. Ipsum, voluptatibus, perspiciatis a quam aliquid cumque cupiditate id ipsa tempora eveniet. Cupiditate, necessitatibus, consequatur odio. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, vitae, laboriosam libero nihil labore hic modi? Odit, veritatis nulla molestiae!
                            </div>
        
                            <div id="snav-content2" aria-labelledby="ui-id-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
                                <h3>Retina Ready Display</h3>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, voluptatem reprehenderit natus facilis id deserunt iusto incidunt cumque odit molestias iste dolor eum esse soluta facere quidem minima in voluptate explicabo ducimus alias ratione aut molestiae omnis fuga labore quod optio modi voluptatum nemo suscipit porro maxime ex. Maiores, ratione eligendi labore quaerat veniam laborum nam rem delectus illum aspernatur quas sequi animi quae nulla alias hic inventore ex perspiciatis nisi consequatur enim a aut dolorum modi quod perferendis dicta impedit magni placeat repellat. Soluta, dicta, dolores, reiciendis, eum accusamus esse et debitis rem fugit fugiat dignissimos pariatur sint quod laborum autem. Nulla, ducimus, culpa, vel esse unde sapiente expedita corrupti consectetur veritatis quas autem laborum mollmquam amet eius. Numquam, ad, quaerat, ab, deleniti rem quae doloremque tenetur ea illum hic amet dolor suscipit porro ducimus excepturi perspiciatis modi praesentium voluptas quos expedita provident adipisci dolorem! Aliquam, ipsum voluptatem et voluptates impedit ab libero similique a. Nisi, ea magni et ab voluptatum nemo numquam odio quis libero aspernatur architecto tempore qui quisquam saepe corrupti necessitatibus natus quos aliquid non voluptatibus quod obcaecati fugiat quibusdam quidem inventore quia eveniet iusto culpa incidunt vero vel in accusamus eum. Molestiae nihil voluptate molestias illum eligendi esse nesciunt.
                            </div>
        
                            <div id="snav-content3" aria-labelledby="ui-id-3" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
                                <img class="alignleft img-responsive" src="http://www.w3schools.com/tags/colormap.gif" alt="">
                                <h3>Unlimited Color Options</h3>Dolor aperiam modi aliquam dolores consequatur error commodi ad eius incidunt! Libero, odio incidunt ullam sunt fugiat? Laboriosam, perferendis, debitis, harum soluta iste eos sunt odit architecto porro eveniet sint optio nihil animi. Laudantium, quam, culpa, velit molestias exercitationem reprehenderit enim distinctio aliquam aut ex numquam sequi assumenda veritatis fuga voluptatum. Magni, voluptates adipisci unde sapiente eligendi ea maxime tempora pariatur ipsa.. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, aspernatur, saepe, quidem animi hic rem libero earum fuga voluptas culpa iure qui accusantium ab quae dolorum laborum quia repellat fugit aut minima molestias placeat mollitia doloribus quibusdam consectetur officia nesciunt ad. Ab, quod ipsum commodi assumenda doloribus possimus sed laudantium.Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            </div>
        
                            <div id="snav-content4" aria-labelledby="ui-id-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
                                <img class="alignleft img-responsive" src="images/landing/bootstrap.png" alt="">
                                <h3>Bootstrap v3.2.0 Compatiable</h3>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, nostrum, dolores id quo nam repudiandae ad culpa architecto minima nemo eaque soluta nulla laborum neque provident saepe facilis expedita numquam quas alias in perferendis accusamus ipsam blanditiis sit voluptatem temporibus vero error veritatis repellat eos reiciendis repellendus quam. Officia dicta ipsam nostrum aperiam. Dolor, expedita enim modi nostrum commodi sint architecto aliquam aut mollitia repellendus deserunt quaerat aspernatur aperiam voluptatibus consequatur rerum consequuntur.
                            </div>
        
                            <div id="snav-content5" aria-labelledby="ui-id-5" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
                                <h3>Light &amp; Dark Scheme Available</h3>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, temporibus, maxime, laudantium quidem sapiente deserunt error rerum illum explicabo voluptate velit tempora cupiditate reprehenderit consequuntur nemo in et blanditiis soluta tempore perspiciatis at atque excepturi culpa facere sequi impedit cumque illo molestias saepe eveniet ducimus fugiat reiciendis unde. Modi, at laboriosam ex velit commodi officiis! Neque, consequatur, modi, nulla, voluptatem quibusdam incidunt minus dolores repellat nihil consectetur ducimus aliquid. Eaque, tempora voluptatum accusantium expedita obcaecati magnam voluptates consequatur ut harum rem dolor id error. Officia, repudiandae, eos, quibusdam porro eius esse cupiditate non fugit dignissimos delectus et tempora sequi fugiat quo voluptatem temporibus vel obcaecati? Laboriosam, quis obcaecati quas veniam repellendus officiis et quos velit id natus mollitia dacilis ipsum et perspiciatis officia iste cupiditate ducimus nisi consequuntur excepturi dolorum. Sint, architecto, cumque facere officia harum dicta perferendis inventore excepturi sequi explicabo provident omnis dolore quasi fugit molestiae atque id consectetur reprehenderit laborum beatae consequatur similique.
                            </div>
        
                        </div>
                        <script>
                            $(function () {
                                $("#side-navigation").tabs({show: {effect: "fade", duration: 400}});
                            });
                        </script>
                    </div>
        
                </div>-->
<!--                <div class="container clearfix"><div class="clear"></div><div class="line"></div>
                    <div class="col_half nobottommargin">
        
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt">1.</i></a>
                            </div>
                            <h3>재수,N수의 새로운 패러다임의 선두주자.</h3>
                            <p>하루가 멀게 변화하는 수능에 대처하고 EBS중심의 다소 쉬워진 수능에 대비하는 유일한 대안을 제공하는 학원. 학(學)만 있고 습(習)이 거의 없는 지금의 환경에서 대학합격으로 가는 안전한 길인 자습을 충분히 제공하는 학원입니다.</p>
                        </div>
        
                    </div>            
                    <div class="col_half col_last">
        
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt">2.</i></a>
                            </div>
                            <h3>최적의 자습공간 제공</h3>
                            <p>밀폐되어 있지 않으며 넓고 쾌적한 책상에서 급우들과 선의의 경쟁을 할 수 있는 최적의 자습공간을 제공하는 학원. 이르키움학원은 학생들에게 자습을 하는데 최고의 환경을 제공하기 위해 넓은 책상과 수납공간이 주어지며, 1인당 공간이 넓어 편안하고 쾌적하게 자습을 할 수 있습니다.</p>
                        </div>
        
                    </div> 
                    <div class="col_half nobottommargin">
        
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt">3.</i></a>
                            </div>
                            <h3>완벽한 온라인 시스템 구축</h3>
                            <p>독학생들이 가장 많이 의존하는 학습의 도구인 인강을 편리하고 가장 효율적으로 활용할 수 있도록 하기 위해 완벽한 온라인 시스템을 구축했습니다.</p>
                        </div>
        
                    </div> 
                    <div class="col_half col_last">
        
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt">4.</i></a>
                            </div>
                            <h3>철저한 학원생활 관리</h3>
                            <p>혼자 공부하면서 자칫 나태해지거나 흐트러지는 것을 방지하기 위해 등하원 시간의 철저한 관리 및 면학분위기를 흐리는 학생에 대한 단호한 제재등 성공적인 재수를 위해 학원내 생활을 철저하게 관리하고 있습니다.</p>
                        </div>
        
                    </div> 
        
        
                </div>-->
        <!--        <div class="container clearfix"><div class="clear"></div><div class="line"></div>
        
                    <div class="col_three_fifth">
        
                        <div class="heading-block">
                            <h3>성적 향상을 부르는 매달 이루어지는 학과 및 입시상담</h3>
                        </div>
        
                        <p>매 달 이루어지는 모의고사의 성적을 토대로 개인당 한 달에 최소 한 번씩은 의무적으로 성적과 입시상담을 하게 되어 어디도 따라올 수 없는 최고의 상담 시스템을 만들어 두었습니다.</p>
        
                    </div>
        
                    <div class="col_two_fifth topmargin col_last" id="doughnutChart" style="opacity: 0;">
        
                    </div>
                </div>-->
        <div class="container clearfix">
            <div class="postcontent nobottommargin clearfix">
                <div id="posts" class="post-timeline clearfix">

                    <div class="timeline-border"></div>

                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            1<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <!--                        <div class="entry-image">
                                                    <a href="/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/images/blog/standard/17.jpg" alt="Standard Post with Image" style="opacity: 1;"></a>
                                                </div>                -->
                        <div class="entry-title">
                            <h2>재수,N수의 심장을 꿰뚫는 명쾌한 개인밀착관리</h2>
                        </div>
                        <div class="entry-content">
                            <img src="/images/main/why_01.png" alt="" align="left"/>
                            <p>오랜 기간 동안 재수종합반에서의 경험을 가진 원장들의 핵심을 찌르는 관리
와 상담은 이르키움만이 드리는 최대의 자산입니다. 현장에서 느꼈던 재수생들
의 고충을 누구보다도 잘 이해하여 수능 날까지 최선을 다할 수 있도록 지도하는
것이 이르키움학원의 원장들과 선생님들의 최고의 목표입니다.</p>
                        </div>
                    </div>
                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            2<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <!--                        <div class="entry-image">
                                                    <a href="/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/images/blog/standard/17.jpg" alt="Standard Post with Image" style="opacity: 1;"></a>
                                                </div>                -->
                        <div class="entry-title">
                            <h2>최적의 자습공간 제공</h2>
                        </div>
                        <div class="entry-content">
                            <img src="/images/main/why_04.png" alt="" align="right" style="height: 250px;"/>
                            <p>밀폐되어 있지 않으며 넓고 쾌적한 책상에서 급우들과 선의의 경쟁을 할 수 있는 최적의 자습공간을 제공하는 학원. 이르키움학원은 학생들에게 자습을 하는데 최고의 환경을 제공하기 위해 넓은 책상과 수납공간이 주어지며, 1인당 공간이 넓어 편안하고 쾌적하게 자습을 할 수 있습니다.</p>
                        </div>
                    </div>
                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            3<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <!--                        <div class="entry-image">
                                                    <a href="/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/images/blog/standard/17.jpg" alt="Standard Post with Image" style="opacity: 1;"></a>
                                                </div>                -->
                        <div class="entry-title">
                            <h2>완벽한 온라인 시스템 구축</h2>
                        </div>
                        <div class="entry-content">
                            <img src="/images/main/why_03.png" alt="" align="left" style="height: 250px;"/>
                            <p>독학생들이 가장 많이 의존하는 학습의 도구인 인강을 편리하고 가장 효율적으로 활용할 수 있도록 하기 위해 
                                완벽한 온라인 시스템을 구축했습니다.
                                아울러 인터넷을 통한 질문지, 자체 무료인강, 1:1 화상상담등의 온라인서비스도 제공됩니다.</p>
                        </div>
                    </div>
                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            4<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <!--                        <div class="entry-image">
                                                    <a href="/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/images/blog/standard/17.jpg" alt="Standard Post with Image" style="opacity: 1;"></a>
                                                </div>                -->
                        <div class="entry-title">
                            <h2>철저한 학원생활 관리</h2>
                        </div>
                        <div class="entry-content">
                            <img src="/images/main/why_02.png" alt="" align="right"  style="height: 250px;"/>
                            <p>혼자 공부하면서 자칫 나태해지거나 흐트러지는 것을 방지하기 위해 등하원 시간의 철저한 관리 및 면학분위기를 흐리는 학생에 대한 단호한 제재등 성공적인 재수를 위해 학원내 생활을 철저하게 관리하고 있습니다.</p>
                        </div>
                    </div>
                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            5<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <!--                        <div class="entry-image">
                                                    <a href="/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/images/blog/standard/17.jpg" alt="Standard Post with Image" style="opacity: 1;"></a>
                                                </div>                -->
                        <div class="entry-title">
                            <h2>최고의 강사진으로 구성된 질의, 응답과 클리닉 시스템</h2>
                        </div>
                        <div class="entry-content">
                            <img src="/images/main/why_05.png" alt="" align="left" style="height: 250px;"/>
                            <p>10년 이상 재수종합반에서 강의를 해온 최고의 지점원장들과 강사들이 인강에서는 절대로 해결해 주지 못하는 부분들을 명쾌하게 답해드립니다. 마찬가지로 인강에서는 제공할 수 없는 학과상담을 과목별 원장과 강사들이 1:1 대면 상담해 드립니다.
                                또한 학생의 요청시 학생에게 맞춤 인강 및 화상 강의를 제공합니다.</p>
                        </div>
                    </div>

                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            6<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <div class="entry-title">
                            <h2>성적 향상을 부르는 매달 이루어지는 학과 및 입시상담</h2>
                        </div>
                        <div class="entry-content">
                            <!--<img src="/images/main/why_05.png" alt="" align="right" style="height: 250px;"/>-->
                            <p>매 달 이루어지는 모의고사의 성적을 토대로 개인당 한 달에 최소 한 번씩은 의무적으로 성적과 입시상담을 하게 되어 어디도 따라올 수 없는 최고의 상담 시스템을 만들어 두었습니다.

                            </p>
                        </div>
                    </div>

                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            7<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <div class="entry-title">
                            <h2>수시대비를 위한 논술특강 진행</h2>
                        </div>
                        <div class="entry-content">
                            <p>독학은 수능점수를 극대화시키기에 최적화 되어있어 수시를 염두해 두는 학생들에게는 논술에 대한 부담과 스트레스는 적지 않다고 볼 수 있습니다. 그런 이유로 이르키움학원은 논술을 대비한 특강을 준비해 두고 있습니다.</p>
                        </div>
                    </div>

                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            8<span>Reason</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <div class="entry-title">
                            <h2>주요과목별 선택 clinic 수업 진행</h2>
                        </div>
                        <div class="entry-content">
                            <p>인강에서 해결할 수 없거나 실강에 대한 필요성을 느끼는 학생들을 위해 과목별 선택 클리닉 수업을 진행합니다.</p>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo $submenu?>         
        </div>  
        <div class="container clearfix">
            <div class="promo promo-dark promo-flat bottommargin">
                <h3>재수,N수를 준비하는 학생이라면 이르키움과 함께 해보세요!</h3>
                <span>오로지 독학재수관리만 해온 이르키움만의 노하우로 수험생들의 성공을 위해 끝까지 함께하겠습니다.</span>
                <a href="/branch" class="button button-dark button-xlarge button-rounded">지점 방문</a>
            </div>               
        </div>
    </div>
</section>