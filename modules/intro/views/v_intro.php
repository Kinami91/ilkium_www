<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini page-title-dark">

    <div class="container clearfix">
        <h1>이르키움 소개</h1>
        <span>Introduce</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">이르키움</a></li>
            <li class="active">이르키움 소개</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section id="content" style="margin-bottom: 0px;">

    <div class="content-wrap nopadding">
        <div class="section parallax full-screen nomargin noborder" style="height: 300px; background-image: url(/images/main/intro_bg.jpg); background-position: 50% -19.2px;" data-stellar-background-ratio="0.4">
            <div class="vertical-middle" style="position: absolute; top: 50%; width: 100%; padding-top: 0px; padding-bottom: 0px; margin-top: -125px;">
                <div class="container clearfix">

                    <div class="col_three_fifth nobottommargin dark">

                        <div class="emphasis-title">
                            <h2><span style="color: #F00 !important; font-size: 4.2rem;">이르키움</span>이 만든</h2>
                            <h2><span style="color: #FFF !important; font-size: 4.2rem;">독학재수관리 시스템</span>에는</h2>
                            <h2>지난 7년간에 걸쳐 깨우친</h2>
                            <h2><span style="color: #FFFFFF !important; font-size: 4.2rem;">이르키움</span>만의 <span style="color: #f00; font-size: 4.2rem;">철학</span>이 </h2>
                            <h2>담겨 있습니다.</h2>

                        </div>
                         <!--<p class="lead topmargin-sm">--- 재시작의 틀을 깨다.</p>-->

                    </div>

                </div>
            </div>
        </div>
        <div class="section nomargin noborder" style="background-image: url('http://canvashtml-cdn.semicolonweb.com/images/parallax/3.jpg');">
            <div class="heading-block center nobottomborder nobottommargin">
                <h2><span>이르키움</span>은</h2>
                <h3>"실패를 경험한 학생들을 일으켜 세워 수험 성공의 길로 전환시키는 역할을 합니다."</h3>
                <!--                <h2>실력있는 학생들을 대학보내는 건 쉽습니다. <br />
                                        실력이 부족한 학생들의 성적을 올려주어 대학을 보내주기에 <br />
                                        <span>이르키움 학원</span>의 가치가 증명됩니다.</h2>-->
            </div>
        </div>        
        <div class="container nobottommargin clearfix">

            <!-- Post Content
            ============================================= -->
            <div class="content-wrap clearfix">

                <div class="fancy-title title-bottom-border">
                    <h3><span>이르키움</span>은</h3>
                </div>

                <!--<i class="i-circled i-light icon-thumbs-up"></i><h3 style="padding-top:15px;">이르키움은...</h3>-->

                <!--                <blockquote>
                                    <p>실패를 경험한 학생들을 일으켜 세워 수험 성공의 길로 전환시키는 역할을 합니다.</p>
                                    <p>실력있는 학생들을 대학보내는 건 쉽습니다. <br />
                                        실력이 부족한 학생들의 성적을 올려주어 대학을 보내주기에 <br />
                                        이르키움 학원의 가치가 증명됩니다.</p>
                                </blockquote>
                
                
                                <div class="divider"><i class="icon-circle"></i></div>
                
                                <i class="i-circled i-light icon-line-bar-graph"></i><h3 style="padding-top:15px;">재시작의 틀을 깨다.</h3>-->

                <p style="font-size: 18px;font-weight: initial;">이르키움은 학생에게 최적의 학습방향을 제시하고 가르침을 최소화합니다. <br />
                        그 방향에 따라 “습”을 최고로 강조하여 최상의 결과를 끌어내기 위해 최선을 다합니다. <br />
                        1등을 대학 보내기는 쉽습니다.<br />
                        <span style="font-weight: 600;font-size: 20px;">하지만 2등, 3등을 1등으로 만들어 대학을 보내주기에 이르키움의 가치가 증명됩니다.</span> 
                </p>
                <p>&nbsp;</p>

                <div class="clearfix"></div>
                <div class="fancy-title title-bottom-border">                
                    <h3><span>독학재수</span>의 오랜 노하우.</h3>
                </div>                
                <!--<i class="i-circled i-light icon-magic"></i><h3 style="padding-top:15px;">독학재수의 오랜 노하우.</h3>-->

                <blockquote class="pull-left quote">
                    <p><i>"나의 새로운 시작은 내 안에 있는 자양분을 찾는 것에서부터...</i>"</p>
                </blockquote>

                <p style="font-size: 18px">2010년 목동에서 처음 독학재수학원을 시작한다고 했을 때는 주위에서 우려와 실패할 것이라는 시선이 대부분이었으나, 
                    현재는 재수,N수 학원의 새로운 패러다임으로 자리잡았습니다. 이르키움만의 그 간의 노하우는 학생들이 최적의 환경에서 
                    공부에 집중할 수 있게 해서 최고의 성적향상을 이끌어 내는데  도움을 줄 것입니다.</p>
                <p style="font-size: 18px">2016년부터는 좀더 많은 곳에서 이르키움과 함께 대입성공의 꿈을 키워나갈 수 있습니다.</p>
                <p>&nbsp;</p>
                
                <!--<div class="entry-image">-->
                    <div class="center">
                        <img src="/images/main/intro_networks1.png" alt="Blog Single" >
                    </div>
                <!--</div>-->   
                



            </div><!-- .postcontent end -->

            <?php /* echo $submenu  */ ?>
        </div>

    </div>

</section>