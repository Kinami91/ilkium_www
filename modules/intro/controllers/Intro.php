<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Intro extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (BROWSER_TYPE == "W") {
            $data['submenu'] = $this->load->view("v_submenu", null, true);
            $this->load->view("inc/v_header");
            $this->load->view("v_intro", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_intro", $data);
            $this->load->view("inc/m/v_footer");
        }
    }

    public function manage() {
        if (BROWSER_TYPE == "W") {
            $data['submenu'] = $this->load->view("v_submenu", null, true);
            $this->load->view("inc/v_header");
            $this->load->view("v_manage", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_manage", $data);
            $this->load->view("inc/m/v_footer");
        }
    }

    public function why() {
        if (BROWSER_TYPE == "W") {
            $data['submenu'] = $this->load->view("v_submenu", null, true);
            $this->load->view("inc/v_header");
            $this->load->view("v_why", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_why", $data);
            $this->load->view("inc/m/v_footer");
        }
    }

    public function teacher() {
        if (BROWSER_TYPE == "W") {
            $data['submenu'] = $this->load->view("v_submenu", null, true);
            $this->load->view("inc/v_header");
            $this->load->view("v_teacher", $data);
            $this->load->view("inc/v_footer");
        } else {
            $this->load->view("inc/m/v_header");
            $this->load->view("m/v_teacher", $data);
            $this->load->view("inc/m/v_footer");
        }
    }

}

/* End of file Intro.php */
/* Location: ./application/controllers/Intro.php */